import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AngularMaterialModule } from './modules/material/mat-import.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragulaModule , DragulaService } from "ng2-dragula";
import { CKEditorModule } from 'ngx-ckeditor';
import { ImageCropperModule } from 'ngx-img-cropper';
// Custom Modules
import { DialogModule } from './dialogs/dialogs.module';
import { ComponentsModule } from './components/components.module';
// Services
import { AuthService } from './services/auth.service';
import { LocalizationService } from './services/localization.service';
import { SpinnerService } from './services/spinner.service';
// API Services
import { CategoriesAPIService } from './api/categories-api.service';
import { UserAPIService } from './api/user-api.service';
import { ShopAPIService } from './api/shop-api.service';
import { ErrorLoggerApiService } from './api/error-logger-api.service';
import { FileAPIService } from './api/file-api.service';
import { ProductAPIService } from './api/product-api.service';
import { ModalService } from './services/modals.service';
import { NotificationService } from './services/notification.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    DragulaModule.forRoot(),
    DialogModule,
    ComponentsModule,
    CKEditorModule,
    ImageCropperModule,
  ],
  exports: [
    // Modules
    CommonModule,
    RouterModule,
    HttpClientModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    DialogModule,
    ComponentsModule,
    CKEditorModule,
    ImageCropperModule,
    DragulaModule,
  ],
  entryComponents: [
  ],
  declarations: [
  ],
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [
        // Services
        AuthService,
        LocalizationService,
        DragulaService,
        SpinnerService,
        ModalService,
        NotificationService,
        // API Services
        CategoriesAPIService,
        UserAPIService,
        ShopAPIService,
        ErrorLoggerApiService,
        FileAPIService,
        ProductAPIService,
      ]
    };
  }
}
