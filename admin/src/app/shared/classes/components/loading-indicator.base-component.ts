import { Directive, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { map } from 'rxjs/operators';
import { Spinners } from "../../enums";
import { SpinnerService } from "../../services/spinner.service";
@Directive()
export class LoadingIndicatorBaseComponent implements OnInit, OnDestroy {
    public readonly Spinners: typeof Spinners = Spinners;
    private _spinnerType: Spinners = Spinners.RING;
    public get spinnerType(): Spinners {
        return this._spinnerType;
    }

    public isSpinnerEnabled!: boolean;
    private subscription: Subscription = new Subscription();

    constructor(
        protected spinnerService: SpinnerService
    ) {}

    ngOnInit(): void {
        this.isSpinnerEnabled = this.spinnerService.isSpinnerEnabled
        this.subscription.add(this.spinnerService.spinningObservable.pipe(
            map((isSpinnerEnabled: boolean) => {
                this.isSpinnerEnabled = isSpinnerEnabled;
                return isSpinnerEnabled;
            })
        ).subscribe());
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

}