import { ApplicationRef, ComponentFactoryResolver, ComponentRef, EmbeddedViewRef, Injectable, Injector, ReflectiveInjector } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { NotificationModalComponent as WebNotificationModalComponent } from '../components/notification/notification-modal.component';
import { INotificationServiceOptions } from '../interfaces/notifications';

@Injectable({providedIn: 'root'})
export class NotificationService {
  private componentRef!: ComponentRef<any> | null;
  private modalComponent: any = WebNotificationModalComponent;

  private closeSubject: Subject<any> = new Subject();

  private notificationStream: BehaviorSubject<INotificationServiceOptions | null> = new BehaviorSubject<INotificationServiceOptions | null>(null);

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
  ) {
    this.startNotification(this.modalComponent, {notificationStream: this.notificationStream.asObservable()})
  }

  public show(notification: INotificationServiceOptions) {
    this.notificationStream.next(notification);
  }

  private startNotification(modalComponent: any, options?: any) {

    this.appendComponentToBody(modalComponent, options);
  }

  private appendComponentToBody(component: any, data: any) {
    // 1. Create a component reference from the component
    this.componentRef = this.componentFactoryResolver
      .resolveComponentFactory(component)
      .create(
        ReflectiveInjector.resolveAndCreate([{
          provide: 'data',
          useValue: data
        }])
      );

    // 2. Attach component to the appRef so that it's inside the ng component tree
    this.appRef.attachView(this.componentRef.hostView);

    // 3. Get DOM element from component
    const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;

    // 4. Append DOM element to the body
    document.body.appendChild(domElem);
  }

  private closeDialog(data?: any) {
    if (!this.componentRef) return;
    this.appRef.detachView(this.componentRef.hostView);
    this.componentRef.destroy();
    this.componentRef = null;
    this.closeSubject.next(data || null);
  }

}

export enum NotificationTypes {
  DANGER = 'danger',
  WARNING = 'warning',
  SUCCESS = 'success',
  INFO = 'info'
}
