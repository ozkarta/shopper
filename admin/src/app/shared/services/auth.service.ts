import { HttpContextToken } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject, of } from 'rxjs';
import { map, catchError, concatMap } from 'rxjs/operators';
import { UserAPIService } from '../api/user-api.service';

@Injectable()
export class AuthService {
  private _currentUser: BehaviorSubject<any> = new BehaviorSubject(null);
  public currentUserObservable: Observable<any> = this._currentUser.asObservable();
  public get currentUser() {
    return this._currentUser.value;
  }
  private readonly accessTokenKey = 'access-token';;
  private readonly refreshTokenKey = 'refresh-token';

  constructor(private userApiService: UserAPIService) {
  }

  logInUser(data: any): Observable<any> {
    return this.userApiService.loginUser(data).pipe(
      map(({accessToken, refreshToken}) => {
        this.setAuthParams({accessToken, refreshToken});
        return {accessToken, refreshToken};
      }),
      concatMap(() => {
        return this.userApiService.getMe().pipe(
          map(currentUser => {
            return this.handleUserLoad(currentUser);
          }),
        );
      }),
    )
  }

  logOutUser() {
    localStorage.removeItem(this.accessTokenKey);
    localStorage.removeItem(this.refreshTokenKey);
    this._currentUser.next(null);
  }

  isSigned() {
    // this is for test only
    return !!localStorage.getItem(this.accessTokenKey) && !!this.currentUser && !!this.currentUser['email'] && this.currentUser['role'] === 'admin';
  }

  getAuthParams() {
    return {
      'access-token': localStorage.getItem(this.accessTokenKey) || '',
    }
  }

  getAuthParamsWithRefreshToken() {
    return {
      'access-token': localStorage.getItem(this.accessTokenKey) || '',
      'refresh-token': localStorage.getItem(this.refreshTokenKey) || '',
    }
  }

  loadUser() {
    let accessToken = this.getAuthParams()['access-token'];
    if (accessToken) {
      return this.userApiService.getMe().pipe(
        map(currentUser => {
          return this.handleUserLoad(currentUser);
        })
      )
    }
    return of(null);
  }

  private handleUserLoad(currentUser: any) {
    if (!currentUser) {
      this.logOutUser();
      return of(null);
    }
    if (currentUser['role'] !== 'admin') {
      this.logOutUser();
      return of(null);
    }
    this.setCurrentUser(currentUser);
    return currentUser;
  }

  private setCurrentUser(data: any) {
    this._currentUser.next(data);
  }

  public setAuthParams({accessToken, refreshToken}: any) {
    localStorage.setItem(this.accessTokenKey, accessToken);
    localStorage.setItem(this.refreshTokenKey, refreshToken);
  }
}