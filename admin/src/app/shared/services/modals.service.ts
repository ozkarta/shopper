import { ApplicationRef, ComponentFactoryResolver, ComponentRef, EmbeddedViewRef, Injectable, Injector, ReflectiveInjector } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
@Injectable({providedIn: 'root'})
export class ModalService {
  private componentRef?: ComponentRef<any> | null;

  private closeSubject: Subject<any> = new Subject();
  public afterDialogClose: Observable<any> = this.closeSubject.asObservable();
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private router: Router
  ) {
    this.subscribeNavigationChange();
  }

  subscribeNavigationChange() {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart || event instanceof NavigationEnd) {
        if (this.componentRef) {
          this.closeDialog();
          return {}
        }
      }
      return {}
    })
  }

  openModal(modalComponent: any, data?: object) {
    if (this.componentRef) {
      this.closeDialog();
      return {}
    }

    this.appendComponentToBody(modalComponent, data);
    return {
      afterClosed: this.afterDialogClose.pipe(take(1))
    };
  }

  appendComponentToBody(component: any, data: any) {
    // 1. Create a component reference from the component
    this.componentRef = this.componentFactoryResolver
      .resolveComponentFactory(component)
      .create(
        ReflectiveInjector.resolveAndCreate([{
          provide: 'data',
          useValue: data
        }])
      );

    // 2. Attach component to the appRef so that it's inside the ng component tree
    this.appRef.attachView(this.componentRef.hostView);

    // 3. Get DOM element from component
    const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;

    // 4. Append DOM element to the body
    document.body.appendChild(domElem);
  }

  closeDialog(data?: any) {
    if (this.componentRef) {
      if (this.componentRef.hostView) {
        this.appRef.detachView(this.componentRef.hostView);
      }
      this.componentRef.destroy();
      this.componentRef = null;
    }
    if (this.closeSubject) {
      this.closeSubject.next(data || null);
    }
  }
}
