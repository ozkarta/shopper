import { AppLanguages, Currencies } from '../enums/index';
export interface IUser {
  language?: AppLanguages,
  currency?: Currencies,

  firstName: string;
  lastName: string;
  email: string;
  role: string;

  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}
