export interface INotificationServiceOptions {
  message: string;
  type: 'danger' |  'warning' | 'success' | 'info';

  icon?: string;
  showCloseBadge?: boolean;
  closeBadgeMessage?: string;
  closeHandler?: Function;
  autoClose?: boolean;
  autoCloseAfter?: number;
  customButton?: ICustomNotificationButton
}

export interface ICustomNotificationButton {
  title: string;
  styles?: {[key: string]: string},
  clickHandler: Function,
}
