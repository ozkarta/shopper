import { NgModule } from '@angular/core';

import { ConfirmationDialogComponent } from './confirmation-modal/confirmation-modal.component';
import { AngularMaterialModule } from '../modules/material/mat-import.module';
@NgModule({
  declarations: [
    ConfirmationDialogComponent,
  ],
  imports: [
    AngularMaterialModule,
  ],
  exports: [
    ConfirmationDialogComponent,
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
})
export class DialogModule {
}
