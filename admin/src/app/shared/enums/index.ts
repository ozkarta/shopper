export enum AppLanguages {
  ENGLISH = 'EN',
  GEORGIAN = 'GE',
  FRENCH = 'FR',
  GERMAN = 'DE',
}

export enum Currencies {
  GEL = 'GEL'
}

export enum Spinners {
  CIRCLE = 'circle',
  DUAL_RING = 'dual-ring',
  FACEBOOK = 'facebook',
  HEART = 'heart',
  RING = 'ring',
  ROLLER = 'roller',
  DEFAULT = 'default',
  ELLIPSIS = 'ellipsis',
  GRID = 'grid',
  HOURGLASS = 'hourglass',
  RIPPLE = 'ripple',
  SPINNER = 'spinner'
}


export enum MediaImageResolutions {
  HIGH_RESOLUTION_IMAGE = 'high_resolution_image',
  MEDIUM_RESLUTION_IMAGE = 'mediam_resolution_image',
  LOW_RESLUTION_IMAGE = 'low-resolution-image',
  ORIGINAL_IMAGE = 'original-image',
}