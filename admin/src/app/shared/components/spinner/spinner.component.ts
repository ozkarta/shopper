import {Component, OnInit, OnDestroy} from '@angular/core';
import { Subscription } from 'rxjs';
import { SpinnerService } from '../../services/spinner.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})

export class SpinnerComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  public show = false;
  
  constructor(private spinnerService: SpinnerService) {
  }

  ngOnInit() {
    this.subscribeSpinner();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  subscribeSpinner() {
    this.subscription.add(this.spinnerService.isSpinning.subscribe(isSpinning => {
      this.show = isSpinning;
    }));
  }

  clickHandler(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }
}