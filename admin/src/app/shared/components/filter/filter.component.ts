import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-generic-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})

export class FilterComponent {
  @Input() query!: any;
  @Input() numTotal!: number;
  @Output('filterChange') updateQueryEmitter: EventEmitter<any> = new EventEmitter();
  
  updateQuery() {
    this.query.page = 1;
    this.updateQueryEmitter.emit(this.query);
  }

  matEventHandler(data: {pageIndex: number, pageSize: number}) {
    this.query.page = data.pageIndex + 1;
    this.query.limit = data.pageSize;
    this.updateQueryEmitter.emit(this.query);
  }
}