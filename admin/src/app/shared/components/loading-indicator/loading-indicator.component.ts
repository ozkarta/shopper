import { Component, OnDestroy, OnInit } from "@angular/core";
import { SpinnerService } from "src/app/shared/services/spinner.service";
import { LoadingIndicatorBaseComponent } from "../../classes/components/loading-indicator.base-component";

@Component({
    selector: 'app-web-loading-indicator',
    templateUrl: 'loading-indicator.component.html',
    styleUrls: ['loading-indicator.component.css']
})

export class WebLoadingIndicator extends LoadingIndicatorBaseComponent implements OnInit, OnDestroy {
    
    constructor(protected spinnerService: SpinnerService) {
        super(spinnerService);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }
}