import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
// import { SpinnerComponent } from './spinner/spinner.component';
import { AngularMaterialModule } from '../modules/material/mat-import.module';
import { FilterComponent } from './filter/filter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WebLoadingIndicator } from './loading-indicator/loading-indicator.component';
import { NotificationModalComponent } from './notification/notification-modal.component';

const declarations = [
  // SpinnerComponent,
  FilterComponent,
  WebLoadingIndicator,
  NotificationModalComponent
]

@NgModule({
  declarations,
  imports: [
    ReactiveFormsModule,
    FormsModule,
    AngularMaterialModule,
    CommonModule,
  ],
  exports: [
    ...declarations
  ],
  entryComponents: [
    // SpinnerComponent
  ],
})
export class ComponentsModule {
}
