import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BaseNotificationModalComponent } from '../../classes/components/base-notification-modal.component';
import { INotificationServiceOptions } from '../../interfaces/notifications';
import { ModalService } from '../../services/modals.service';
@Component({
  selector: 'app-notification-web-component',
  templateUrl: './notification-modal.component.html',
  styleUrls: ['./notification-modal.component.css']
})

export class NotificationModalComponent extends BaseNotificationModalComponent {
  constructor(
    @Inject('data')data: {notificationStream: Observable<INotificationServiceOptions>},
    protected modalService: ModalService,
    protected router: Router,
    protected route: ActivatedRoute) {
      super(data, modalService, router, route);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy()
  }

}

