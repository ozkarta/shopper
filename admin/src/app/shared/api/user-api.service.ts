import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpContext, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BYPASS_TOKEN_INTERCEPTION } from '../interceptors/token-interceptor.service';

const API_URL = environment.serverUrl;
@Injectable()

export class UserAPIService {
  constructor(private httpClient: HttpClient) { }
  // =============================================================================
  getMe(): Observable<any> {
    return this.httpClient.get(`${API_URL}/api/v1/user/me`);
  }

  getByQuery(params: any): Observable<any> {
    return this.httpClient.get(`${API_URL}/api/v1/user/query`, {params});
  }
  // =============================================================================
  loginUser(userData: any): Observable<any> {
    return this.httpClient.post(`${API_URL}/api/v1/user/login`, userData);
  }

  logOut(): Observable<any> {
    return this.httpClient.post(`${API_URL}/api/v1/user/logout`, {});
  }

  refreshTokens(): Observable<any> {
    return this.httpClient.post(`${API_URL}/api/v1/user/refresh-tokens`, {}, {context: (new HttpContext()).set(BYPASS_TOKEN_INTERCEPTION, true)}); 
  }

  adminCreateMarketManager(data: any) : Observable<any> {
    return this.httpClient.post(`${API_URL}/api/v1/user/admin-create-market-manager`, data);
  }

  // ==============================================================================
  adminUpdateMarketManager(id: string, data: any) : Observable<any> {
    return this.httpClient.put(`${API_URL}/api/v1/user/admin-update-market-manager/${id}`, data);
  }
  // ==============================================================================
}