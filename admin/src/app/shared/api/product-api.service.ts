import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ProductModel } from '../models/product.model';
import { map } from 'rxjs/operators';

const API_URL = environment.serverUrl;
@Injectable()

export class ProductAPIService {
  constructor(private httpClient: HttpClient) { }

  getProductsByQuery(params: any): Observable<{items: ProductModel[], numTotal: number}> {
    return this.httpClient.get<{items: ProductModel[], numTotal: number}>(`${API_URL}/api/v1/product/query`, {params});
  }

  createProductWithShop(data: any) : Observable<ProductModel> {
    return this.httpClient.post<ProductModel>(`${API_URL}/api/v1/product/product-with-shop`, data).pipe(
      map(result => new ProductModel(result))
    );
  }

  updateProduct(id: string, data: any) : Observable<ProductModel> {
    return this.httpClient.put<ProductModel>(`${API_URL}/api/v1/product/product/${id}`, data).pipe(
      map(result => new ProductModel(result))
    );
  }

  updateProductInfo(id: string, data: any) : Observable<ProductModel> {
    return this.httpClient.put<ProductModel>(`${API_URL}/api/v1/product/product-info/${id}`, data).pipe(
      map(result => new ProductModel(result))
    );
  }

  updateCategoriesAndKeywords(id: string, data: any) : Observable<ProductModel> {
    return this.httpClient.put<ProductModel>(`${API_URL}/api/v1/product/categories-and-keywords/${id}`, data).pipe(
      map(result => new ProductModel(result))
    );
  }

  updateProductEnabledStatus(id: string, data: any) : Observable<ProductModel> {
    return this.httpClient.put<ProductModel>(`${API_URL}/api/v1/product/product-enabled-status/${id}`, data).pipe(
      map(result => new ProductModel(result))
    );
  }

  updateProductMedia(id: string, data: any) : Observable<ProductModel> {
    return this.httpClient.put<ProductModel>(`${API_URL}/api/v1/product/product-media/${id}`, data).pipe(
      map(result => new ProductModel(result))
    );
  }

  updateProductCategoriesAndKeywords(id: string, data: any) : Observable<ProductModel> {
    return this.httpClient.put<ProductModel>(`${API_URL}/api/v1/product/product-categories-and-keywords/${id}`, data).pipe(
      map(result => new ProductModel(result))
    );
  }

  updateProductPriceAndDiscountData(productId: string, data: any): Observable<ProductModel> {
    return this.httpClient.put<ProductModel>(`${API_URL}/api/v1/product/price-and-discuoont-data/${productId}`, data).pipe(
      map(result => new ProductModel(result))
    );
  }

  deleteProduct(id: string) : Observable<any> {
    return this.httpClient.delete(`${API_URL}/api/v1/product/${id}`);
  }

  getProductById(id: string): Observable<ProductModel> {
    return this.httpClient.get<ProductModel>(`${API_URL}/api/v1/product/${id}`).pipe(
      map(result => new ProductModel(result))
    );
  }
}