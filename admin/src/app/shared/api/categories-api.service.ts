import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CategoryModel } from '../models/category.model';
import { map } from 'rxjs/operators';

const API_URL = environment.serverUrl;
@Injectable()

export class CategoriesAPIService {
  constructor(private httpClient: HttpClient) { }

  create(categoryData: any): Observable<any> {
    return this.httpClient.post(`${API_URL}/api/v1/category`, categoryData);
  }

  update(id: string, categoryData: any): Observable<any> {
    return this.httpClient.put(`${API_URL}/api/v1/category/${id}`, categoryData);
  }

  updateLocation(id: string, categoryData: any): Observable<any> {
    return this.httpClient.put(`${API_URL}/api/v1/category/category-location/${id}`, categoryData);
  }

  remove(id: string): Observable<any> {
    return this.httpClient.delete(`${API_URL}/api/v1/category/${id}`);
  }

  getGenericCategories():Observable<CategoryModel[]> {
    return this.httpClient.get<CategoryModel[]>(`${API_URL}/api/v1/category/generic`).pipe(
      map((result: any[]) => result.map((item: any) => {return item? new CategoryModel(item): <any>null}))
    );
  }

  getAllCategories(depth: number = 50):Observable<CategoryModel[]> {
    return this.httpClient.get<CategoryModel[]>(`${API_URL}/api/v1/category`, {params: {depth}}).pipe(
      map((result: any[]) => result.map((item: any) => {return item? new CategoryModel(item): <any>null}))
    );
  }

  getGeneric2LevelCategories():Observable<CategoryModel[]> {
    return this.httpClient.get<CategoryModel[]>(`${API_URL}/api/v1/category/generic2level`).pipe(
      map((result: any[]) => result.map((item: any) => {return item? new CategoryModel(item): <any>null}))
    );
  }

  getParentCategories(): Observable<CategoryModel[]> {
    return this.httpClient.get<CategoryModel[]>(`${API_URL}/api/v1/category/parent-categories`).pipe(
      map((result: any[]) => result.map((item: any) => {return item? new CategoryModel(item): <any>null}))
    );
  }

  getChildCategories(categoryId: string): Observable<CategoryModel[]> {
    return this.httpClient.get<CategoryModel[]>(`${API_URL}/api/v1/category/child-categories/${categoryId}`).pipe(
      map((result: any[]) => result.map((item: any) => {return item? new CategoryModel(item): <any>null}))
    );
  }

  getByQuery(params: any): Observable<CategoryModel[]> {
    return this.httpClient.get<CategoryModel[]>(`${API_URL}/api/v1/category`, {params}).pipe(
      map((result: any[]) => result.map((item: any) => {return item? new CategoryModel(item): <any>null}))
    );
  }
}