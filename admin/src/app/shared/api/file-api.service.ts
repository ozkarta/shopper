import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FileModel } from '../models/file.model';
import { MediaModel } from '../models/media.model';

const API_URL = environment.serverUrl;
@Injectable()

export class FileAPIService {
  constructor(private httpClient: HttpClient) { }

  public upladCroppedImageAlngWithOriginal(mediaId: string | null, originalFile: Blob | null, croppedFile: Blob, fileName?: string): Observable<MediaModel> {
    const formData = new FormData();
    if (mediaId) {
      formData.append('mediaId', mediaId);
    }
    if (originalFile) {
      formData.append('original-image-file', originalFile, fileName);
    }
    formData.append('cropped-image-file', croppedFile, 'cropped_' + fileName);
    return this.httpClient.post<MediaModel>(`${API_URL}/api/v1/file/cropped-image`, formData);
  }

  public uploadSingleImage(file: Blob, fileName?: string): Observable<MediaModel> {
    const formData = new FormData();
    formData.append('image-file', file, fileName);
    return this.httpClient.post<MediaModel>(`${API_URL}/api/v1/file/image`, formData);
  }

  public uploadMultipleImages(files: Blob[]): Observable<FileModel[]> {
    const formData = new FormData();
    for (let file of files) {
      formData.append('image-files', file);
    }
    return this.httpClient.post<FileModel[]>(`${API_URL}/api/v1/file/image`, formData);
  }

  // ==============================================================================================================
  public download(fileId: string): Observable<any> {
    return this.httpClient.get(`${API_URL}/api/v1/file/download/${fileId}`);
  }

  public getBase64DataURI(fileId: string): Observable<string> {
    return this.httpClient.get(`${API_URL}/api/v1/file/image/${fileId}`, {responseType: 'text'});
  }

  public getFileStream(fileId: string): Observable<Blob> {
    return this.httpClient.get(`${API_URL}/api/v1/file/image/${fileId}`, {responseType: 'blob'});
  }

  public getFileDownloadUrl(fileId: string): string {
    return `${API_URL}/api/v1/file/download/${fileId}`;
  }

  public getFileReadableStreamUrl(fileId: string): string {
    return `${API_URL}/api/v1/file/file-stream/${fileId}`;
  }
}