
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { environment } from '../../../environments/environment';
import { catchError, map } from 'rxjs/operators';

const API_URL = environment.serverUrl;

@Injectable()
export class ErrorLoggerApiService {
  constructor(private http: HttpClient) { }

  public getDirectoriesByQuery(params?: any): Observable<any> {
    return this.http.get<any>(`${API_URL}/api/v1/error-log/log-directories`, { params }).pipe(
      map(response => response),
      catchError(error => {
        return of({
          items: [],
          numTotal: 0,
        })
      })
    );
  }

  public getFilesByDirectory(params?: any): Observable<any> {
    return this.http.get<any>(`${API_URL}/api/v1/error-log/files-by-directory`, { params }).pipe(
      map(response => response),
      catchError(error => {
        return of({
          items: [],
          numTotal: 0,
        })
      })
    );
  }

  public getFileByName(params?: any): Observable<any> {
    return this.http.get<any>(`${API_URL}/api/v1/error-log/file`, { params }).pipe(
      map(response => response),
      catchError(error => {
        return of(null);
      })
    );
  }
}