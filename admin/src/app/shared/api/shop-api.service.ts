import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ShopModel } from '../models/shop.model';
import { map } from 'rxjs/operators';

const API_URL = environment.serverUrl;
@Injectable()

export class ShopAPIService {
  constructor(private httpClient: HttpClient) { }

  getShopsByQuery(params: any): Observable<{items: ShopModel[], numTotal: number}> {
    return this.httpClient.get<{items: ShopModel[], numTotal: number}>(`${API_URL}/api/v1/shop/query`, {params});
  }

  // ========================================================================================================
  createShopWithOwner(data: any) : Observable<ShopModel> {
    return this.httpClient.post<ShopModel>(`${API_URL}/api/v1/shop/shop-with-owner`, data).pipe(
      map(result => new ShopModel(result))
    );
  }

  updateShop(id: string, data: any) : Observable<ShopModel> {
    return this.httpClient.put<ShopModel>(`${API_URL}/api/v1/shop/shops/${id}`, data).pipe(
      map(result => new ShopModel(result))
    );
  }

  updateShopInfo(id: string, data: any) : Observable<ShopModel> {
    return this.httpClient.put<ShopModel>(`${API_URL}/api/v1/shop/shop-info/${id}`, data).pipe(
      map(result => new ShopModel(result))
    );
  }

  updateCategoriesAndKeywords(id: string, data: any) : Observable<ShopModel> {
    return this.httpClient.put<ShopModel>(`${API_URL}/api/v1/shop/categories-and-keywords/${id}`, data).pipe(
      map(result => new ShopModel(result))
    );
  }

  updateShopContactInfo(id: string, data: any) : Observable<ShopModel> {
    return this.httpClient.put<ShopModel>(`${API_URL}/api/v1/shop/shop-contact-info/${id}`, data).pipe(
      map(result => new ShopModel(result))
    );
  }

  updateShopEnabledStatus(id: string, data: any) : Observable<ShopModel> {
    return this.httpClient.put<ShopModel>(`${API_URL}/api/v1/shop/shop-enabled-status/${id}`, data).pipe(
      map(result => new ShopModel(result))
    );
  }

  updateShopMedia(id: string, data: any) : Observable<ShopModel> {
    return this.httpClient.put<ShopModel>(`${API_URL}/api/v1/shop/shop-media/${id}`, data).pipe(
      map(result => new ShopModel(result))
    );
  }

  deleteShop(id: string) : Observable<any> {
    return this.httpClient.delete(`${API_URL}/api/v1/shop/${id}`);
  }

  getShopById(id: string): Observable<ShopModel> {
    return this.httpClient.get<ShopModel>(`${API_URL}/api/v1/shop/${id}`).pipe(
      map((result: any) => {
        if (!result) return result;
        return new ShopModel(result);
      })
    );
  }
}