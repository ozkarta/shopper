import { FileModel } from './file.model'

export class MediaModel {
  originalImage?: FileModel | string;
  highResolutionImage?: FileModel | string;
  mediumResolutionImage?: FileModel | string;
  lowResolutionImage?: FileModel | string;
  _id!: string;
}