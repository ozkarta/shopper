import { LocalizationString } from './localization-string.model';
import * as _ from 'lodash';

export class CategoryModel {
  public _id!: string;

  public categoryName: LocalizationString = new LocalizationString();
  public friendlyId: LocalizationString = new LocalizationString();
  public includeInSearch!: boolean;
  public sortWeight!: number;

  public parentCategory!: CategoryModel | string;
  public childCategories: CategoryModel[] = [];

  public createdAt!: Date;
  public updatedAt!: Date;

  constructor(category?: CategoryModel) {
    if (category) {
      _.merge(this, category);
    }
  }
}