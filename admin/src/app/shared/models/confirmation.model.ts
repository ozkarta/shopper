export interface ConfirmationModel {
  title: string,
  question: string,
  noButtonText: string,
  yesButtonText: string
}