
import { LocalizationString } from './localization-string.model';
import * as _ from 'lodash';
import { ShopModel } from './shop.model';
import { CategoryModel } from './category.model';
import { MediaModel } from './media.model';

export class ProductModel {
  _id: string | null | undefined;
  shop!: ShopModel | string;
  
  friendlyId: LocalizationString = new LocalizationString();
  title: LocalizationString = new LocalizationString();
  shortDescription: LocalizationString = new LocalizationString();
  longDescription: LocalizationString = new LocalizationString();
  media!: ProductMediaItem[];

  quantity!: number;
  price!: number;
  currency!: string;


  categories!: CategoryModel[];
  keywords!: string[];
  
  // status: {type: String, enum: ['active', 'disabled', 'out_of_stock']},

  // ==============================================
  // discounts: [{
  //   currency: {
  //     type: String,
  //     enum: CountryEnum,
  //   },
  //   price: {
  //     type: Number,
  //     default: 0
  //   }
  // }],
  // rating: {}, // TODO
  // sales: [], // TODO
  // feedback: [], // TODO

  // SYSTEM
  enabled!: boolean;
  deleted!: boolean;

  createdAt!: Date;
  updatedAt!: Date;

  constructor(product?: ProductModel) {
    if (product) {
      _.merge(this, product);
    }

    if (!this.categories) {
      this.categories = [];
    }

    if (!this.keywords) {
      this.keywords = [];
    }

    if (!this.media) {
      this.media = [];
    }
  }
  
  public isEmptyProduct(): boolean {
    return !this._id;
  }

  public getProductImageId( _imageProperty?: 'originalImage' | 'highResolutionImage' | 'mediumResolutionImage' | 'lowResolutionImage'): string | null {
    let imageProperty = _imageProperty || 'originalImage';
    if (!this) return null
    if (!this.media || !this.media.length) return null
    let mainProductMedia: ProductMediaItem = this.media.filter((item: ProductMediaItem) => item.isPrimary)[0] || this.media[0] || null;
    if (!mainProductMedia || !mainProductMedia.item || !(<any>mainProductMedia).item[imageProperty]) return null

    return typeof (<any>mainProductMedia).item[imageProperty] === 'object' ? (<any>mainProductMedia).item[imageProperty]._id : (<any>mainProductMedia).item[imageProperty]
  }
}

export interface ProductMediaItem {
  item: MediaModel,
  isPrimary: Boolean,
}