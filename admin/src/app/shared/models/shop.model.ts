import { LocalizationString } from './localization-string.model';
import { MediaModel } from './media.model';
import * as _ from 'lodash';

export class ShopModel {
  public _id!: string;
  public createdAt!: Date;
  public updatedAt!: Date;

  public owner: any;

  public business: any = {};

  public title: LocalizationString = new LocalizationString();
  public friendlyId: LocalizationString = new LocalizationString();
  public shortDescription: LocalizationString = new LocalizationString();
  public longDescription: LocalizationString = new LocalizationString();
  public termsAndCondition: LocalizationString = new LocalizationString();

  public addresses: any[] = [];
  public phones: string[] = [];
  public emails: string[] = [];
  public facebook!: string;

  public categories!: any[];
  public keywords: string[] = [];
  // products may not be referenced here
  public products!: any[];

  public media!: MediaModel

  public rating: any; // TODO
  public sales!: any[]; // TODO
  public feedback!: any[]; // TODO

  constructor(store?: ShopModel) {
    if (store) {
      _.merge(this, store);
    }
    
    if (!this.addresses) {
      this.addresses = [];
    }

    if (!this.phones) {
      this.phones = [];
    }

    if (!this.emails) {
      this.emails = [];
    }

    if (!this.categories) {
      this.categories = [];
    }

    if (!this.keywords) {
      this.keywords = [];
    }

    if (!this.products) {
      this.products = [];
    }
  }

  public getShopInfo(): ShopInfo {
    return {
      title: this.title,
      friendlyId: this.friendlyId,
      shortDescription: this.shortDescription,
      longDescription: this.longDescription,
      termsAndCondition: this.termsAndCondition,
    }
  }

  public hasShopInfo() {
    return this.title.en && this.title.ge 
    && this.friendlyId.en && this.friendlyId.ge 
    && this.shortDescription.en && this.shortDescription.ge;
  }

  public isEmptyShop(): boolean {
    return !this._id;
  }
}

interface ShopInfo {
  title: LocalizationString;
  friendlyId: LocalizationString;
  shortDescription: LocalizationString;
  longDescription: LocalizationString;
  termsAndCondition: LocalizationString;
}
