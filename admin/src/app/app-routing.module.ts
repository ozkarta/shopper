import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

const routes = [
  {
    path: '',
    canActivate: [],
    children: [
      {
        path: '',
        loadChildren: () => import('./routes/main/main.module').then((m) => m.MainModule)
      },      
    ]
  },
  {
    path: '**',
    loadChildren: () => import('./routes/page-not-found/page-not-found.module').then((m) => m.PageNotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  declarations: [],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}