import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TokenInterceptor } from './shared/interceptors/token-interceptor.service';
import { AppComponent } from './app.component';

// Modules
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './shared/services/auth.service';
import { SpinnerService } from './shared/services/spinner.service';

export function initializeApp(authService: AuthService, spinnerService: SpinnerService, ) {
  return () => new Promise((resolve, reject) => {
    spinnerService.start();
    authService.loadUser().subscribe(
      currentUser => {
        spinnerService.stop();
        return resolve(currentUser)
      },
      error => {
        spinnerService.stop();
        return resolve(null);
      }
    )
  });
}
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    SharedModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AuthService, SpinnerService],
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
