import { NgModule } from '@angular/core';

import { MainRoutingModule } from './main-routing.module';
// import { ComponentsModule } from './components/components.module';
// import { CurrentUserResolver } from './main.resolver';
import { MainComponent } from './main.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  imports: [
    SharedModule,
    MainRoutingModule,
  ],
  declarations: [
    MainComponent,
  ],
  providers: [
  ]
})
export class MainModule { }
