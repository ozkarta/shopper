import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { CurrentUserResolver } from './main.resolver';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./routes/login/login.module').then(m => m.LoginModule)
      },
      {
        path: 'signed',
        loadChildren: () => import('./routes/signed/signed.module').then(m => m.SignedModule)
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
