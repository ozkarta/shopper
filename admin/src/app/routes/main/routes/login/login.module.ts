import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { SharedModule } from '../../../../shared/shared.module';
import { LoginAuthGuard } from './login-guard.service';

@NgModule({
  imports: [
    LoginRoutingModule,
    SharedModule,
  ],
  declarations: [
    LoginComponent
  ],
  providers: [
    LoginAuthGuard
  ]
})
export class LoginModule { }
