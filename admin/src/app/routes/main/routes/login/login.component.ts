import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnDestroy {
  public loginFormGroup!: FormGroup;

  constructor(private authService: AuthService,
    private router: Router,
    private fb: FormBuilder) {
  }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {

  }

  initForm() {
    this.loginFormGroup = this.fb.group({
      email: ['', [Validators.required], []],
      password: ['', [Validators.required]],
    });
  }

  submit() {
    if (!this.loginFormGroup.valid) return;
    this.authService.logInUser(this.loginFormGroup.value).subscribe(
      (currentUser: any) => {
        if (currentUser && this.authService.isSigned()) {
          this.router.navigate(['/signed']);
        }
      },
      (error) => {
        console.log(error);
      }
    );

  }
}