import { Component, OnInit, OnDestroy } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sidenav-content-component',
  templateUrl: './sidenav-content.component.html',
  styleUrls: ['./sidenav-content.component.css']
})

export class SidenavContentComponent implements OnInit, OnDestroy {
  private TREE_DATA: any[] = [
    {
      name: 'Administration',
      children: [
        {
          name: 'Categories',
          route: '/signed/administration/categories'
        },
        {
          name: 'Businesses Moderation',
          route: '/signed/administration/business-moderation'
        },
        {
          name: 'Products Moderation',
          route: '/signed/administration/product-moderation'
        },
        {
          name: 'New Category Request Moderation',
          route: '/signed/administration/new-category-request-moderation'
        },
      ]
    },
    {
      name: 'Clients',
      children: [
      ]
    },
    {
      name: 'Sellers',
      children: [
        {
          name: 'Market Managers',
          route: '/signed/sellers/market-managers'
        },
        // {
        //   name: 'Businesses',
        //   route: '/signed/sellers/businesses'
        // },
        {
          name: 'Shops',
          route: '/signed/sellers/shops'
        },
        {
          name: 'Products',
          route: '/signed/sellers/products'
        }
      ]
    },
    {
      name: 'Statistics',
      children: [
        {
          name: 'Error Logs',
          route: '/signed/statistics/error-logs'
        }
      ]
    }
  ];

  private _transformer = (node: any, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      route: node.route,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<any>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  hasChild = (_: number, node: any) => node.expandable;

  constructor(private titleService: Title,
    private router: Router) {
  }

  ngOnInit() {
    this.dataSource.data = this.TREE_DATA;
  }

  ngOnDestroy() {

  }

  routeClickHandler(navItem: any) {
    this.titleService.setTitle(navItem.name);
    this.router.navigate([navItem.route]);
  }

}