import { NgModule } from '@angular/core';
import { SignedComponent } from './signed.component';
import { SignedRoutingModule } from './signed-routing.module';
import { SharedModule } from '../../../../shared/shared.module';
import { LoggedInAsAdminAuthGuard } from './signed-guard.service';

import { SidenavContentComponent } from './components/sidenav/sidenav-content.component';

@NgModule({
  imports: [
    SignedRoutingModule,
    SharedModule,
  ],
  declarations: [
    SignedComponent,
    SidenavContentComponent
  ],
  providers: [
    LoggedInAsAdminAuthGuard
  ]
})
export class SignedModule { }
