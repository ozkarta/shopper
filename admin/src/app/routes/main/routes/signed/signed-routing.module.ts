import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignedComponent } from './signed.component';
import { LoggedInAsAdminAuthGuard } from './signed-guard.service';

const routes: Routes = [
  {
    path: '',
    component: SignedComponent,
    canActivate: [
      LoggedInAsAdminAuthGuard
    ],
    children: [
      {
        path: 'administration',
        loadChildren: () => import('./routes/administration/administration.module').then(m => m.AdministrationModule)
      },
      {
        path: 'sellers',
        loadChildren: () => import('./routes/sellers/sellers.module').then(m => m.SellersModule)
      },
      {
        path: 'statistics',
        loadChildren: () => import('./routes/statistics/statistics.module').then(m => m.StatisticsModule)
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignedRoutingModule { }
