import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, BehaviorSubject, Observable, of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { ProductAPIService } from '../../../../../../../../shared/api/product-api.service';
import { ProductModel } from '../../../../../../../../shared/models/product.model';
import { SpinnerService } from '../../../../../../../../shared/services/spinner.service';
import { FormDialogComponent } from './dialogs/form/form-dialog.component';
import { ConfirmationModel } from 'src/app/shared/models/confirmation.model';
import { ConfirmationDialogComponent } from 'src/app/shared/dialogs/confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'app-administration-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit, OnDestroy {
  public numTotal = 0;
  public products: ProductModel[] = [];
  public query: any = {
    keyword: '',
    page: 1,
    limit: 10,
  };

  private subscription: Subscription = new Subscription();


  private loadData: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private spinnerService: SpinnerService,
    private dialog: MatDialog,
    private productAPIService: ProductAPIService) {
  }

  ngOnInit() {
    this.subscription.add(this.initLoadData().subscribe());
    this.loadData.next(true);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setInitialStateOfQuery() {
    this.query = {
      keyword: '',
      page: 1,
      limit: 10,
    };
  }

  initLoadData(): Observable<any> {
    return this.loadData.pipe(
      switchMap((update: boolean) => {
        if (update) {
          this.spinnerService.start();
          return this.productAPIService.getProductsByQuery(this.query).pipe(
            map(({items, numTotal}) => {
              this.products = items || [];
              this.numTotal = numTotal || 0;
              this.spinnerService.stop();
              return items;
            }),
            catchError(error => {
              this.spinnerService.stop();
              throw error;
            })
          )
        } else {
          return of(null);
        }
      })
    );
  }

  createHandler() {
    let product = new ProductModel();
    this.openProductFormDialog(undefined, product, (formData: any) => {
      this.setInitialStateOfQuery();
      this.loadData.next(true);
    });
  }

  filterChange(event: any) {
    
  }

  // ==============================
  openProductFormDialog(type: string = 'create', formData: any = {}, afterClosed: (formData: any) => void): void {
    const dialogRef = this.dialog.open(FormDialogComponent, {
      width: '70%',
      height: '80%',
      data: {
        type,
        formData
      }
    });

    dialogRef.afterClosed().subscribe(afterClosed);
  }

  editHandler(product: ProductModel) {
    this.openProductFormDialog('update', product, (formData: any) => {
      this.loadData.next(true);
    });
  }

  deleteHandler(product: ProductModel) {
    this.openConfirmationDialog({
      title: 'Delete Product',
      question: 'Are you sure you want to delete Product?',
      noButtonText: 'Close',
      yesButtonText: 'Yes, Delete',
    }, (result) => {
      result && this.deleteProduct(product);
    })
    
  }

  deleteProduct(product: ProductModel) {
    if (!product?._id) return;
    this.spinnerService.start();
    this.productAPIService.deleteProduct(product._id).pipe(
      map((result: any) => {
        this.setInitialStateOfQuery();
        this.loadData.next(true);
        return result;
      }),
      catchError((error: any) => {
        console.log(error);
        return of(null);
      })
    ).subscribe(
      () => this.spinnerService.stop()
    );
  } 

  enableStatusChangeHandler(data: {enabled: boolean, _id: string}) {
    if (!data || !data._id) return;
    this.productAPIService.updateProductEnabledStatus(data._id, {enabled: data.enabled}).pipe(
      tap(() => this.spinnerService.start()),
      map((result: ProductModel) => this.loadData.next(true)),
      catchError((error: Error) => {
        this.spinnerService.stop();
        this.loadData.next(true);
        throw error;
      }),
      tap(() => this.spinnerService.stop())
    ).subscribe();
  }

  openConfirmationDialog(data: ConfirmationModel, afterClosed: (data: any) => void): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '30%',
      data
    });
    dialogRef.afterClosed().subscribe(afterClosed);
  }
}