import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../shared/shared.module';
import { NewCategoryRequestModerationComponent } from './new-category-request-moderation.component';
import { NewCategoryRequestModerationRoutingModule } from './new-category-request-moderation-routing.module';

@NgModule({
  imports: [
    SharedModule,
    NewCategoryRequestModerationRoutingModule
  ],
  declarations: [
    NewCategoryRequestModerationComponent
  ],
  providers: [
  ],
  exports: [
  ]
})
export class NewCategoryRequestModerationModule { }
