import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../shared/shared.module';

// import { FilterComponent } from './filter/filter.component';
import { ToolbatComponent } from './toolbar/toolbar.component';
@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    // FilterComponent,
    ToolbatComponent
  ],
  providers: [
  ],
  exports: [
    // FilterComponent,
    ToolbatComponent
  ]
})
export class ComponentsModule { }
