import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../shared/shared.module';
import { ShopDetailsRoutingModule } from './shop-details-routing.module';
import { ShopSubFormsModule } from '../shared/components/shop/shop.module';
import { ShopDetailsComponent } from './shop-details.component';
import { ComponentsModule } from './components/components.module';
//
import { ShopResolver, ShopProductsResolver } from './shop-details-resolver.service';

@NgModule({
  imports: [
    SharedModule,
    ShopDetailsRoutingModule,
    ShopSubFormsModule,
    ComponentsModule,
  ],
  declarations: [
    ShopDetailsComponent
  ],
  providers: [
    ShopResolver,
    ShopProductsResolver
  ],
  exports: [
  ]
})
export class ShopDetailsModule { }
