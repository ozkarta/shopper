import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { CategoriesAPIService } from '../../../../../../../../shared/api/categories-api.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';

@Injectable()

export class AllCategoriesResolver implements Resolve<Observable<any>> {

  constructor(private categoryAPIService: CategoriesAPIService, private spinnerService: SpinnerService) { }

  resolve() {
    this.spinnerService.start();
    return this.categoryAPIService.getAllCategories(20).pipe(
      map(categories => {
        this.spinnerService.stop();
        return categories;
      }),
      catchError(error => {
        this.spinnerService.stop();
        return of([]);
      })
    )
  }
}
