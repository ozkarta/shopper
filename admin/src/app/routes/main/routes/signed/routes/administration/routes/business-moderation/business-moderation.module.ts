import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../shared/shared.module';
import { BusinessModerationComponent } from './business-moderation.component';
import { BusinessModerationRoutingModule } from './business-moderation-routing.module';

@NgModule({
  imports: [
    SharedModule,
    BusinessModerationRoutingModule
  ],
  declarations: [
    BusinessModerationComponent
  ],
  providers: [
  ],
  exports: [
  ]
})
export class BusinessModerationModule { }
