import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { generateFriendlyId } from '../../../../../../../../../../../shared/utils/functions';
import { validateAllFormFields } from '../functions';
import { ShopAPIService } from '../../../../../../../../../../../shared/api/shop-api.service';
import { SpinnerService } from '../../../../../../../../../../../shared/services/spinner.service';
import { LocalizationString } from '../../../../../../../../../../../shared/models/localization-string.model';
import { ShopModel } from '../../../../../../../../../../../shared/models/shop.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-shop-info-form-component',
  templateUrl: './shop-info-form.component.html',
  styleUrls: ['./shop-info-form.component.css']
})

export class ShopInfoFormComponent implements OnInit, OnDestroy {
  @Input('formData') formData!: ShopModel;
  // @Input('stepper') stepper: MatStepper;
  @Output('shopChangeHandler') shopChangeEmitter: EventEmitter<any> = new EventEmitter();
  private subscription: Subscription = new Subscription();

  public addShopInfoFormGroup!: FormGroup;
  private addShopInfoFormGroupSubmitted: boolean = false;
  private addShopInfoFormGroupLastValueCopy = null;
  private addShopInfoFormGroupConfig: any;

  constructor(private fb: FormBuilder,
    private shopAPIService: ShopAPIService,
    private spinnerService: SpinnerService) { }

  initFormConfig() {
    this.addShopInfoFormGroupConfig = {
      titleEN: [this.formData.title.en, Validators.required],
      titleGE: [this.formData.title.ge, Validators.required],

      friendlyIdEN: [this.formData.friendlyId.en, Validators.required],
      friendlyIdGE: [this.formData.friendlyId.ge, Validators.required],

      shortDescriptionEN: [this.formData.shortDescription.en, Validators.required],
      shortDescriptionGE: [this.formData.shortDescription.ge, Validators.required],

      longDescriptionEN: [this.formData.longDescription.en, Validators.required],
      longDescriptionGE: [this.formData.longDescription.ge, Validators.required],

      termsAndConditionEN: [this.formData.termsAndCondition.en, Validators.required],
      termsAndConditionGE: [this.formData.termsAndCondition.ge, Validators.required],
    }
  }

  ngOnInit() {
    this.initFormConfig();

    this.addShopInfoFormGroup = this.fb.group(this.addShopInfoFormGroupConfig);

    // if (this.formData && this.formData.hasShopInfo()) {
    //   this.addShopInfoFormGroupSubmitted = true;
    //   this.addShopInfoFormGroupLastValueCopy = this.formData.getShopInfo();
    //   let timeout = setTimeout(() => {
    //     this.stepper.selected.completed = true;
    //     clearTimeout(timeout);
    //   }, 500);
    // }

    this.subscribeAddShopInfoFormGroupValueChanges();
    this.subscribeAddShopInfoFormGroupControlValueChanges();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  // ====================================SUBSCRIPTIONS=================================================

  subscribeAddShopInfoFormGroupControlValueChanges() {
    this.subscription.add(
      this.addShopInfoFormGroup.controls['titleEN'].valueChanges.pipe(
        map(value => {
          this.addShopInfoFormGroup.controls['friendlyIdEN'].setValue(generateFriendlyId(value));
        })
      ).subscribe()
    );
    this.subscription.add(
      this.addShopInfoFormGroup.controls['titleGE'].valueChanges.pipe(
        map(value => {
          this.addShopInfoFormGroup.controls['friendlyIdGE'].setValue(generateFriendlyId(value));
        })
      ).subscribe()
    )
  }

  subscribeAddShopInfoFormGroupValueChanges() {
    this.subscription.add(
      this.addShopInfoFormGroup.valueChanges.pipe(
        map((value: any) => {
          // this.stepper.selected.completed =
          //   this.addShopInfoFormGroupSubmitted && _.isEqual(this.generateFormData(), value) && this.addShopInfoFormGroup.valid;
        })
      ).subscribe()
    );
  }
  // ==================================================================================================
  submitShopInfoForm() {
    if (!this.addShopInfoFormGroup.valid) {
      validateAllFormFields(this.addShopInfoFormGroup);
      return;
    }

    let data = this.composeShopInfoObject();

    this.spinnerService.start();
    this.shopAPIService.updateShopInfo(this.formData._id, data).pipe(
      map((shop: ShopModel) => {
        this.shopChangeEmitter.emit(shop);
        this.spinnerService.stop();
        this.addShopInfoFormGroupSubmitted = true;
        this.addShopInfoFormGroupLastValueCopy = this.addShopInfoFormGroup.value;

        // this.stepper.selected.completed = true;
        // this.stepper.next();
      }),
      catchError(error => {
        throw error;
      })
    ).subscribe(
      (result: any) => {
        this.spinnerService.stop();
        // this.stepper.next();
      },
      (error: any) => {
        this.spinnerService.stop();
      }
    );
  }

  composeShopInfoObject() {
    let data: ShopModel = <ShopModel>{};
    data.title = new LocalizationString(this.addShopInfoFormGroup.value.titleEN, this.addShopInfoFormGroup.value.titleGE);
    data.friendlyId = new LocalizationString(this.addShopInfoFormGroup.value.friendlyIdEN, this.addShopInfoFormGroup.value.friendlyIdGE);
    data.shortDescription = new LocalizationString(this.addShopInfoFormGroup.value.shortDescriptionEN, this.addShopInfoFormGroup.value.shortDescriptionGE);
    data.longDescription = new LocalizationString(this.addShopInfoFormGroup.value.longDescriptionEN, this.addShopInfoFormGroup.value.longDescriptionGE);
    data.termsAndCondition = new LocalizationString(this.addShopInfoFormGroup.value.termsAndConditionEN, this.addShopInfoFormGroup.value.termsAndConditionGE);
    return data;
  }

  generateFormData() {
    return {
      titleEN: this.formData.title.en,
      titleGE: this.formData.title.ge,

      friendlyIdEN: this.formData.friendlyId.en,
      friendlyIdGE: this.formData.friendlyId.ge,

      shortDescriptionEN: this.formData.shortDescription.en,
      shortDescriptionGE: this.formData.shortDescription.ge,

      longDescriptionEN: this.formData.longDescription.en,
      longDescriptionGE: this.formData.longDescription.ge,

      termsAndConditionEN: this.formData.termsAndCondition.en,
      termsAndConditionGE: this.formData.termsAndCondition.ge,
    }
  }
}