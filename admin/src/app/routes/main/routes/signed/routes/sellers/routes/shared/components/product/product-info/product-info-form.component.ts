import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { map, catchError, debounceTime, startWith, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { generateFriendlyId } from 'src/app/shared/utils/functions';
import { validateAllFormFields } from '../functions';
import { ProductAPIService } from 'src/app/shared/api/product-api.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { LocalizationString } from 'src/app/shared/models/localization-string.model';
import * as _ from 'lodash';
import { ProductModel } from 'src/app/shared/models/product.model';
import { ShopModel } from 'src/app/shared/models/shop.model';
import { ShopAPIService } from 'src/app/shared/api/shop-api.service';

@Component({
  selector: 'app-product-info-form-component',
  templateUrl: './product-info-form.component.html',
  styleUrls: ['./product-info-form.component.css']
})

export class ProductInfoFormComponent implements OnInit, OnDestroy {
  @Input('formData') formData!: ProductModel;
  // @Input('stepper') stepper: MatStepper;
  @Output('productChangeHandler') productChangeEmitter: EventEmitter<any> = new EventEmitter();
  private subscription: Subscription = new Subscription();

  public addProductInfoFormGroup!: FormGroup;
  private addProductInfoFormGroupConfig: any;

  public shops!: Observable<ShopModel[]>;

  constructor(private fb: FormBuilder,
    private productAPIService: ProductAPIService,
    private spinnerService: SpinnerService,
    private shopAPIService: ShopAPIService) { }

  initFormConfig() {
    // let shopId = (this.formData.shop && typeof this.formData.shop === 'object') ? this.formData.shop._id : this.formData.shop;
    
    this.addProductInfoFormGroupConfig = {
      shop: [this.formData.shop, Validators.required],

      titleEN: [this.formData.title.en, Validators.required],
      titleGE: [this.formData.title.ge, Validators.required],

      friendlyIdEN: [this.formData.friendlyId.en, Validators.required],
      friendlyIdGE: [this.formData.friendlyId.ge, Validators.required],

      shortDescriptionEN: [this.formData.shortDescription.en, Validators.required],
      shortDescriptionGE: [this.formData.shortDescription.ge, Validators.required],

      longDescriptionEN: [this.formData.longDescription.en, Validators.required],
      longDescriptionGE: [this.formData.longDescription.ge, Validators.required],

    }
  }

  ngOnInit() {
    this.initFormConfig();
    this.addProductInfoFormGroup = this.fb.group(this.addProductInfoFormGroupConfig);
    this.initShopTypeaheadValueChanges();

    this.subscribeAddProductInfoFormGroupValueChanges();
    this.subscribeAddProductInfoFormGroupControlValueChanges();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  // ====================================SUBSCRIPTIONS=================================================
  initShopTypeaheadValueChanges() {
    this.shops = this.addProductInfoFormGroup.controls['shop'].valueChanges.pipe(
      debounceTime(500),
      startWith(''),
      distinctUntilChanged(),
      switchMap((keyword: string) => {
        return this.queryShops({keyword, page: 1, limit: 10})
      }),
    )
  }

  subscribeAddProductInfoFormGroupControlValueChanges() {
    this.subscription.add(
      this.addProductInfoFormGroup.controls['titleEN'].valueChanges.pipe(
        map(value => {
          this.addProductInfoFormGroup.controls['friendlyIdEN'].setValue(generateFriendlyId(value));
        })
      ).subscribe()
    );
    this.subscription.add(
      this.addProductInfoFormGroup.controls['titleGE'].valueChanges.pipe(
        map(value => {
          this.addProductInfoFormGroup.controls['friendlyIdGE'].setValue(generateFriendlyId(value));
        })
      ).subscribe()
    )
  }

  subscribeAddProductInfoFormGroupValueChanges() {
    this.subscription.add(
      this.addProductInfoFormGroup.valueChanges.pipe(
        map((value: any) => {
          // this.stepper.selected.completed =
          //   this.addShopInfoFormGroupSubmitted && _.isEqual(this.generateFormData(), value) && this.addProductInfoFormGroup.valid;
        })
      ).subscribe()
    );
  }
  // ==================================================================================================

  shopDisplayFunction(shop: ShopModel) {
    if (!shop) return '';
    if (typeof shop === 'string') return shop;
    return `${shop.title? shop.title.en : ''}`
  }

  queryShops(query: any): Observable<ShopModel[]> {
    return this.shopAPIService.getShopsByQuery(query).pipe(
      map(({items, numTotal}) => {
        return items;
      })
    );
  }

  submitProductInfoForm() {
    if (!this.addProductInfoFormGroup.valid) {
      validateAllFormFields(this.addProductInfoFormGroup);
      return;
    }
    let observable: Observable<any>;
    let data = this.composeProductInfoObject();
    if (!this.formData._id) {
      observable = this.productAPIService.createProductWithShop(data)
    } else {
      observable = this.productAPIService.updateProductInfo(this.formData._id, data);
    }

    observable = observable.pipe(
      map((product: ProductModel) => {
        this.productChangeEmitter.emit(product);
        this.spinnerService.stop();
      }),
      catchError(error => {
        throw error;
      })
    );

    this.spinnerService.start();
    observable.subscribe(
      (result: any) => {
        this.spinnerService.stop();
      },
      (error: any) => {
        console.log(error);
        this.spinnerService.stop();
      }
    );
  }

  composeProductInfoObject() {
    let data: ProductModel = <ProductModel>{};
    // if (this.formData.shop) {
    //   data.shop = (typeof this.formData.shop === 'string') ?  this.formData.shop : this.formData.shop._id;
    // }

    
    data.shop = this.addProductInfoFormGroup.value.shop._id;
    data.title = new LocalizationString(this.addProductInfoFormGroup.value.titleEN, this.addProductInfoFormGroup.value.titleGE);
    data.friendlyId = new LocalizationString(this.addProductInfoFormGroup.value.friendlyIdEN, this.addProductInfoFormGroup.value.friendlyIdGE);
    data.shortDescription = new LocalizationString(this.addProductInfoFormGroup.value.shortDescriptionEN, this.addProductInfoFormGroup.value.shortDescriptionGE);
    data.longDescription = new LocalizationString(this.addProductInfoFormGroup.value.longDescriptionEN, this.addProductInfoFormGroup.value.longDescriptionGE);
    return data;
  }

  generateFormData() {
    return {
      titleEN: this.formData.title.en,
      titleGE: this.formData.title.ge,

      friendlyIdEN: this.formData.friendlyId.en,
      friendlyIdGE: this.formData.friendlyId.ge,

      shortDescriptionEN: this.formData.shortDescription.en,
      shortDescriptionGE: this.formData.shortDescription.ge,

      longDescriptionEN: this.formData.longDescription.en,
      longDescriptionGE: this.formData.longDescription.ge,
      
    }
  }
}