import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellersComponent } from './sellers.component';
import { HasSellerPermissionGuard } from './sellers-guard.service';

const routes: Routes = [
  {
    path: '',
    component: SellersComponent,
    canActivate: [
      HasSellerPermissionGuard
    ],
    children: [
      {
        path: 'market-managers',
        loadChildren: () => import('./routes/market-managers/market-managers.module').then(m => m.MarketManagersModule)
      },
      {
        path: 'businesses',
        loadChildren: () => import('./routes/businesses/businesses.module').then(m => m.BusinessesModule)
      },
      {
        path: 'shops',
        loadChildren: () => import('./routes/shops/shops.module').then(m => m.ShopsModule)
      },
      {
        path: 'shops/:shopId',
        loadChildren: () => import('./routes/shop-details/shop-details.module').then(m => m.ShopDetailsModule)
      },
      {
        path: 'products',
        loadChildren: () => import('./routes/products/products.module').then(m => m.ProductsModule)
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellersRoutingModule { }
