import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../shared/shared.module';

import { ListComponent } from './list/list.component';
import { ToolbatComponent } from './toolbar/toolbar.component';
@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    ListComponent,
    ToolbatComponent
  ],
  providers: [
  ],
  exports: [
    ListComponent,
    ToolbatComponent
  ]
})
export class ComponentsModule { }
