import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../../../../../shared/shared.module';
import { FileContentViewerComponent } from './file-content-viewer/file-content-viewer.component';
@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    FileContentViewerComponent,
  ],
  entryComponents: [
    FileContentViewerComponent,
  ]
})
export class DialogModule { }
