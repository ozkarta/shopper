import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectorRef, OnChanges } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { MatChipInputEvent } from '@angular/material/chips';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { countryObjectValidator } from '../custom-validators';
import { distinctUntilChanged, debounceTime, startWith, map, catchError } from 'rxjs/operators';
import { CountryModel } from '../../../../../../../../../../../shared/models/country.model';
import { countries } from '../../../../../../../../../../../shared/constants/country';
import { validateAllFormFields } from '../functions';
import { SpinnerService } from '../../../../../../../../../../../shared/services/spinner.service';
import { ShopAPIService } from '../../../../../../../../../../../shared/api/shop-api.service';
import { emailRegExp } from '../../../../../../../../../../../shared/constants/regexps';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ShopModel } from '../../../../../../../../../../../shared/models/shop.model';

@Component({
  selector: 'app-contact-info-form',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.css']
})

export class ContactInfoFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input('formData') formData!: ShopModel;
  @Output('shopChangeHandler') shopChangeEmitter: EventEmitter<any> = new EventEmitter();
  // @Input('stepper') stepper: MatStepper;
  private subscription: Subscription = new Subscription();


  public countries!: Observable<CountryModel[] | undefined>;
  public phoneChipsConfig: any = {
    visible: true,
    selectable: true,
    removable: true,
    addOnBlur: true,
    separatorKeysCodes: [ENTER, COMMA],
  }

  public emailChipsConfig: any = {
    visible: true,
    selectable: true,
    removable: true,
    addOnBlur: true,
    separatorKeysCodes: [ENTER, COMMA],
  }

  // Forms
  public shopContactFormGroup!: FormGroup;
  private shopContactFormGroupSubmitted: boolean = false;
  private shopContactFormGroupLastValueCopy: any = null;
  private shopContactFormGroupConfig: any = {};

  public shopContactAddressFormGroup!: FormGroup;
  private shopContactAddressFormGroupConfig = {
    street: ['', [Validators.required]],
    city: ['', [Validators.required]],
    province: [''],
    country: ['', [Validators.required, countryObjectValidator]],
    zip: ['', [Validators.required]],
  };

  // Table Field COnfig
  // =====================
  public addressColumnsToDisplay = [
    'street',
    'city',
    'province',
    'country',
    'zip',
    'action'
  ];
  public addressDisplayColumnTranslations: any = {
    'street': {
      title: 'Street',
    },
    'city': {
      title: 'City',
    },
    'province': {
      title: 'Province',
    },
    'country': {
      title: 'Country',
      transformer: (element: any) => {
        return `${element.country.name} (${element.country.alpha_2})`;
      }
    },
    'zip': {
      title: 'ZIP'
    }
  }
  // =====================

  constructor(private fb: FormBuilder,
    private shopAPIService: ShopAPIService,
    private spinnerService: SpinnerService,
    private changeDetectorRefs: ChangeDetectorRef) {}

  ngOnInit() {
    this.shopContactFormGroupConfig = {
      facebook: [this.formData.facebook, []],
    };
    this.shopContactFormGroup = this.fb.group(this.shopContactFormGroupConfig);
    this.shopContactAddressFormGroup = this.fb.group(this.shopContactAddressFormGroupConfig);

    this.subscribeShopContactAddressFormGroupControlValueChanges();
  }

  ngOnChanges(changes: any) {
    if (changes && changes.formData) {
      this.shopContactFormGroup && this.shopContactFormGroup.controls && this.shopContactFormGroup.controls.facebook && this.shopContactFormGroup.controls.facebook.setValue(this.formData.facebook);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  // =======================================================================================

  subscribeShopContactAddressFormGroupControlValueChanges() {
    this.countries = this.shopContactAddressFormGroup.controls['country'].valueChanges.pipe(
      debounceTime(500),
      startWith(''),
      distinctUntilChanged(),
      map(keyword => this.filterCountries(keyword)),
    )
  }
  // ========================================================================================

  addAddress() {
    if (!this.shopContactAddressFormGroup.valid) {
      validateAllFormFields(this.shopContactAddressFormGroup);
      return;
    }
    this.formData.addresses = [...this.formData.addresses, this.shopContactAddressFormGroup.value];
    this.shopContactAddressFormGroup.reset();
  }


  addPhone(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim() && this.formData.phones.indexOf(value) < 0) {
      this.formData.phones.push(value.trim());
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removePhone(phone: string): void {
    const index = this.formData.phones.indexOf(phone);
    if (index >= 0) {
      this.formData.phones.splice(index, 1);
    }
  }

  addEmail(event: MatChipInputEvent): void {
    const input = event.input;
    const value = (event.value || '').trim();
    if (value && this.formData.emails.indexOf(value) < 0 && value.match(emailRegExp)) {
      this.formData.emails.push(value);
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeEmail(email: string): void {
    const index = this.formData.emails.indexOf(email);
    if (index >= 0) {
      this.formData.emails.splice(index, 1);
    }
  }

  filterCountries(countryName: string) {
    if (typeof countryName === 'object') return;
    return countries.filter((country: CountryModel) => (country.name || '').toLocaleLowerCase().startsWith(countryName.toLocaleLowerCase()));
  }

  countryDisplayFunction(country: CountryModel) {
    if (!country) return '';
    if (typeof country === 'string') return country;
    return `${country.name} (${country.alpha_2})`;
  }
  // ========================================================================================

  submitShopContactForm() {
    if (!this.shopContactFormGroup.valid) {
      validateAllFormFields(this.shopContactFormGroup);
      return;
    }

    let data = Object.assign({}, this.shopContactFormGroup.value);
    data.addresses = this.formData.addresses;
    data.phones = this.formData.phones;
    data.emails = this.formData.emails;

    this.spinnerService.start();
    this.shopAPIService.updateShopContactInfo(this.formData._id, data).pipe(
      map((shop: ShopModel) => {
        this.shopChangeEmitter.emit(shop);
      }),
      catchError(error => {
        throw error;
      })
    )
    .subscribe(
      (result: any) => {
        this.spinnerService.stop();
      },
      (error: any) => {
        console.log(error);
        this.spinnerService.stop();
      }
    )
  }

  removeAddress(item: any) {
    if (!item) return;
    let index = this.formData.addresses.indexOf(item);
    if (index < 0) return;
    this.formData.addresses.splice(index, 1);
    this.formData.addresses = this.formData.addresses.slice();
  }
}