import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { ShopModel } from '../../../../../../../../shared/models/shop.model';
import { ProductModel } from '../../../../../../../../shared/models/product.model'
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Subscription, of } from 'rxjs';
import { ShopAPIService } from '../../../../../../../../shared/api/shop-api.service';
import { map, catchError } from 'rxjs/operators';
import { SpinnerService } from '../../../../../../../../shared/services/spinner.service';

@Component({
  selector: 'app-shop-details',
  templateUrl: './shop-details.component.html',
  styleUrls: ['./shop-details.component.css']
})

export class ShopDetailsComponent implements OnInit, OnDestroy {
  public shop?: ShopModel;
  public shopProducts!: ProductModel[];
  private shopId!: string;
  private activatedRouteSnapshot: ActivatedRouteSnapshot;

  private subscription: Subscription = new Subscription();

  constructor(
    private route: ActivatedRoute,
    private shopAPIService: ShopAPIService,
    private activatedRoute: ActivatedRoute,
    private spinnerService: SpinnerService
  ) {
    this.activatedRouteSnapshot = activatedRoute.snapshot;
  }

  ngOnInit() {
    this.shopId = this.activatedRouteSnapshot.paramMap.get('shopId') || '';
    this.shop = this.route.snapshot.data['shop'];
    this.shopProducts = this.route.snapshot.data['shopProducts'];
  }
  
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  selectedTabChange(event: MatTabChangeEvent) {
    // const {tab} = event;
    // if (!tab.textLabel.toLowerCase().match('product')) return
    // console.log('Product');
  }

  loadShop() {
    this.spinnerService.start();
    this.shopAPIService.getShopById(this.shopId).pipe(
      map((shop: ShopModel) => {
        this.spinnerService.stop();
        this.shop = shop;
        return null;
      }),
      catchError((error: any) => {
        console.log(error);
        this.spinnerService.stop();
        this.shop = undefined; //TODO or make another handler ...
        return of(null);
      })
    ).subscribe();
  }

  loadProducts() {

  }

  shopChangeHandler(load: any) {
    if (!load) return;
    this.loadShop();
  }
}