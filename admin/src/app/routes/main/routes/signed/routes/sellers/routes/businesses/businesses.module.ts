import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../shared/shared.module';
import { BusinessesComponent } from './businesses.component';
import { BusinessesRoutingModule } from './businesses-routing.module';

@NgModule({
  imports: [
    SharedModule,
    BusinessesRoutingModule
  ],
  declarations: [
    BusinessesComponent
  ],
  providers: [
  ],
  exports: [
  ]
})
export class BusinessesModule { }
