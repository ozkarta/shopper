import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../shared/shared.module';
import { StatisticsComponent } from './statistics.component';
import { StatisticsRoutingModule } from './statistics-routing.module';
import { HasStatisticsPermissionGuard } from './statistics-guard.service';

@NgModule({
  imports: [
    SharedModule,
    StatisticsRoutingModule
  ],
  declarations: [
    StatisticsComponent
  ],
  providers: [
    HasStatisticsPermissionGuard
  ]
})
export class StatisticsModule { }
