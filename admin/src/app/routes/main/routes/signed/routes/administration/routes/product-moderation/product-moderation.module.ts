import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../shared/shared.module';
import { ProductModerationComponent } from './product-moderation.component';
import { ProductModerationRoutingModule } from './product-moderation-routing.module';

@NgModule({
  imports: [
    SharedModule,
    ProductModerationRoutingModule
  ],
  declarations: [
    ProductModerationComponent
  ],
  providers: [
  ],
  exports: [
  ]
})
export class ProductModerationModule { }
