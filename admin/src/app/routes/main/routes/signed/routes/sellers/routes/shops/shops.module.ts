import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../shared/shared.module';
import { ShopsComponent } from './shops.component';
import { ShopsRoutingModule } from './shops-routing.module';
import { ComponentsModule } from './components/components.module';
import { DialogsModule } from './dialogs/dialogs.module';

@NgModule({
  imports: [
    SharedModule,
    ShopsRoutingModule,
    ComponentsModule,
    DialogsModule
  ],
  declarations: [
    ShopsComponent
  ],
  providers: [
  ],
  exports: [
  ]
})
export class ShopsModule { }
