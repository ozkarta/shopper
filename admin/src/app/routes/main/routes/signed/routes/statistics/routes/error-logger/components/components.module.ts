import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { FiltersComponent } from './filters/filters.component';
import { FileListComponent } from './file-list/file-list.component';
import { FolderListComponent } from './folder-list/folder-list.component';

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    FiltersComponent,
    FileListComponent,
    FolderListComponent,
  ],
  exports: [
    FiltersComponent,
    FileListComponent,
    FolderListComponent,
  ]
})
export class ComponentsModule { }
