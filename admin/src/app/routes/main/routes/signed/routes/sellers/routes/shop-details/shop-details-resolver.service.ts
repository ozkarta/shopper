import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { SpinnerService } from '../../../../../../../../shared/services/spinner.service';
import { ShopAPIService } from '../../../../../../../../shared/api/shop-api.service';
import { ShopModel } from '../../../../../../../../shared/models/shop.model';

@Injectable()
export class ShopResolver implements Resolve<Observable<any>> {

  constructor(private shopAPIService: ShopAPIService, private spinnerService: SpinnerService) { }

  resolve(route: ActivatedRouteSnapshot) {
    let shopId: string = route.paramMap.get('shopId') || '';
    this.spinnerService.start();
    return this.shopAPIService.getShopById(shopId).pipe(
      map((shop: ShopModel) => {
        this.spinnerService.stop();
        if (!shop) return null;
        return shop;
      }),
      catchError(error => {
        this.spinnerService.stop();
        return of(null);
      })
    )
  }
}

@Injectable()
export class ShopProductsResolver implements Resolve<Observable<any>> {

  constructor(private shopAPIService: ShopAPIService, private spinnerService: SpinnerService) { }

  resolve(route: ActivatedRouteSnapshot) {
    // let shopId: string = route.paramMap.get('shopId') || '';
    // this.spinnerService.start();
    // return this.shopAPIService.getShopById(shopId).pipe(
    //   map((shop: ShopModel) => {
    //     this.spinnerService.stop();
    //     return shop;
    //   }),
    //   catchError(error => {
    //     this.spinnerService.stop();
    //     return of([]);
    //   })
    // )
    return of ([]);
  }
}
