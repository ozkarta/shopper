import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusinessModerationComponent } from './business-moderation.component';

const routes: Routes = [
  {
    path: '',
    component: BusinessModerationComponent,
    canActivate: [
    ],
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessModerationRoutingModule { }
