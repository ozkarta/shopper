import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss'],
})
export class FileListComponent {
  @Input() items!: any[];
  @Input() activeFolder: any;
  @Output('folderSelectHandler') folderSelectEmitter: EventEmitter<any> = new EventEmitter(); 
  @Output('fileSelectHandler') fileSelectEmitter: EventEmitter<any> = new EventEmitter(); 

  goBack() {
    this.folderSelectEmitter.emit(null);
  }

  fileClickHandler(file: any) {
    this.fileSelectEmitter.emit(file);
  }
}
