import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../shared/shared.module';
import { FormDialogComponent } from './form/form-dialog.component';

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    FormDialogComponent
  ],
  exports: [
    FormDialogComponent
  ],
  entryComponents: [
    FormDialogComponent
  ]
})
export class DialogsModule { }
