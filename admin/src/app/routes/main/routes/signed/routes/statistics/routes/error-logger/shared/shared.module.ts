import { NgModule } from '@angular/core';

import { SharedModule as GlobalSharedModule } from '../../../../../../../../../shared/shared.module';
@NgModule({
  imports: [
    GlobalSharedModule,
  ],
  declarations: [
  ],
  exports: [
    GlobalSharedModule,
  ],
  entryComponents: [
  ],
  providers: [],
})
export class SharedModule { }
