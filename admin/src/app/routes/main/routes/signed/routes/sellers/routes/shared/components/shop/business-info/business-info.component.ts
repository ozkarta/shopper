import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef } from '@angular/core';
import { Subscription, Observable, of } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { validateAllFormFields } from '../functions';
import { SpinnerService } from '../../../../../../../../../../../shared/services/spinner.service';
import { catchError, map } from 'rxjs/operators';
import { ShopAPIService } from '../../../../../../../../../../../shared/api/shop-api.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { debounceTime, startWith, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { CategoriesAPIService } from '../../../../../../../../../../../shared/api/categories-api.service';
import { categories } from './categories';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-business-info-form-component',
  templateUrl: './business-info.component.html',
  styleUrls: ['./business-info.component.css']
})

export class BusinessInfoFormComponent implements OnInit, OnDestroy {
  @Input('formData') formData: any;
  // @Input('stepper') stepper: MatStepper;
  private subscription: Subscription = new Subscription();
  public businessData: any = {
    businessDisplayName: '',
    businessCategories: [],
    identificationCode: '',
    registrationCode: '',
    registrationDate: new Date(),
  }

  public categoryChipConfig = {
    visible: true,
    selectable: true,
    removable: true,
    addOnBlur: true,
    separatorKeysCodes: [ENTER, COMMA]
  }

  public businessFormGroup!: FormGroup;
  private businessFormGroupConfig = {
    businessDisplayName: ['', []],
    businessCategories: ['', []],
    identificationCode: ['', []],
    registrationCode: ['', []],
    registrationDate: [new Date(), []],
  };

  @ViewChild('categoryInput', {static: false}) categoryInput!: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete!: MatAutocomplete;

  public filteredCategories!: Observable<any>;

  constructor(private fb: FormBuilder,
    private categoriesAPIService: CategoriesAPIService,
    private shopAPIService: ShopAPIService,
    private spinnerService: SpinnerService) {}

  ngOnInit() {
    this.businessFormGroup = this.fb.group(this.businessFormGroupConfig);

    this.filteredCategories = this.businessFormGroup.controls['businessCategories'].valueChanges.pipe(
      debounceTime(500),
      startWith(''),
      distinctUntilChanged(),
      switchMap(keyword => {
        return this.queryCategories({keyword, page: 0, limit: 10})
      }),
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // ==========================================================================

  submitForm() {
    if (!this.businessFormGroup.valid) {
      validateAllFormFields(this.businessFormGroup);
      return;
    }

    let data = Object.assign({}, this.businessFormGroup.value);
    this.spinnerService.start();
    this.shopAPIService.updateShop(this.formData._id, data).pipe(
      map(result => {
        return result;
      }),
      catchError(error => {
        throw error;
      })
    )
    // .subscribe(
    //   (result: any) => {
    //     this.spinnerService.stop();
    //     this.stepper.next();
    //   },
    //   (error: any) => {
    //     this.spinnerService.stop();
    //   }
    // )

    // ეს ყველაფერი მოხდება საბსქრაიბის საქსესში
    this.spinnerService.stop();

    // this.stepper.selected.completed = true;
    // this.stepper.next();
  }

  // ==========================================================================
  add(event: MatChipInputEvent): void {
  }

  remove(businessCategory: any): void {
    const index = this.businessData.businessCategories.indexOf(businessCategory);

    if (index >= 0) {
      this.businessData.businessCategories.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    const category: any = event.option.value;
    this.businessData.businessCategories.push(category);
    this.categoryInput.nativeElement.value = '';
    this.businessFormGroup.controls['businessCategories'].setValue(null);
  }

  queryCategories(query: any): Observable<any> {
    return this.categoriesAPIService.getByQuery(query).pipe(
      map(result => result),
      catchError(error => of(categories))
    )
  }
}