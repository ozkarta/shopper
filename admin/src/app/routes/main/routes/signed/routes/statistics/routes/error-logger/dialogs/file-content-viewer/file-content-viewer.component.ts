import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-file-content-viewer-component',
  templateUrl: './file-content-viewer.component.html',
  styleUrls: ['./file-content-viewer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileContentViewerComponent {

  constructor(
    public dialogRef: MatDialogRef<FileContentViewerComponent>,
    @Inject(MAT_DIALOG_DATA) public log: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  getLevelClass(level: any) {
    let classItem: any = {};
    classItem[level] = true;
    return classItem;
  }

}