import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Subscription, Observable, of } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CategoriesAPIService } from '../../../../../../../../../../../shared/api/categories-api.service';
import { categories } from './categories';
import { map, catchError, debounce, debounceTime, startWith, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ShopModel } from '../../../../../../../../../../../shared/models/shop.model';
import { ShopAPIService } from '../../../../../../../../../../../shared/api/shop-api.service';
import { SpinnerService } from '../../../../../../../../../../../shared/services/spinner.service';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-categories-and-keywords-form-component',
  templateUrl: './categories-and-keywords.component.html',
  styleUrls: ['./categories-and-keywords.component.css']
})

export class CategoriesAndKeywordsFormComponent implements OnInit, OnDestroy {
  @Input('formData') formData!: ShopModel;
  // @Input('stepper') stepper: MatStepper;
  private subscription: Subscription = new Subscription();
  @ViewChild('categoryInput', {static: false}) categoryInput!: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete!: MatAutocomplete;
  @Output('shopChangeHandler') shopChangeEmitter: EventEmitter<any> = new EventEmitter();

  public categoryChipConfig = {
    visible: true,
    selectable: true,
    removable: true,
    addOnBlur: true,
    separatorKeysCodes: [ENTER, COMMA]
  }

  public keywordChipsConfig: any = {
    visible: true,
    selectable: true,
    removable: true,
    addOnBlur: true,
    separatorKeysCodes: [ENTER, COMMA],
  }

  public shopCategoriesAndKeywordsFormGroup!: FormGroup;
  private shopCategoriesAndKeywordsFormGroupConfig = {
    categories: ['', []],
    keywords: ['', []],
  };
  public filteredCategories!: Observable<any>;

  constructor(private fb: FormBuilder,
    private categoriesAPIService: CategoriesAPIService,
    private shopAPIService: ShopAPIService,
    private spinnerService: SpinnerService) {}

  ngOnInit() {
    this.shopCategoriesAndKeywordsFormGroup = this.fb.group(this.shopCategoriesAndKeywordsFormGroupConfig);

    this.filteredCategories = this.shopCategoriesAndKeywordsFormGroup.controls['categories'].valueChanges.pipe(
      debounceTime(500),
      startWith(''),
      distinctUntilChanged(),
      switchMap(keyword => {
        return this.queryCategories({keyword, page: 0, limit: 10})
      }),
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // ========================================================================================================
  // =================================CATEGORIES=========================================
  addCategory(event: MatChipInputEvent): void {
  }

  removeCategory(businessCategory: any): void {
    const index = this.formData.categories.indexOf(businessCategory);

    if (index >= 0) {
      this.formData.categories.splice(index, 1);
    }
  }

  categorySelected(event: MatAutocompleteSelectedEvent): void {
    const category = event.option.value;
    this.formData.categories.push(category);
    this.categoryInput.nativeElement.value = '';
    this.shopCategoriesAndKeywordsFormGroup.controls['categories'].setValue(null);
  }

  queryCategories(query: any): Observable<any> {
    return this.categoriesAPIService.getByQuery(query).pipe(
      map(result => result),
      catchError(error => of(categories))
    )
  }

  // =============================================KEYWORDS======================================================
  addKeyword(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim() && this.formData.keywords.indexOf(value) < 0) {
      this.formData.keywords.push(value.trim());
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeKeyword(keyword: string): void {
    const index = this.formData.keywords.indexOf(keyword);
    if (index >= 0) {
      this.formData.keywords.splice(index, 1);
    }
  }

  submitForm() {

    // if (!this.shopContactFormGroup.valid) {
    //   validateAllFormFields(this.shopContactFormGroup);
    //   return;
    // }

    let data: any = {};
    data.categories = this.formData.categories.map(item => item._id);
    data.keywords = this.formData.keywords;

    this.spinnerService.start();
    this.shopAPIService.updateCategoriesAndKeywords(this.formData._id, data).pipe(
      map((shop: ShopModel) => {
        this.shopChangeEmitter.emit(shop);
      }),
      catchError(error => {
        throw error;
      })
    )
    .subscribe(
      (result: any) => {
        this.spinnerService.stop();
      },
      (error: any) => {
        console.log(error);
        this.spinnerService.stop();
      }
    )
  }
}