import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../../shared/shared.module';
import { ProductsComponent } from './products.component';
import { ProductSharedComponentsModule } from '../../../shared/components/product/product.module';
import { ComponentsModule } from './components/components.module';
import { DialogsModule } from './dialogs/dialogs.module';
// import { ListComponent } from './list/list.component';
// import { FilterComponent } from './filter/filter.component';
// import { ToolbatComponent } from './toolbar/toolbar.component';
@NgModule({
  imports: [
    SharedModule,
    ProductSharedComponentsModule,
    ComponentsModule,
    DialogsModule,
  ],
  declarations: [
    // ListComponent,
    // FilterComponent,
    // ToolbatComponent,
    ProductsComponent,
  ],
  providers: [
  ],
  exports: [
    // ListComponent,
    // FilterComponent,
    // ToolbatComponent,
    ProductsComponent
  ]
})
export class ProductsModule { }
