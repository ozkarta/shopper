import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../shared/shared.module';

import { ListComponent } from './list/list.component';
// import { FilterComponent } from './filter/filter.component';
import { ToolbatComponent } from './toolbar/toolbar.component';
@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    ListComponent,
    // FilterComponent,
    ToolbatComponent
  ],
  providers: [
  ],
  exports: [
    ListComponent,
    // FilterComponent,
    ToolbatComponent
  ]
})
export class ComponentsModule { }
