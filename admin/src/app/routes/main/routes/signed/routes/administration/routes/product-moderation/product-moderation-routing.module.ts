import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductModerationComponent } from './product-moderation.component';

const routes: Routes = [
  {
    path: '',
    component: ProductModerationComponent,
    canActivate: [
    ],
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductModerationRoutingModule { }
