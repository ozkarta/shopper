import { Component, Inject, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatStepper } from '@angular/material/stepper';
import { ProductModel } from '../../../../../../../../../../../../shared/models/product.model';

@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styleUrls: ['./form-dialog.component.css']
})
export class FormDialogComponent implements OnInit, OnDestroy {
  public product!: ProductModel;
  @ViewChild('stepper', {static: false}) stepper!: MatStepper
  public isLinear: boolean = true;
  private subscription: Subscription = new Subscription();
  // ============================================================
  constructor(
    public dialogRef: MatDialogRef<FormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { type: string, formData: any }
  ) { }

  ngOnInit() {
    if (!this.data) return;
    this.product = this.data.formData;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  productChangeHandler(product: any) {
    this.product = product;
  } 
}