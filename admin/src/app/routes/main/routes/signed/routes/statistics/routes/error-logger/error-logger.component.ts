import { Component, OnInit, OnDestroy } from '@angular/core';
import { SpinnerService } from '../../../../../../../../shared/services/spinner.service';
import { BehaviorSubject, Subject, Observable, of } from 'rxjs';
import { switchMap, share, map, filter, tap, catchError } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ErrorLoggerApiService } from '../../../../../../../../shared/api/error-logger-api.service';

import { FileContentViewerComponent } from './dialogs/file-content-viewer/file-content-viewer.component';

@Component({
  selector: 'app-error-logger-component',
  templateUrl: './error-logger.component.html',
  styleUrls: ['./error-logger.component.scss']
})

export class ErrorLoggerComponent implements OnInit, OnDestroy {
  moduleId: any = '';
  componentId: any = '';

  loadDirectoryData$!: Subject<any>;
  directoryItems$!: Observable<any>;
  directoryNumTotal$!: Observable<number>;

  loadFileData$!: Subject<any>;
  fileItems$!: Observable<any>;
  fileNumTotal$!: Observable<number>;

  directoryQuery: any = {
    keyword: '',
    page: 1,
    limit: 10,
  };
  fileQuery: any = {
    keyword: '',
    page: 1,
    limit: 10,
  };;

  activeFolder: any;
  

  constructor(private route: ActivatedRoute,
    private router: Router,
    private api: ErrorLoggerApiService,
    private dialog: MatDialog,
    private spinner: SpinnerService, ) {

  }

  ngOnInit() {
    if (!this.directoryQuery.keyword) {
      this.directoryQuery.keyword = "";
    }

    this.loadDirectoryData$ = new BehaviorSubject(this.directoryQuery);
    this.loadFileData$ = new BehaviorSubject(null);

    this.loadDirectoryData$.subscribe((query: any) => {
    });

    this.loadFileData$.subscribe();

    const directoryData$ = this.loadDirectoryData$.pipe(
      tap(() => this.spinner.start()),
      tap(() => {
        this.activeFolder = null;
      }),
      switchMap(q => this.api.getDirectoriesByQuery(q)),
      tap(() => this.spinner.stop()),
      share(),
    );

    const fileData$ = this.loadFileData$.pipe(
      tap(() => this.spinner.start()),
      switchMap((load) => load? this.api.getFilesByDirectory(Object.assign({}, {directoryName: this.activeFolder.title}, this.fileQuery)): of({items: [], numTotal: 0})),
      tap(() => this.spinner.stop()),
      share()
    )

    this.directoryItems$ = directoryData$.pipe(map(d => d.items));
    this.directoryNumTotal$ = directoryData$.pipe(map(d => d.numTotal));

    this.fileItems$ = fileData$.pipe(map(d => d.items));
    this.fileNumTotal$ = fileData$.pipe(map(d => d.numTotal));
  }

  ngOnDestroy() {

  }

  reloadParams(query: any) {
    this.directoryQuery = query;
    this.loadDirectoryData$.next(query);
  }

  folderSelectHandler(folder: any) {
    this.activeFolder = folder;
    this.loadFileData$.next(!!folder);
    if (!folder) {
      this.reloadParams(this.directoryQuery);
    }
  }

  fileSelectHandler(file: any) {
    if (!file) return;
    this.api.getFileByName({fileName: file.title, directoryName: this.activeFolder.title}).pipe(
      tap(() => this.spinner.start()),
      map(file => {
        this.openFileContentViewerDialog(file);
        return of(null);
      }),
      catchError((error) => {
        this.spinner.stop();
        alert('Something Went Wrong');
        throw error;
      }),
      tap(() => this.spinner.stop())
    ).subscribe()
  }

  openFileContentViewerDialog(fileContent: any): void {
    const dialogRef = this.dialog.open(FileContentViewerComponent, {
      width: '80%',
      data: fileContent
    });
  }

  download(filename: string, content: string) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
}