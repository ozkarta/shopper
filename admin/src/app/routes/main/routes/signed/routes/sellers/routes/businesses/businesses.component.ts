import { Component, OnInit, OnDestroy } from '@angular/core';
@Component({
  selector: 'app-administration-businesses',
  templateUrl: './businesses.component.html',
  styleUrls: ['./businesses.component.css']
})

export class BusinessesComponent implements OnInit, OnDestroy {
  constructor() {

  }

  ngOnInit() {

  }

  ngOnDestroy() {
    
  }
}