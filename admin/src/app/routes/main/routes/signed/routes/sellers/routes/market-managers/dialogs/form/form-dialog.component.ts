import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserAPIService } from '../../../../../../../../../../shared/api/user-api.service';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SpinnerService } from '../../../../../../../../../../shared/services/spinner.service';
@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styleUrls: ['./form-dialog.component.css']
})
export class FormDialogComponent implements OnInit, OnDestroy {
  public form!: FormGroup;
  private formConfig: any = {
  };

  constructor(
    private userApiService: UserAPIService,
    private spinnerService: SpinnerService,
    public fb: FormBuilder,
    public dialogRef: MatDialogRef<FormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { type: string, formData: any }) { }

  ngOnInit() {
    if (!this.data) return;
    this.formConfig = {
      firstName: [this.data.formData && this.data.formData.firstName || '', [Validators.required, Validators.minLength(3)]],
      lastName: [this.data.formData && this.data.formData.lastName || '', [Validators.required, Validators.minLength(3)]],
      email: [this.data.formData && this.data.formData.email || '', [Validators.required, Validators.minLength(3), Validators.email]],
    }

    if (this.data.type === 'create') {
      this.formConfig['password'] = [this.generatePassword(), [Validators.required, Validators.minLength(6)]];
    }
    this.form = this.fb.group(this.formConfig);
  }

  ngOnDestroy() {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    if (!this.form.valid) {
      this.validateAllFormFields(this.form);
      return;
    }
    let data = Object.assign({}, this.form.value);
    let actionObservable: Observable<any> = of();
    this.spinnerService.start();
    if (this.data.type === 'create') {
      actionObservable = this.userApiService.adminCreateMarketManager(data).pipe(
        map(result => {
          return result;
        }),
        catchError(error => {
          throw error;
        })
      );
    }
    if (this.data.type === 'update') {
      actionObservable = this.userApiService.adminUpdateMarketManager(this.data.formData._id, data).pipe(
        map(result => {
          return result;
        }),
        catchError(error => {
          throw error;
        })
      )
    }

    actionObservable.subscribe(
      (result: any) => {
        this.spinnerService.stop();
        this.dialogRef.close(true);
      },
      (error: any) => {
        this.spinnerService.stop();
        console.log(error);
      }
    )

    //this.dialogRef.close(data);
  }

  refreshPassword() {
    this.form.controls['password'].setValue(this.generatePassword());
  }

  // ===================================================================================
  generatePassword(length = 10) {
    let charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let retVal = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}