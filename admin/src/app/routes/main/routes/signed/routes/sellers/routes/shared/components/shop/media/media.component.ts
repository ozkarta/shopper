import { Component, OnInit, OnDestroy, Input, ViewChild, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription, of, Observable } from 'rxjs';
import { ImageCropperComponent, CropperSettings } from 'ngx-img-cropper';
import { FileAPIService } from '../../../../../../../../../../../shared/api/file-api.service';
import { SpinnerService } from '../../../../../../../../../../../shared/services/spinner.service';
import { tap, map, catchError, concatMap } from 'rxjs/operators';
import { convertBase64ToBlob } from '../../../../../../../../../../../shared/utils/functions';
import { MediaModel } from '../../../../../../../../../../../shared/models/media.model';
import { FileModel } from '../../../../../../../../../../../shared/models/file.model';
import { ShopAPIService } from '../../../../../../../../../../../shared/api/shop-api.service';
import { ShopModel } from 'src/app/shared/models/shop.model';
import { mediaConstants } from 'src/app/shared/constants/media-files';
import { MediaImageResolutions } from 'src/app/shared/enums';
import { IMedia } from 'src/app/shared/interfaces/media';
import { MediaService } from 'src/app/shared/services/media.service';

@Component({
  selector: 'app-media-form',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})

export class MediaFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input('formData') formData!: ShopModel;
  @Output('shopChangeHandler') shopChangeEmitter: EventEmitter<any> = new EventEmitter();
  private subscription: Subscription = new Subscription();

  public productMediaItemBeendEdited: boolean = false;

  public shopMediaData!: MediaModel;
  // ==============
  public cropperSettings!: CropperSettings;
  public cropperData: any;
  @ViewChild('mainImageCropper') cropper!: ImageCropperComponent;
  
  constructor(private fileAPIService: FileAPIService,
    private spinnerService: SpinnerService,
    private shopAPIService: ShopAPIService,
    private mediaService: MediaService) {    
  }

  ngOnInit() {
    this.shopMediaData = this.formData.media;

    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.keepAspect = true;

    this.cropperSettings.width = 90;
    this.cropperSettings.height = 120;

    this.cropperSettings.croppedWidth = 1200;
    this.cropperSettings.croppedHeight = 1600;
    this.cropperSettings.canvasWidth = 300;
    this.cropperSettings.canvasHeight = 400;

    this.cropperData = {};

    this.initCropperInitialData();
  }

  initCropperInitialData() {
    if (!this.shopMediaData) return;
    

    let image: HTMLImageElement = new Image();
    image.crossOrigin = "Anonymous";
    image.src = this.getProductMediaSRC(this.shopMediaData, MediaImageResolutions.ORIGINAL_IMAGE);
    image.onload = () => {
      this.cropper.setImage(image);
      this.cropperData.fileName = typeof this.shopMediaData.originalImage === 'object'?  this.shopMediaData.originalImage.filename : 'file-name';
    }
  }

  ngOnChanges(changes: any) {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // ===========================================================================================

  resetDefaultOptions() {

  }

  isShopMediaFormReadyForSubmit(): boolean {
    return true;
  }

  singleFileChangeListener($event: Event) {
    if (!$event || !$event.target) return;
    let file: File = Array.from(<FileList>(<HTMLInputElement>$event.target).files)[0];
    let reader: FileReader = new FileReader();
    let image:any = new Image();
    reader.onloadend = (loadEvent: any) => {
      image.src = loadEvent.target.result;
      this.cropper.setImage(image);
      this.cropperData.fileName = file.name;
    }
    reader.readAsDataURL(file);
  }

  // ============================================================================================
  uploadCropperImage() {
    if (!this.cropperData || !this.cropperData.image) return;
    this.spinnerService.start();
    let originalImaegBlob: Blob | null;
    try {
      originalImaegBlob = convertBase64ToBlob(this.cropperData.original.src);
    } catch (error) {
      originalImaegBlob = null;
    }
    // this.uploadSingleImage(convertBase64ToBlob(this.cropperData.image), this.cropperData.fileName).pipe(
    this.upladCroppedImageAlngWithOriginal(originalImaegBlob, convertBase64ToBlob(this.cropperData.image), this.cropperData.fileName).pipe(
      // tap(() => this.spinnerService.start()),
      map((media: MediaModel) => {
        this.shopMediaData = media;
        return media;
      }),
      concatMap((media: MediaModel) => {
        return this.shopAPIService.updateShopMedia(this.formData._id, {mediaId: media._id}).pipe(
          map((result: any) => {
            this.resetDefaultOptions();
          }),
          catchError((error: Error) => of(null)),
          tap(() => this.spinnerService.stop())
        )
      }),
      catchError((error: Error) => {
        console.log(error);
        throw error;
      }),
      tap(() => this.spinnerService.stop()),
    ).subscribe();
  }

  submitStoreMediaForm() {
    this.spinnerService.start()
    this.shopAPIService.updateShopMedia(this.formData._id, this.shopMediaData).pipe(
      map((result: any) => {
        this.resetDefaultOptions();
      }),
      catchError((error: Error) => of(null)),
      tap(() => this.spinnerService.stop())
    ).subscribe();
  }
  // ============================================================================================

  uploadSingleImage(file: Blob, fileName: string): Observable<MediaModel> {
    return this.fileAPIService.uploadSingleImage(file, fileName).pipe(
      tap(() => this.spinnerService.start()),
      map((result: MediaModel) => result),
      tap(() => this.spinnerService.stop()),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  upladCroppedImageAlngWithOriginal(original: Blob | null, cropped: Blob, fileName: string): Observable<MediaModel> {
    return this.fileAPIService.upladCroppedImageAlngWithOriginal(this.shopMediaData?._id || null, original, cropped, fileName).pipe(
      tap(() => this.spinnerService.start()),
      map((result: MediaModel) => result),
      tap(() => this.spinnerService.stop()),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  getProductMediaSRC(productMedia: any, mediaImageReslution: MediaImageResolutions = MediaImageResolutions.LOW_RESLUTION_IMAGE) {
    return this.mediaService.getProductMediaFileUrlByMediaImageResolutions(productMedia, mediaImageReslution)
  }

  // ==============================================================
}