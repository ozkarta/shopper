import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrationComponent } from './administration.component';
import { HasAdministrationPermissionGuard } from './administration-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AdministrationComponent,
    canActivate: [
      HasAdministrationPermissionGuard
    ],
    children: [
      {
        path: 'categories',
        loadChildren: () => import('./routes/categories/categories.module').then(m => m.CategoriesModule)
      },
      {
        path: 'business-moderation',
        loadChildren: () => import('./routes/business-moderation/business-moderation.module').then(m => m.BusinessModerationModule)
      },
      {
        path: 'product-moderation',
        loadChildren: () => import('./routes/product-moderation/product-moderation.module').then(m => m.ProductModerationModule)
      },
      {
        path: 'new-category-request-moderation',
        loadChildren: () => import('./routes/new-category-request-moderation/new-category-request-moderation.module').then(m => m.NewCategoryRequestModerationModule)
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
