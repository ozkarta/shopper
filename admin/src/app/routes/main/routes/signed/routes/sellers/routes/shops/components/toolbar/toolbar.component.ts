import { Component, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})

export class ToolbatComponent {
  @Output('createHandler') createEmitter: EventEmitter<any> = new EventEmitter();

  createClickHandler() {
    this.createEmitter.emit();
  }
}