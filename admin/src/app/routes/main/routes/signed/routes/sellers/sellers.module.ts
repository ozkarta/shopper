import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../shared/shared.module';
import { SellersComponent } from './sellers.component';
import { SellersRoutingModule } from './sellers-routing.module';
import { HasSellerPermissionGuard } from './sellers-guard.service';

@NgModule({
  imports: [
    SharedModule,
    SellersRoutingModule
  ],
  declarations: [
    SellersComponent
  ],
  providers: [
    HasSellerPermissionGuard
  ]
})
export class SellersModule { }
