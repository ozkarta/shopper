import { NgModule } from '@angular/core';
import { ErrorLoggerComponent } from './error-logger.component';
import { ComponentsModule } from './components/components.module';
import { DialogModule } from './dialogs/dialogs.module';
import { SharedModule } from './shared/shared.module';
import { ErrorLoggerRoutingModule } from './error-logger-routing.module';

@NgModule({
  imports: [
    ComponentsModule,
    DialogModule,
    SharedModule,
    ErrorLoggerRoutingModule
  ],
  declarations: [
    ErrorLoggerComponent,
  ],
  entryComponents: [
    ErrorLoggerComponent,
  ]
})
export class ErrorLoggerModule { }
