import { Component, Input, Output, EventEmitter } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-folder-list',
  templateUrl: './folder-list.component.html',
  styleUrls: ['./folder-list.component.scss'],
})
export class FolderListComponent {
  @Input() items!: any[];
  @Output('folderSelectHandler') folderSelectEmitter: EventEmitter<any> = new EventEmitter(); 

  folderClickHandler(folder: any) {
    this.folderSelectEmitter.emit(folder);
  }
}
