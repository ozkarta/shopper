import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription, BehaviorSubject, Observable, of } from 'rxjs';
import { ShopModel } from '../../../../../../../../../../shared/models/shop.model';
import { ProductModel } from '../../../../../../../../../../shared/models/product.model';
import { SpinnerService } from '../../../../../../../../../../shared/services/spinner.service';

import { FormDialogComponent } from './dialogs/form/form-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ProductAPIService } from '../../../../../../../../../../shared/api/product-api.service';
import { switchMap, map, catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'app-products-component',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit, OnDestroy, OnChanges {
  @Input('shop') shop!: ShopModel

  public numTotal = 0;
  public products: ProductModel[] = [];
  public query: any = {
    keyword: '',
    page: 1,
    limit: 10,
  };

  private subscription: Subscription = new Subscription();


  private loadData: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private spinnerService: SpinnerService,
    private dialog: MatDialog,
    private productAPIService: ProductAPIService) {
  }

  ngOnInit() {
    if (!this.shop) return;
    this.query.shop = this.shop._id;
    this.subscription.add(this.initLoadData().subscribe());
    this.loadData.next(true);
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setInitialStateOfQuery() {
    this.query = {
      keyword: '',
      page: 0,
      limit: 10,
    };
  }

  initLoadData(): Observable<any> {
    return this.loadData.pipe(
      switchMap((update: boolean) => {
        if (update) {
          this.spinnerService.start();
          return this.productAPIService.getProductsByQuery(this.query).pipe(
            map(({items, numTotal}) => {
              this.products = items || [];
              this.numTotal = numTotal || 0;
              this.spinnerService.stop();
              return items;
            }),
            catchError(error => {
              this.spinnerService.stop();
              throw error;
            })
          )
        } else {
          return of(null);
        }
      })
    );
  }

  createHandler() {
    if (!this.shop) return;
    let product = new ProductModel();
    product.shop = this.shop;
    this.openProductFormDialog(undefined, product, (formData: any) => {
      this.setInitialStateOfQuery();
      this.loadData.next(true);
    });
  }

  filterChange(event: any) {
    
  }

  // ==============================
  openProductFormDialog(type: string = 'create', formData: any = {}, afterClosed: (formData: any) => void): void {
    const dialogRef = this.dialog.open(FormDialogComponent, {
      width: '70%',
      height: '80%',
      data: {
        type,
        formData
      }
    });

    dialogRef.afterClosed().subscribe(afterClosed);
  }

  editHandler(product: ProductModel) {
    this.openProductFormDialog('update', product, (formData: any) => {
      this.loadData.next(true);
    });
  }

  deleteHandler(product: ProductModel) {
    
  }

  enableStatusChangeHandler(data: {enabled: boolean, _id: string}) {
    if (!data || !data._id) return;
    this.productAPIService.updateProductEnabledStatus(data._id, {enabled: data.enabled}).pipe(
      tap(() => this.spinnerService.start()),
      map((result: ProductModel) => this.loadData.next(true)),
      catchError((error: Error) => {
        this.spinnerService.stop();
        this.loadData.next(true);
        throw error;
      }),
      tap(() => this.spinnerService.stop())
    ).subscribe();
  }
}