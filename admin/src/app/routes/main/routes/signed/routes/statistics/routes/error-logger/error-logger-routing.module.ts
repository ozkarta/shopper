import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorLoggerComponent } from './error-logger.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorLoggerComponent,
    canActivate: [
    ],
    children: [
      {
        path: '',
        component: ErrorLoggerComponent
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorLoggerRoutingModule { }
