import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DragulaService } from 'ng2-dragula';
import { Subscription, BehaviorSubject, of, Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { CategoryFormModalComponent } from './modal/category-form-modal/category-form-modal.component';
import { CategoriesAPIService } from '../../../../../../../../shared/api/categories-api.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../../../../../shared/dialogs/confirmation-modal/confirmation-modal.component';
import { ConfirmationModel } from '../../../../../../../../shared/models/confirmation.model';
import { SpinnerService } from '../../../../../../../../shared/services/spinner.service';
@Component({
  selector: 'app-administration-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})

export class CategoriesComponent implements OnInit, OnDestroy {
  private openCategoryMap: any = {};
  public allCategories: any[] = [];
  public connectedToElementArray = [];

  // DRAGULA
  public BAG = "DRAGULA_CATEGORIES";
  private subs = new Subscription();
  private loadData: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private $loadData!: Observable<any>;
  constructor(private route: ActivatedRoute,
    private dragulaService: DragulaService,
    private dialog: MatDialog,
    private categoryAPIService: CategoriesAPIService,
    private spinnerService: SpinnerService) {
    this.dragulaService.createGroup(this.BAG, {
      moves: (el: any, container: any, handle: any) => {
        return handle.className.indexOf('handles-drag') >= 0;
      }
    });
  }

  ngOnInit() {
    this.allCategories = this.route.snapshot.data.allCategories;
    this.handleDragulaEvents();
    this.$loadData = this.loadData.asObservable().pipe(switchMap(update => {
      if(update) {
        this.spinnerService.start();
        return this.categoryAPIService.getAllCategories(20).pipe(map(data => {
          this.allCategories = data;
          this.spinnerService.stop();
        }, catchError((error) => {
          this.spinnerService.stop();
          throw error;
        })))  
      }
      return of(null);
    }));
    this.subs.add(this.$loadData.subscribe());
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.dragulaService.destroy(this.BAG);
  }

  openCategoryFormDialog(type: string = 'create', formData: any = {}, afterClosed: (formData: any) => void): void {
    const dialogRef = this.dialog.open(CategoryFormModalComponent, {
      width: '30%',
      data: {
        type,
        formData
      }
    });

    dialogRef.afterClosed().subscribe(afterClosed);
  }

  openConfirmationDialog(data: ConfirmationModel, afterClosed: (data: any) => void): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '30%',
      data
    });
    dialogRef.afterClosed().subscribe(afterClosed);
  }

  // DRAGULA DRAG&DROP
  // ====================================================================================
  handleDragulaEvents() {
    this.subs.add(this.dragulaService.dropModel(this.BAG)
      .subscribe(({ name, el, target, source, sibling, targetIndex, sourceIndex, sourceModel, targetModel }) => {
        let categoryId = el.id;
        let categoryIndex = targetIndex + 1;
        let parentCategoryId = target.id;
        let prevCategoryId = (categoryIndex > 0) ? targetModel[categoryIndex - 1]._id || null : null;
        let nextCategoryId = (categoryIndex < targetModel.length - 1) ? targetModel[categoryIndex + 1]._id || null : null;

        console.log(`${categoryId} moved to ${parentCategoryId} on index ${categoryIndex}, between ${prevCategoryId} & ${nextCategoryId}`);
        this.updateCategoryLocation({
          _id: categoryId,
          sortWeight: categoryIndex,
          parentCategory: parentCategoryId || null,
          prevCategoryId,
          nextCategoryId,
        })
      })
    );
  }

  addClass(el: HTMLElement, className: string) {
    console.log('Add ', className);
  }

  removeClass(el: HTMLElement, className: string) {
    console.log('Remove ', className);
  }
  // =====================================================================================

  // EVENT HANDLERS
  // =====================================================================================
  deleteCategoryClickHandler(category: any) {
    this.openConfirmationDialog({
      title: 'Delete Category',
      question: 'Are you sure you want to delete Category?',
      noButtonText: 'Close',
      yesButtonText: 'Yes, Delete',
    }, (result) => {
      result && this.deleteCategory(category._id);
    })
  }

  updateCategoryClickHandler(category: any) {
    this.openCategoryFormDialog('update', category, (formData) => {
      if (formData) {
        // {includeInSearch, categoryNameEN, categoryNameGE, friendlyIdEN, friendlyIdGE}
        this.updateCategory(category._id, {
          includeInSearch: formData.includeInSearch,
          categoryName: {
            en: formData.categoryNameEN,
            ge: formData.categoryNameGE
          },
          friendlyId: {
            en: formData.friendlyIdEN,
            ge: formData.friendlyIdGE
          }
        });
      }
    })
  }

  createCategoryClickandler(parentCategory: any) {
    let category = Object.assign({}, { parentCategory: parentCategory ? parentCategory._id : null });
    this.openCategoryFormDialog('create', category, (formData) => {
      if (formData) {
        this.createCategory({
          includeInSearch: formData.includeInSearch,
          categoryName: {
            en: formData.categoryNameEN,
            ge: formData.categoryNameGE
          },
          friendlyId: {
            en: formData.friendlyIdEN,
            ge: formData.friendlyIdGE
          },
          parentCategory: parentCategory? parentCategory._id : null
        });
        this.toggleCategory(parentCategory);
      }
    })
  }

  toggleCategory(category: any) {
    if (!category) return;
    this.openCategoryMap[category._id] = !this.openCategoryMap[category._id];
  }

  closeCategory(category: any) {
    if (!category) return;
    this.openCategoryMap[category._id] = false;
  }

  isOpen(category: any) {
    return !!this.openCategoryMap[category._id];
  }

  // =====================================================================================

  // API CALLS
  // =====================================================================================
  updateCategoryLocation(categoryData: any) {
    this.spinnerService.start();
    this.categoryAPIService.updateLocation(categoryData._id,categoryData).pipe(
      map((result) => {
        this.spinnerService.stop();
        this.loadData.next(true);
      }),
      catchError(error => {
        this.spinnerService.stop();
        throw error;
      })
    ).subscribe();
  }

  updateCategory(id: string, categoryData: any) {
    this.spinnerService.start();
    this.categoryAPIService.update(id, categoryData).pipe(
      map((result) => {
        this.spinnerService.stop();
        this.loadData.next(true);
      }),
      catchError(error => {
        this.spinnerService.stop();
        throw error;
      })
    ).subscribe();
  }

  createCategory(categoryData: any) {
    this.spinnerService.start();
    this.categoryAPIService.create(categoryData).pipe(
      map((result) => {
        this.spinnerService.stop();
        this.loadData.next(true);
      }),
      catchError(error => {
        this.spinnerService.stop();
        throw error;
      })
    ).subscribe();
  }

  deleteCategory(id: string) {
    this.spinnerService.start()
    this.categoryAPIService.remove(id).pipe(
      map((result) => {
        this.spinnerService.stop();
        this.loadData.next(true);
      }),
      catchError(error => {
        this.spinnerService.stop();
        throw error;
      })
    ).subscribe();
  }
  // =====================================================================================
  
}