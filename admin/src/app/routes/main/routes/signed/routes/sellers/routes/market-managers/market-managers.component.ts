import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormDialogComponent } from './dialogs/form/form-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { UserAPIService } from '../../../../../../../../shared/api/user-api.service';
import { SpinnerService } from '../../../../../../../../shared/services/spinner.service';
import { Subscription, BehaviorSubject, of, Observable } from 'rxjs';
import { switchMap, catchError, map } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../../../../../shared/dialogs/confirmation-modal/confirmation-modal.component';
import { ConfirmationModel } from '../../../../../../../../shared/models/confirmation.model';

@Component({
  selector: 'app-administration-market-managers',
  templateUrl: './market-managers.component.html',
  styleUrls: ['./market-managers.component.css']
})

export class MarketManagersComponent implements OnInit, OnDestroy {
  public numTotal: number = 0;
  public items: any[] = [];
  private subscription: Subscription = new Subscription();
  private loadData: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public query: any = {
    keyword: '',
    page: 1,
    limit: 10,
  };
  constructor(private dialog: MatDialog,
    private userApiService: UserAPIService,
    private spinnerService: SpinnerService) {
  }

  ngOnInit() {
    this.subscription.add(this.initLoadData().subscribe());
    this.loadData.next(true);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  initLoadData(): Observable<any> {
    return this.loadData.pipe(
      switchMap(update => {
        if (update) {
          this.spinnerService.start();
          return this.userApiService.getByQuery(this.query).pipe(
            map(response => {
              this.items = response.items || [];
              this.numTotal = response.numTotal || 0;
              this.spinnerService.stop();
              return response.items;
            }),
            catchError(error => {
              this.spinnerService.stop();
              throw error;
            })
          )
        } else {
          return of(null);
        }
      })
    );
  }

  openCategoryFormDialog(type: string = 'create', formData: any = {}, afterClosed: (formData: any) => void): void {
    const dialogRef = this.dialog.open(FormDialogComponent, {
      width: '30%',
      data: {
        type,
        formData
      }
    });

    dialogRef.afterClosed().subscribe(afterClosed);
  }

  openConfirmationDialog(data: ConfirmationModel, afterClosed: (data: any) => void): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '30%',
      data
    });
    dialogRef.afterClosed().subscribe(afterClosed);
  }

  // event handlers
  editManagerHandler(item: any) {
    this.openCategoryFormDialog('update', item, (loadData) => {
      this.loadData.next(loadData)
    })
  }

  createManagerHandler() {
    this.openCategoryFormDialog('create', {}, (loadData) => {
      this.loadData.next(loadData);
    })
  }

  filterChange(query: any) {
    this.query = query;
    this.loadData.next(true);
  }

  deleteManagerClickhandler(item: any) {
    this.openConfirmationDialog({
      title: 'Delete Market Manager',
      question: 'Are you sure you want to delete market manager?',
      noButtonText: 'Close',
      yesButtonText: 'Yes, Delete',
    }, (result) => {
      result && this.deleteManager(item._id);
    })
  }
  
  deleteManager(id: string) {
    // if(!id) return;
    // this.spinnerService.start();
    // this.userApiService.deleteMarketManager(id)
    //   .pipe(
    //     map(result => {
    //       this.spinnerService.stop();
    //       this.loadData.next(true);
    //     }),
    //     catchError(error => {
    //       this.spinnerService.stop();
    //       throw error;
    //     })
    //   ).subscribe();
  }

}