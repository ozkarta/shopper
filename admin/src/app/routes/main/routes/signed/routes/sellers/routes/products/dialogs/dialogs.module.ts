import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../shared/shared.module';
import { FormDialogComponent } from './form/form-dialog.component';
import { ProductSharedComponentsModule } from '../../shared/components/product/product.module';

@NgModule({
  imports: [
    SharedModule,
    ProductSharedComponentsModule,
  ],
  declarations: [
    FormDialogComponent
  ],
  exports: [
    FormDialogComponent
  ],
  entryComponents: [
    FormDialogComponent
  ]
})
export class DialogsModule { }
