import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../shared/shared.module';
import { MarketManagersComponent } from './market-managers.component';
import { marketManagersRoutingModule } from './market-managers-routing.module';
import { ComponentsModule } from './components/components.module';
import { DialogsModule } from './dialogs/dialogs.module';

@NgModule({
  imports: [
    SharedModule,
    marketManagersRoutingModule,
    ComponentsModule,
    DialogsModule
  ],
  declarations: [
    MarketManagersComponent
  ],
  providers: [
  ],
  exports: [
  ]
})
export class MarketManagersModule { }
