import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarketManagersComponent } from './market-managers.component';

const routes: Routes = [
  {
    path: '',
    component: MarketManagersComponent,
    canActivate: [
    ],
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class marketManagersRoutingModule { }
