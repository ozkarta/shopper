import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../shared/shared.module';
import { FormDialogComponent } from './form/form-dialog.component';
import { ShopSubFormsModule } from '../../shared/components/shop/shop.module';
@NgModule({
  imports: [
    SharedModule,
    ShopSubFormsModule,
  ],
  declarations: [
    FormDialogComponent
  ],
  exports: [
    FormDialogComponent
  ],
  entryComponents: [
    FormDialogComponent
  ]
})
export class DialogsModule { }
