import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../../shared/shared.module';

import { MediaFormComponent } from './media/media.component';
import { ProductInfoFormComponent } from './product-info/product-info-form.component';
import { ListComponent } from './list/list.component';
import { CategoriesAndKeywordsComponent } from './categories-and-keywords/categories-and-keywords.component';
import { ProductPriceAndDiscountsFormComponent } from './price-and-discounts/price-and-discounts.component';

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    MediaFormComponent,
    ProductInfoFormComponent,
    ListComponent,
    CategoriesAndKeywordsComponent,
    ProductPriceAndDiscountsFormComponent,
  ],
  exports: [
    MediaFormComponent,
    ProductInfoFormComponent,
    ListComponent,
    CategoriesAndKeywordsComponent,
    ProductPriceAndDiscountsFormComponent,
  ],
})
export class ProductSharedComponentsModule { }
