import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ProductAPIService } from 'src/app/shared/api/product-api.service';
import { Currencies } from 'src/app/shared/enums';
import { ProductModel } from 'src/app/shared/models/product.model';
import { SpinnerService } from 'src/app/shared/services/spinner.service';

@Component({
  selector: 'app-prooduct-price-and-discounts-form-component',
  templateUrl: 'price-and-discounts.component.html',
  styleUrls: ['price-and-discounts.component.css']
})

export class ProductPriceAndDiscountsFormComponent implements OnInit {
  @Input('formData') product!: ProductModel; 

  @Output('productChangeHandler') productChangeEmitter: EventEmitter<any> = new EventEmitter();
  private subscription: Subscription = new Subscription();

  public addProductPriceAndDiscountsFormGroup!: FormGroup;
  private addProductPriceAndDiscountsFormGroupConfig: any;
  constructor(
    private spinnerService: SpinnerService,
    private fb: FormBuilder,
    private productAPIService: ProductAPIService,
  ) { }

  ngOnInit() {
    this.initFormConfig();
    this.addProductPriceAndDiscountsFormGroup = this.fb.group(this.addProductPriceAndDiscountsFormGroupConfig);
  }

  initFormConfig() {
    this.addProductPriceAndDiscountsFormGroupConfig = {
      quantity: [this.product.quantity || 0, [Validators.required, Validators.min(1), Validators.max(1000)]],
      price: [this.product.price || 0, [Validators.required, Validators.min(1), Validators.max(100000)]],
      currency: [{value: this.product.currency || Currencies.GEL, disabled: true}, [Validators.required]],
    }
  }
   

   submitProductPriceAndDiscountsForm() {
      if (!this.addProductPriceAndDiscountsFormGroup.valid || !this.product._id) return;
      this.spinnerService.start();
      this.productAPIService.updateProductPriceAndDiscountData(this.product._id, this.addProductPriceAndDiscountsFormGroup.value).pipe(
        map((product: ProductModel) => {
          this.spinnerService.stop();
          this.productChangeEmitter.emit(product);
          return product;
        }),
        catchError((error: any) => {
          console.log(error);
          this.spinnerService.stop();
          return of(null);
        })
      ).subscribe();
   }
}