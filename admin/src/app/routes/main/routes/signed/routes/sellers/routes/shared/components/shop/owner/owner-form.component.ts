import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ownerObjectValidator } from '../custom-validators';
import { validateAllFormFields } from '../functions';
import { Observable, Subscription, of } from 'rxjs';
import { map, debounceTime, startWith, distinctUntilChanged, switchMap, catchError } from 'rxjs/operators';
import { UserAPIService } from '../../../../../../../../../../../shared/api/user-api.service';
import { MatStepper } from '@angular/material/stepper';
import { SpinnerService } from '../../../../../../../../../../../shared/services/spinner.service';
import { ShopAPIService } from '../../../../../../../../../../../shared/api/shop-api.service';
import { ShopModel } from '../../../../../../../../../../../shared/models/shop.model';

@Component({
  selector: 'app-owner-form-component',
  templateUrl: './owner-form.component.html',
  styleUrls: ['./owner-form.component.css']
})

export class OwnerFormComponent implements OnInit, OnDestroy {
  @Input('formType') formType!: string;
  @Input('formData') formData: any;
  // @Input('stepper') stepper: MatStepper;
  @Output('shopChangeHandler') shopChangeEmitter: EventEmitter<any> = new EventEmitter();
  public marketManagers!: Observable<any[]>;

  public addOwnerFormGroup!: FormGroup;
  private addOwnerFormGroupSubmitted: boolean = false;
  private addOwnerFormGroupLastValueCopy = null;
  private addOwnerFormGroupConfig = {};

  private subscription: Subscription = new Subscription();

  constructor(private fb: FormBuilder,
    private shopAPIService: ShopAPIService,
    private spinnerService: SpinnerService,
    private userAPIService: UserAPIService) {}

  ngOnInit() {
    this.addOwnerFormGroupConfig = {
      owner: [this.formData? this.formData.owner : '', [Validators.required, ownerObjectValidator]]
    };

    this.addOwnerFormGroup = this.fb.group(this.addOwnerFormGroupConfig);


    // if (this.formData && this.formData.owner) {
    //   this.addOwnerFormGroupSubmitted = true;
    //   this.addOwnerFormGroupLastValueCopy = this.formData.owner;
    //   let timeout = setTimeout(() => {
    //     this.stepper.selected.completed = true;
    //     clearTimeout(timeout);
    //   }, 500);
    // }

    this.subscribeAddOwnerFormGroupValueChanges();
    this.subscribeAddOwnerFormGroupControlValueChanges();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // ======================================SUBSCRIPTIONS=================================================
  subscribeAddOwnerFormGroupValueChanges() {
    this.subscription.add(
      this.addOwnerFormGroup.valueChanges.pipe(
        map((value: any) => {
          // this.stepper.selected.completed = 
          //   this.addOwnerFormGroupSubmitted && this.addOwnerFormGroupLastValueCopy === value && this.addOwnerFormGroup.valid;
        })
      ).subscribe()
    );
  }

  subscribeAddOwnerFormGroupControlValueChanges() {
    this.marketManagers = this.addOwnerFormGroup.controls['owner'].valueChanges.pipe(
      debounceTime(500),
      startWith(''),
      distinctUntilChanged(),
      switchMap((keyword: string) => {
        return this.queryMarketManagers({keyword, page: 1, limit: 10})
      }),
    )
  }
  // =================================================================================================

  queryMarketManagers(query: any): Observable<any[]> {
    if (query && query.keyword && typeof query.keyword === 'object') return of([]);
    return this.userAPIService.getByQuery(query).pipe(
      map(result => {
        return result.items || [];
      }),
      catchError(error => {
        return of([]);
      })
    )
  }

  ownerDisplayFunction(owner: any) {
    if (!owner) return '';
    if (typeof owner === 'string') return owner;
    return `${owner.firstName} ${owner.lastName}`
  }

  // =====================================================================================================


  submitOwnerForm() {
    if (!this.addOwnerFormGroup.valid) {
      validateAllFormFields(this.addOwnerFormGroup);
      return;
    }

    let data = Object.assign({}, this.addOwnerFormGroup.value);
    data.owner = data.owner._id;

    let actionObservable: Observable<any> = of();
    this.spinnerService.start();
    if (this.formType === 'create') {
      actionObservable = this.shopAPIService.createShopWithOwner(data).pipe(
        map(result => {
          return result;
        }),
        catchError(error => {
          throw error;
        })
      );
    }
    if (this.formType === 'update') {
      actionObservable = this.shopAPIService.updateShop(this.formData._id, data).pipe(
        map(result => {
          return result;
        }),
        catchError(error => {
          throw error;
        })
      )
    }
    
    actionObservable.subscribe(
      (shopData: ShopModel) => {
        this.shopChangeEmitter.emit(shopData);
        this.spinnerService.stop();
        this.addOwnerFormGroupSubmitted = true;
        this.addOwnerFormGroupLastValueCopy = this.addOwnerFormGroup.value;

        // this.stepper.selected.completed = true;
        // this.stepper.next();
      },
      (error: any) => {
        console.log(error);
        this.spinnerService.stop();
      }
    )
  }
}