import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopDetailsComponent } from './shop-details.component';
import { ShopResolver, ShopProductsResolver } from './shop-details-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: ShopDetailsComponent,
    resolve: {
      shop: ShopResolver,
      // shopProducts: ShopProductsResolver,
    },
    canActivate: [
    ],
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopDetailsRoutingModule { }
