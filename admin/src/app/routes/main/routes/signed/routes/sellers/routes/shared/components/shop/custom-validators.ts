import { FormControl, FormGroup } from '@angular/forms';

export function ownerObjectValidator(control: FormControl) {
  const { value } = control;
  if (!value) return 'Value is Required';
  if (typeof value === 'string') {
    return {
      notAnObject: 'Value is not an Object'
    }
  }
  if (!value._id) {
    return {
      mustContainObjectId: 'Must contain Object ID',
    }
  }
  return null;
}

export function countryObjectValidator(control: FormControl) {
  const { value } = control;
  if (!value) return 'Value is Required'
  if (typeof value === 'string') {
    return {
      notAnObject: 'Value is not an Object'
    }
  }
  return null;
}