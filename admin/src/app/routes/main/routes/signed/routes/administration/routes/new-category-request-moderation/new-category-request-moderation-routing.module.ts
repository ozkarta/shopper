import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewCategoryRequestModerationComponent } from './new-category-request-moderation.component';

const routes: Routes = [
  {
    path: '',
    component: NewCategoryRequestModerationComponent,
    canActivate: [
    ],
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewCategoryRequestModerationRoutingModule { }
