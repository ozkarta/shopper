import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { CategoriesComponent } from './categories.component';
import { CategoriesRoutingModule } from './categories-routing.module';
import { AllCategoriesResolver } from './categories-resolver.service';
import { CategoryFormModalComponent } from './modal/category-form-modal/category-form-modal.component';

@NgModule({
  imports: [
    SharedModule,
    CategoriesRoutingModule
  ],
  declarations: [
    CategoriesComponent,
    CategoryFormModalComponent
  ],
  providers: [
    AllCategoriesResolver
  ],
  exports: [
  ],
  entryComponents: [
    CategoryFormModalComponent
  ]
})
export class CategoriesModule { }
