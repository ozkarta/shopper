import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { formatDate, registerLocaleData } from '@angular/common';
// import localeUS from '@angular/common/locales/es-US';
import { ShopModel } from '../../../../../../../../../../shared/models/shop.model';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { mediaConstants } from 'src/app/shared/constants/media-files';
import { FileAPIService } from 'src/app/shared/api/file-api.service';
import { MediaService } from 'src/app/shared/services/media.service';
import { MediaImageResolutions } from 'src/app/shared/enums';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent {
  @Input() items!: ShopModel[];
  @Output('editHandler') editEmitter: EventEmitter<any> = new EventEmitter();
  @Output('deleteHandler') deleteEmitter: EventEmitter<any> = new EventEmitter();
  @Output('enableStatusChangeHandler') enableStatusChangeEmitter: EventEmitter<any> = new EventEmitter();

  public columnsToDisplay = [
    'media',
    'owner',
    'businessRegistrationDate',
    'businessDisplayName',
    'title',
    'actions'
  ];
  public displayColumnTranslations: any = {
    'media': {
      title: 'Media',
      type: 'image',
      transformer: (element: any) => {
        return ``;
      }
    },
    'businessRegistrationDate': {
      title: 'Business Registration Date',
      transformer: (element: any) => {
        return element['business'] && formatDate(element['business']['registrationDate'], 'M/d/yy, h:mm a', 'es-US');
      }
    },
    'businessDisplayName': {
      title: 'Business Name',
      transformer: (element: any) => {
        return element.business && `${element.business.businessDisplayName}`;
      }
    },
    'owner': {
      title: 'Owner',
      transformer: (element: any) => {
        return `${element.owner.firstName} ${element.owner.lastName}`;
      }
    },
    'title': {
      title: 'Title',
      type: 'link',
      linkRoute: (element: any) => {
        return `/signed/sellers/shops/${element._id}`
      },
      transformer: (element: any) => {
        return `${(element.title && element.title.en) || 'N/A'}`;
      }
    },
  }

  constructor(
    private fileAPIService: FileAPIService,
    private mediaService: MediaService,
  ) {}

  // ====================================

  editClickHandler(item: any) {
    this.editEmitter.next(item);
  }

  deleteClickHandler(item: any) {
    this.deleteEmitter.emit(item);
  }

  changeEnableStatusHandler(event: MatSlideToggleChange, shop: ShopModel) {
    this.enableStatusChangeEmitter.emit({
      enabled: event.checked,
      _id: shop._id
    });
  }

  getShopImage(_shop: any) {
    return this.mediaService.getShopMediaFileUrlByMediaImageResolutions(_shop.media, MediaImageResolutions.LOW_RESLUTION_IMAGE)
  }
}