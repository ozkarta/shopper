import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import * as _  from 'lodash';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent {

  @Input() query!: any;
  @Input() numTotal!: number | null | undefined;

  public timePeriods: {viewName: any, value: any}[] = [
    {viewName: 'Last 24 Hours', value: 'last_24_h'},
    {viewName: 'Last Week', value: 'last_w'},
    {viewName: 'Last Month', value: 'last_m'},
    {viewName: 'All Time', value: ''}
  ]
  public selectedTimePeriod = '';

  

  @Output() queryChange = new EventEmitter<any>();

  constructor() {
  }

  changePage(data: {pageIndex: number, pageSize: number}) {
    this.queryChange.emit({
      ...this.query,
      page: data.pageIndex + 1,
      limit: data.pageSize,
    });
  }

  updateSearch() {
    this.queryChange.emit({
      ...this.query,
      page: 1,
    });
  }

  getMinDate() {
    return this.query.startDate ? moment(this.query.startDate).add(1, 'day').toDate() : null;
  }

  getMaxDate() {
    return this.query.endDate ? moment(this.query.endDate).add(-1, 'day').toDate() : null;
  }
}
