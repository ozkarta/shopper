import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormDialogComponent } from './dialogs/form/form-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, Observable, of, BehaviorSubject } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { SpinnerService } from '../../../../../../../../shared/services/spinner.service';
import { ShopAPIService } from '../../../../../../../../shared/api/shop-api.service';
import { ShopModel } from '../../../../../../../../shared/models/shop.model';

import { ConfirmationDialogComponent } from '../../../../../../../../shared/dialogs/confirmation-modal/confirmation-modal.component';
import { ConfirmationModel } from '../../../../../../../../shared/models/confirmation.model';

@Component({
  selector: 'app-administration-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.css']
})

export class ShopsComponent implements OnInit, OnDestroy {
  public items: ShopModel[] = [];
  public numTotal = 0;
  private subscription: Subscription = new Subscription();
  private loadData: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public query: any = {
    keyword: '',
    page: 1,
    limit: 10,
  };

  constructor(private dialog: MatDialog,
    private spinnerService: SpinnerService,
    private shopAPIService: ShopAPIService) {}

  ngOnInit() {
    this.subscription.add(this.initLoadData().subscribe());
    this.loadData.next(true);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  initLoadData(): Observable<any> {
    return this.loadData.pipe(
      switchMap((update: boolean) => {
        if (update) {
          this.spinnerService.start();
          return this.shopAPIService.getShopsByQuery(this.query).pipe(
            map(({items, numTotal}) => {
              this.items = items || [];
              this.numTotal = numTotal || 0;
              this.spinnerService.stop();
              return items;
            }),
            catchError(error => {
              this.spinnerService.stop();
              throw error;
            })
          )
        } else {
          return of(null);
        }
      })
    );
  }

  openConfirmationDialog(data: ConfirmationModel, afterClosed: (data: any) => void): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '30%',
      data
    });
    dialogRef.afterClosed().subscribe(afterClosed);
  }

  openShopFormDialog(type: string = 'create', formData: any = {}, afterClosed: (formData: any) => void): void {
    const dialogRef = this.dialog.open(FormDialogComponent, {
      width: '70%',
      height: '80%',
      data: {
        type,
        formData
      }
    });

    dialogRef.afterClosed().subscribe(afterClosed);
  }

  createShopHandler() {
    this.openShopFormDialog('create', new ShopModel(), (loadData) => {
      this.loadData.next(true);
    });
  }

  filterChange(query: any) {
    this.query = query;
    this.loadData.next(true);
  }

  editHandler(shop: ShopModel) {
    if (!shop || !shop._id)return;
    this.spinnerService.start();
    this.shopAPIService.getShopById(shop._id).pipe(
      map((_shop: ShopModel) => {
        this.spinnerService.stop();
        this.openShopFormDialog('update', _shop, (formData: any) => {
          this.loadData.next(true);
        })
        return shop;
      }),
      catchError((error: any) => {
        this.spinnerService.stop();
        return of(null);
      })
    ).subscribe();
  }

  deleteHandler(shop: ShopModel) {
    this.openConfirmationDialog({
      title: 'Delete Shop',
      question: 'Are you sure you want to delete Shop?',
      noButtonText: 'Close',
      yesButtonText: 'Yes, Delete',
    }, (result) => {
      result && this.deleteShop(shop);
    })

  }
  
  deleteShop(shop: ShopModel) {
    if(!shop || !shop._id) return;
    this.spinnerService.start();
    this.shopAPIService.deleteShop(shop._id)
      .pipe(
        map(result => {
          this.spinnerService.stop();
          this.loadData.next(true);
        }),
        catchError(error => {
          this.spinnerService.stop();
          throw error;
        })
      ).subscribe();
  }

  enableStatusChangeHandler(data: {enabled: boolean, _id: string}) {
    if (!data._id) return;
    this.spinnerService.start();
    this.shopAPIService.updateShopEnabledStatus(data._id, {enabled: data.enabled}).pipe(
      map(result => this.loadData.next(true)),
      catchError(error => {
        this.loadData.next(true);
        return of();
      })
    ).subscribe(result => this.spinnerService.stop());
  }
}