import { Component, OnInit, OnDestroy, EventEmitter, Input, Output } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { generateFriendlyId } from 'src/app/shared/utils/functions';
import { validateAllFormFields } from '../functions';
import { ProductAPIService } from 'src/app/shared/api/product-api.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { LocalizationString } from 'src/app/shared/models/localization-string.model';
import * as _ from 'lodash';
import { ProductModel } from 'src/app/shared/models/product.model';
import { CategoriesAPIService } from 'src/app/shared/api/categories-api.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { CategoryModel } from 'src/app/shared/models/category.model';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-product-categories-and-keywords-component',
  templateUrl: './categories-and-keywords.component.html',
  styleUrls: ['./categories-and-keywords.component.css']
})

export class CategoriesAndKeywordsComponent implements OnInit, OnDestroy {
  @Input('formData') formData!: ProductModel;
  @Output('productChangeHandler') productChangeEmitter: EventEmitter<any> = new EventEmitter();
  private subscription: Subscription = new Subscription();

  public categories: CategoryModel[] = [];
  public openCategoryMap: any = {};

  public productCategories: CategoryModel[] = [];
  public productKeywords: string[] = [];


  // ==========
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  
  constructor(private productAPIService: ProductAPIService,
    private spinnerService: SpinnerService,
    private categoriesAPIService: CategoriesAPIService) { }

  ngOnInit() {
    this.productCategories = this.formData.categories || [];
    this.productKeywords = this.formData.keywords || [];
    this.loadCategories();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // =================================LOADS_AND_INITS==================================================
  loadCategories() {
    this.subscription.add(this.categoriesAPIService.getAllCategories(20).pipe(
      tap(() => this.spinnerService.start()),
      map((categories: CategoryModel[]) => {
        this.categories = categories;
      }),
      catchError((error: Error) => {
        this.spinnerService.stop();
        console.log(error);
        throw error;
      }),
      tap(() => this.spinnerService.stop())
    ).subscribe());
  }

  // ====================================SUBSCRIPTIONS=================================================

  // ==================================================================================================
  addCategoryClickHandler(category: CategoryModel) {
    if (!this.isCategoryLegalToBeAdded(category)) return;
    this.productCategories.push(category);
  }

  removeCategoryClickHandler(category: CategoryModel) {
    if (this.isCategoryAdded(category)) {
      this.removeCategory(category);
    }
  }

  isCategoryLegalToBeAdded(category: CategoryModel) {
    return category && !this.isCategoryAdded(category) && !this.isAnyOfChildCategoryAdded(category) && !this.isAnyOfParentCategoryAdded(category);
  }

  isCategoryAdded(category: CategoryModel): boolean {
    return this.containsById(category, this.productCategories);
  }

  containsById(target: CategoryModel, list: CategoryModel[]): boolean {
    let contains: boolean = false;
    if (!target || !target._id) return false;
    for (let item of list) {
      contains = contains || item._id === target._id;
    }
    return contains;
  }

  indexById(target: CategoryModel, list: CategoryModel[]): number {
    let index: number = -1;
    if (!target || !target._id) index;
    for (let i=0; i<list.length; i++) {
      if (list[i]._id === target._id) {
        index = i;
      }
    }
    return index;
  }

  isAnyOfChildCategoryAdded(category: CategoryModel): boolean {
    if (!category.childCategories || !category.childCategories.length) return false;
    
    let result: boolean = false;

    for (let child of category.childCategories) {
      if (this.containsById(child, this.productCategories)) {
        return true;
      }
      result = result || this.isAnyOfChildCategoryAdded(child);
    }
    return result;
  }

  isAnyOfParentCategoryAdded(category: CategoryModel): boolean {
    if (!category.parentCategory) return false;
    for (let productCategory of this.productCategories) {
      if (productCategory._id === category.parentCategory) {
        return true;
      }
    }
    let result: boolean = false;
    let parentCategoryId = typeof category.parentCategory === 'object'? category.parentCategory._id : category.parentCategory;
    let parentCategory = this.findCategoryById(parentCategoryId, this.categories);
    if (!parentCategory) return false;
    result = result || this.isAnyOfParentCategoryAdded(parentCategory);
    return result;
  }

  findCategoryById(categoryId: string, categories: CategoryModel[]): CategoryModel | null {
    let resultCategory: CategoryModel | null = null;
    for (let category of categories) {
      if (category && category._id === categoryId) {
        return category;
      }
      resultCategory = resultCategory || this.findCategoryById(categoryId, category.childCategories || []);
      if (resultCategory) return resultCategory;
    }
    return resultCategory;    
  }

  addKeyword(event: MatChipInputEvent): void {
    const input = event.input;
    const value = (event.value || '').trim();

    // Add our fruit
    if (value && this.productKeywords.indexOf(value) < 0) {
      this.productKeywords.push(value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeKeyword(keyword: string) {
    const index = this.productKeywords.indexOf(keyword);

    if (index >= 0) {
      this.productKeywords.splice(index, 1);
    }
  }

  // this should be exist empty .... !!!
  addCategory(event: MatChipInputEvent) {    
  }

  removeCategory(category: CategoryModel) {
    const index = this.indexById(category, this.productCategories);
    if (index >= 0) {
      this.productCategories.splice(index, 1);
    }
  }
  
  toggleCategory(category: CategoryModel) {
    if (!category) return;
    this.openCategoryMap[category._id] = !this.openCategoryMap[category._id];
  }

  closeCategory(category: CategoryModel) {
    if (!category) return;
    this.openCategoryMap[category._id] = false;
  }

  isOpen(category: CategoryModel) {
    return !!this.openCategoryMap[category._id];
  }

  isProductCategoriesAndKeywordsFormValid() {
    return true;
  }
  // ==================================================================================================
  submitProductCategoriesAndKeywordsForm() {
    if (!this.formData._id) return;
    let updateData = {
      categories: this.productCategories.map((item: CategoryModel) => item._id).filter(item => !!item) || [],
      keywords: this.productKeywords || [],
    };

    this.productAPIService.updateProductCategoriesAndKeywords(this.formData._id, updateData).pipe(
      tap(() => this.spinnerService.start()),
      map((product: ProductModel) => {
        this.productChangeEmitter.emit(product);
      }),
      catchError((error: Error) => {
        console.log(error);
        this.spinnerService.stop();
        throw error;
      }),
      tap(() => this.spinnerService.stop())
    ).subscribe();
  }
}