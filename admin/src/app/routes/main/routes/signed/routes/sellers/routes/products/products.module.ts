import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../shared/shared.module';
import { ProductsComponent } from './products.component';
import { ProductsRoutingModule } from './products-routing.module';
import { ComponentsModule } from './components/components.module';
import { DialogsModule } from './dialogs/dialogs.module';
import { ProductSharedComponentsModule } from '../shared/components/product/product.module';

@NgModule({
  imports: [
    SharedModule,
    ProductsRoutingModule,
    ComponentsModule,
    DialogsModule,
    ProductSharedComponentsModule
  ],
  declarations: [
    ProductsComponent
  ],
  providers: [
  ],
  exports: [
  ]
})
export class ProductsModule { }
