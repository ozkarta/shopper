import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { generateFriendlyId } from '../../../../../../../../../../shared/utils/functions';

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'category-form-modal.component.html',
  styleUrls: ['category-form-modal.component.css']
})
export class CategoryFormModalComponent implements OnInit {
  public form!: FormGroup;
  private formConfig = {
  };

  constructor(
    public fb: FormBuilder,
    public dialogRef: MatDialogRef<CategoryFormModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { type: string, formData: any }) { }

  ngOnInit() {
    if (!this.data) return;
    this.formConfig = {

      includeInSearch: [this.data.formData && this.data.formData.includeInSearch || ''],

      categoryNameEN: [this.data.formData && this.data.formData.categoryName && this.data.formData.categoryName.en || '', [Validators.required, Validators.minLength(3)]],
      categoryNameGE: [this.data.formData && this.data.formData.categoryName && this.data.formData.categoryName.ge || '', [Validators.required, Validators.minLength(3)]],

      friendlyIdEN: [this.data.formData && this.data.formData.friendlyId && this.data.formData.friendlyId.en || '', [Validators.required, Validators.minLength(3)]],
      friendlyIdGE: [this.data.formData && this.data.formData.friendlyId && this.data.formData.friendlyId.ge || '', [Validators.required, Validators.minLength(3)]],
    }
    this.form = this.fb.group(this.formConfig);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    if (!this.form.valid) {
      this.validateAllFormFields(this.form);
      return;
    }
    let data = Object.assign({}, this.form.value);
    data.includeInSearch = !!data.includeInSearch;
    this.dialogRef.close(data);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}