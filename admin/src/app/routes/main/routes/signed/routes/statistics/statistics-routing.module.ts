import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatisticsComponent } from './statistics.component';
import { HasStatisticsPermissionGuard } from './statistics-guard.service';

const routes: Routes = [
  {
    path: '',
    component: StatisticsComponent,
    canActivate: [
      HasStatisticsPermissionGuard
    ],
    children: [
      {
        path: 'error-logs',
        loadChildren: () => import('./routes/error-logger/error-logger.module').then(m => m.ErrorLoggerModule)
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatisticsRoutingModule { }
