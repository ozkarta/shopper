import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent } from './categories.component';
import { AllCategoriesResolver } from './categories-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: CategoriesComponent,
    resolve: {
      allCategories: AllCategoriesResolver
    },
    canActivate: [
    ],
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
