import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../shared/shared.module';
import { AdministrationComponent } from './administration.component';
import { AdministrationRoutingModule } from './administration-routing.module';
import { HasAdministrationPermissionGuard } from './administration-guard.service';

@NgModule({
  imports: [
    SharedModule,
    AdministrationRoutingModule
  ],
  declarations: [
    AdministrationComponent
  ],
  providers: [
    HasAdministrationPermissionGuard
  ]
})
export class AdministrationModule { }
