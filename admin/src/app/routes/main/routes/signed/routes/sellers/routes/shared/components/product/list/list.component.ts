import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { formatDate, registerLocaleData } from '@angular/common';
import { ShopModel } from '../../../../../../../../../../../shared/models/shop.model';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ProductModel, ProductMediaItem } from '../../../../../../../../../../../shared/models/product.model';
import { mediaConstants } from '../../../../../../../../../../../shared/constants/media-files';
import localeUS from '@angular/common/locales/es-US';
import { FileAPIService } from '../../../../../../../../../../../shared/api/file-api.service';
import { MediaService } from 'src/app/shared/services/media.service';
import { MediaImageResolutions } from 'src/app/shared/enums';
import { IMedia } from 'src/app/shared/interfaces/media';
registerLocaleData(localeUS);

@Component({
  selector: 'app-product-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent {
  @Input() items!: ProductModel[];
  @Output('editHandler') editEmitter: EventEmitter<ProductModel> = new EventEmitter();
  @Output('deleteHandler') deleteEmitter: EventEmitter<ProductModel> = new EventEmitter();
  @Output('enableStatusChangeHandler') enableStatusChangeEmitter: EventEmitter<any> = new EventEmitter();

  public columnsToDisplay = [
    'media',
    'createdAt',
    'title',
    'shortDescription',
    'quantity',
    'actions'
  ];
  public displayColumnTranslations: any = {
    'media': {
      title: 'Media',
      type: 'image',
      transformer: (element: any) => {
        return ``;
      }
    },
    'createdAt': {
      title: 'Create Date',
      transformer: (element: any) => {
        return element['createdAt'] && formatDate(element['createdAt'], 'M/d/yy, h:mm a', 'es-US') || 'N/A';
      }
    },
    'title': {
      title: 'Title',
      type: 'link',
      linkRoute: (element: any) => {
        return `/signed/sellers/products/${element._id}`
      },
      transformer: (element: any) => {
        return `${(element.title && element.title.en) || 'N/A'}`;
      }
    },
    'shortDescription': {
      title: 'Description',
      transformer: (element: any) => {
        return element.shortDescription? element.shortDescription.en : '';
      }
    },
    'quantity': {
      title: 'Quantity',
    },
  }

  constructor(
    private fileAPIService: FileAPIService,
    private mediaService: MediaService
  ) {}

  getProductImage(_product: any) {
    let primaryImage: IMedia = ((_product?.media || []).filter((item: any) => item.isPrimary)[0])?.item || null;
    return this.mediaService.getProductMediaFileUrlByMediaImageResolutions(primaryImage, MediaImageResolutions.LOW_RESLUTION_IMAGE)
  }

  // ====================================

  editClickHandler(item: any) {
    this.editEmitter.next(item? new ProductModel(item): undefined);
  }

  deleteClickHandler(item: any) {
    this.deleteEmitter.emit(item? new ProductModel(item): undefined);
  }

  changeEnableStatusHandler(event: MatSlideToggleChange, product: ProductModel) {
    this.enableStatusChangeEmitter.emit({
      enabled: event.checked,
      _id: product._id
    });
  }
}