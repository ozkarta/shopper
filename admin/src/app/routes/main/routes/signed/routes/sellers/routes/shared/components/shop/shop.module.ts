import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../../shared/shared.module';
import { OwnerFormComponent } from './owner/owner-form.component';
import { ShopInfoFormComponent } from './shop-info/shop-info-form.component';
import { MediaFormComponent } from './media/media.component';
import { ContactInfoFormComponent } from './contact-info/contact-info.component';
import { CategoriesAndKeywordsFormComponent } from './categories-and-keywords/categories-and-keywords.component';
import { BusinessInfoFormComponent } from './business-info/business-info.component';

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    OwnerFormComponent,
    ShopInfoFormComponent,
    MediaFormComponent,
    ContactInfoFormComponent,
    CategoriesAndKeywordsFormComponent,
    BusinessInfoFormComponent
  ],
  exports: [
    OwnerFormComponent,
    ShopInfoFormComponent,
    MediaFormComponent,
    ContactInfoFormComponent,
    CategoriesAndKeywordsFormComponent,
    BusinessInfoFormComponent
  ],
})
export class ShopSubFormsModule { }
