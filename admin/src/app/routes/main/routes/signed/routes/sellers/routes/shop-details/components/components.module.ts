import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../shared/shared.module';
import { ProductSharedComponentsModule } from '../../shared/components/product/product.module';

import { ProductsModule } from './products/products.module';
@NgModule({
  imports: [
    SharedModule,
    ProductSharedComponentsModule,
    ProductsModule,
  ],
  declarations: [
  ],
  providers: [
  ],
  exports: [
    ProductsModule,
  ]
})
export class ComponentsModule { }
