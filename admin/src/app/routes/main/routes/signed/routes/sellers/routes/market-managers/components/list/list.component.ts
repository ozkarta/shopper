import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { formatDate, registerLocaleData } from '@angular/common';
import localeUS from '@angular/common/locales/es-US';
registerLocaleData(localeUS);

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent {
  @Input() items!: any[];
  @Output('editHandler') editEmitter: EventEmitter<any> = new EventEmitter();
  @Output('deleteHandler') deleteEmitter: EventEmitter<any> = new EventEmitter();

  public columnsToDisplay = [
    'createdAt',
    'name',
    'email',

    'actions'
  ];
  public displayColumnTranslations: any = {
    'createdAt': {
      title: 'Registration Date',
      transformer: (element: any) => {
        return formatDate(element['createdAt'], 'M/d/yy, h:mm a', 'es-US');
      }
    },
    'name': {
      title: 'Name',
      transformer: (element: any) => {
        return `${element.firstName} ${element.lastName}`;
      }
    },
    'email': {
      title: 'Email',
    },
  }

  // ====================================

  editClickHandler(item: any) {
    this.editEmitter.next(item);
  }

  deleteClickHandler(item: any) {
    this.deleteEmitter.emit(item);
  }
}