import { Component, OnInit, OnDestroy, Input, ViewChild, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription, of, Observable, Observer, zip, forkJoin } from 'rxjs';
import { ImageCropperComponent, CropperSettings } from 'ngx-img-cropper';
import { FileAPIService } from '../../../../../../../../../../../shared/api/file-api.service';
import { SpinnerService } from '../../../../../../../../../../../shared/services/spinner.service';
import { tap, map, catchError } from 'rxjs/operators';
import { convertBase64ToBlob } from '../../../../../../../../../../../shared/utils/functions';
import { MediaModel } from '../../../../../../../../../../../shared/models/media.model';
import { FileModel } from '../../../../../../../../../../../shared/models/file.model';
import { ShopAPIService } from '../../../../../../../../../../../shared/api/shop-api.service';

import { formatDate, registerLocaleData } from '@angular/common';
import localeUS from '@angular/common/locales/es-US';
import { ProductModel, ProductMediaItem } from '../../../../../../../../../../../shared/models/product.model';
import { ProductAPIService } from '../../../../../../../../../../../shared/api/product-api.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { mediaConstants } from 'src/app/shared/constants/media-files';
import { IMedia } from 'src/app/shared/interfaces/media';
import { MediaService } from 'src/app/shared/services/media.service';
import { MediaImageResolutions } from 'src/app/shared/enums';
registerLocaleData(localeUS);

@Component({
  selector: 'app-product-media-form-component',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})

export class MediaFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input('formData') formData!: ProductModel;
  @Output('shopChangeHandler') shopChangeEmitter: EventEmitter<any> = new EventEmitter();
  private subscription: Subscription = new Subscription();
  public productMedia: ProductMediaItem[] = [];
  public productMediaItemBeendEdited: ProductMediaItem | null | undefined;
  // ==============
  public columnsToDisplay = [
    'media',
    'createdAt',
    'fileName',
    'size',
    'mimetype',
    'actions'
  ];
  public displayColumnTranslations: any = {
    'media': {
      title: 'Media',
      type: 'image',
      transformer: (element: FileModel) => {
        if (!element) return 'N/A';
        return `Image`;
      }
    },
    'createdAt': {
      title: 'Upload Date',
      transformer: (element: FileModel) => {
        if (!element) return 'N/A';
        return element['createdAt'] && formatDate(element['createdAt'], 'M/d/yy, h:mm a', 'es-US') || 'N/A';
      }
    },
    'fileName': {
      title: 'File Name',
      transformer: (element: FileModel) => {
        if (!element) return 'N/A';
        return element.filename;
      }
    },
    'size': {
      title: 'Size',
      transformer: (element: FileModel) => {
        if (!element) return 'N/A';
        return `${(element.size / 1000).toFixed(2) } KB`;
      }
    },
    'mimetype': {
      title: 'Mime Type',
      transformer: (element: FileModel) => {
        if (!element) return 'N/A';
        return element.mimetype;
      }
    },
  }

  public cropperSettings!: CropperSettings;
  public cropperData: any;
  @ViewChild('mainImageCropper') cropper!: ImageCropperComponent;

  constructor(private fileAPIService: FileAPIService,
    private spinnerService: SpinnerService,
    private productAPIService: ProductAPIService,
    private mediaService: MediaService) {
  }

  ngOnInit() {
    // this.shopMediaFormGroup = this.fb.group(this.shopMediaFormGroupConfig);
    this.productMedia = this.formData.media;

    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.keepAspect = true;

    this.cropperSettings.width = 90;
    this.cropperSettings.height = 120;

    this.cropperSettings.croppedWidth = 1200;
    this.cropperSettings.croppedHeight = 1600;
    this.cropperSettings.canvasWidth = 300;
    this.cropperSettings.canvasHeight = 400;



    this.cropperData = {};
  }

  ngOnChanges(changes: any) {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  submitProductMediaForm() {    
    if (!this.isProductMediaFormReadyForSubmit() || !this.formData._id) return;
    this.spinnerService.start();
    let _data = this.productMedia.map((item: ProductMediaItem) => {
      let _item: any = {};
      _item.isPrimary = item.isPrimary;
      _item.item = item.item._id
      return _item;
    });
    this.productAPIService.updateProductMedia(this.formData._id, _data).pipe(
      map((product: ProductModel) => {
        this.spinnerService.stop();
        this.shopChangeEmitter.emit(product);
      }),
      catchError((error: Error) => {
        console.log(error);
        this.spinnerService.stop();
        return of (null);
      })
    ).subscribe();
  }

  isProductMediaFormReadyForSubmit() {
    return true;
  }

  singleFileChangeListener($event: Event) {
    if (!$event || !$event.target) return;
    let file: File = Array.from(<FileList>(<HTMLInputElement>$event.target).files)[0];
    let reader: FileReader = new FileReader();
    let image:any = new Image();
    reader.onloadend = (loadEvent: any) => {
      image.src = loadEvent.target.result;
      this.cropper.setImage(image);
      this.cropperData.fileName = file.name;
    }
    reader.readAsDataURL(file);
  }

  changePrimaryStatusHandler(event: MatSlideToggleChange, element: ProductMediaItem) {
    let checked: boolean = event.checked;
    if (checked) {
      this.productMedia.forEach((item: ProductMediaItem) => {
        item.isPrimary = false;
      });
      element.isPrimary = true;
    } else {
      element.isPrimary = checked;
    }
  }

  cancelImageEditClickHandler() {
    this.cropper.reset();
    this.productMediaItemBeendEdited = null;
  }

  editClickHandler(element: ProductMediaItem) {
    if (!element) return;
    if (this.productMediaItemBeendEdited === element) return;
    if (this.productMediaItemBeendEdited) {
      this.productMediaItemBeendEdited = null;
      this.cropper.reset();
    }
    this.productMediaItemBeendEdited = element;

    let image: HTMLImageElement = new Image();
    image.crossOrigin = "Anonymous";
    image.src = this.getProductMediaSRC(element, MediaImageResolutions.ORIGINAL_IMAGE);
    image.onload = () => {
      this.cropper.setImage(image);
      this.cropperData.fileName = typeof element.item.originalImage === 'object'?  element.item.originalImage.filename : 'file-name';
    }

    
  }

  deleteClickHandler(element: ProductMediaItem) {
    if (!element) return;
    if (this.productMediaItemBeendEdited === element) return;
    this.productMedia.splice(this.productMedia.indexOf(element), 1);
    this.productMedia = [...this.productMedia];
  }

  uploadCropperImage() {
    if (!this.cropperData || !this.cropperData.image) return;
    this.spinnerService.start();
    let originalImaegBlob: Blob | null;
    try {
      originalImaegBlob = convertBase64ToBlob(this.cropperData.original.src);
    } catch (error) {
      originalImaegBlob = null;
    }
    this.upladCroppedImageAlngWithOriginal(originalImaegBlob, convertBase64ToBlob(this.cropperData.image), this.cropperData.fileName).pipe(
      // tap(() => this.spinnerService.start()),
      map((media: MediaModel) => {
        if (this.productMediaItemBeendEdited) {
          this.productMediaItemBeendEdited.item = media;
          this.productMedia = [...this.productMedia];
        } else {
          this.productMedia = [...this.productMedia, {
            item: media,
            isPrimary: false,
          }];
        }
        
        this.productMediaItemBeendEdited = null;
        this.cropper.reset();
      }),
      catchError((error: Error) => {
        console.log(error);
        throw error;
      }),
      tap(() => this.spinnerService.stop()),
    ).subscribe();
  }

  // ==================

  uploadSingleImage(file: Blob, fileName: string): Observable<MediaModel> {
    return this.fileAPIService.uploadSingleImage(file, fileName).pipe(
      tap(() => this.spinnerService.start()),
      map((result: MediaModel) => result),
      tap(() => this.spinnerService.stop()),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  upladCroppedImageAlngWithOriginal(original: Blob | null, cropped: Blob, fileName: string): Observable<MediaModel> {
    return this.fileAPIService.upladCroppedImageAlngWithOriginal(this.productMediaItemBeendEdited?.item._id || null, original, cropped, fileName).pipe(
      tap(() => this.spinnerService.start()),
      map((result: MediaModel) => result),
      tap(() => this.spinnerService.stop()),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  // ==============================================================
  // getFileDownloadUrl(fileId: string) {
  //   if (typeof fileId !== 'string') return;
  //   return this.fileAPIService.getFileReadableStreamUrl(fileId);
  // }

  getProductMediaSRC(productMedia: any, mediaImageReslution: MediaImageResolutions = MediaImageResolutions.LOW_RESLUTION_IMAGE) {
    let item: IMedia  = productMedia?.item;
    return this.mediaService.getProductMediaFileUrlByMediaImageResolutions(item, mediaImageReslution)
  }
}