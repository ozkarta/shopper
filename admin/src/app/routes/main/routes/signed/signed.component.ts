import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { AuthService } from '../../../../shared/services/auth.service';
import { Router } from '@angular/router';
import { UserAPIService } from 'src/app/shared/api/user-api.service';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
@Component({
  selector: 'app-signed-component',
  templateUrl: './signed.component.html',
  styleUrls: ['./signed.component.css']
})
export class SignedComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private authService: AuthService,
    private router: Router,
    private userAPIServicee: UserAPIService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logOut() {
    this.userAPIServicee.logOut().pipe(
      map((data: any) => {
        return data;
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe(
      () => {
        this.authService.logOutUser();
        location.reload();
      }
    )
    
  }
}