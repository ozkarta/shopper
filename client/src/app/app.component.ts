import { ApplicationInitStatus, Component, OnInit } from '@angular/core';
import { CartAPIService } from './shared/http/cart-api.service';
import { ApplicationStateService } from './shared/services/application-state.service';
import { CurrentUserService } from './shared/services/current-user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public get isMobile() {
    return this.appStateService.isMobile;
  }

  public get isWeb() {
    return !this.appStateService.isMobile;
  }
  constructor(
    private currentUserService: CurrentUserService,
    private appStateService: ApplicationStateService
  ) {}

  ngOnInit(): void {}
}
