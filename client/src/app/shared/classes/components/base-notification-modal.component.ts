import { Directive, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ICustomNotificationButton, INotificationServiceOptions } from 'src/app/shared/interfaces/notifications';
import { ModalService } from '../../services/modals.service';

@Directive()
export class BaseNotificationModalComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private notificationStream: Observable<INotificationServiceOptions>;

  private defaultCloseAfterOption: number = 10 * 1000;
  public notifications: INotification[] = [];
  constructor(
    protected data: {notificationStream: Observable<INotificationServiceOptions>},
    protected modalService: ModalService,
    protected router: Router,
    protected route: ActivatedRoute) {
      this.notificationStream = data.notificationStream;
  }

  ngOnInit() {
    this.subscribeNotificationStream();
    // this.subscribeRouterEventChanges();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  // ==========================================

  subscribeRouterEventChanges() {
    this.subscription.add(this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        for (let notification of this.notifications) {
          for (let timeout of notification.timeOuts) {
            clearTimeout(timeout);
          }
          notification.show = false;
          // remove from DOM
          notification.timeOuts.push(setTimeout(() => {
            this.notifications.splice(this.notifications.indexOf(notification), 1);
          }, 200));
        }
      }
    }))
  }

  subscribeNotificationStream() {
    this.subscription.add(
      this.notificationStream.subscribe(
        (_notification: INotificationServiceOptions) => {
          if (!_notification) return;
          let notification: any = Object.assign({}, _notification);
          notification.timeOuts = [];
          this.setNotificationTimeOuts(notification);
          this.notifications.push(notification);
        }
      )
    );
  }

  setNotificationTimeOuts(notification: INotification) {
    // show
    notification.timeOuts.push(setTimeout(() => {
      notification.show = true;
      // hide
      if (notification.autoClose) {
        notification.timeOuts.push(setTimeout(() => {
          notification.show = false;
          // remove from DOM
          notification.timeOuts.push(setTimeout(() => {
            this.notifications.splice(this.notifications.indexOf(notification), 1);
          }, 200));
        }, notification.autoCloseAfter || this.defaultCloseAfterOption))
      }
    }, 100));
  }

  getNotificationClass(notification: INotification) {
    let classObject: any = {};
    classObject.show = notification.show;
    classObject[`alert-${notification.type}`] = true;
    return classObject;
  }

  customButtonClickHandler(notification: INotification) {
    let button: ICustomNotificationButton | null | undefined = notification.customButton;
    if (!button || !button.clickHandler || typeof button.clickHandler !== 'function') {
      this.closeNotification(notification);
      return;
    }
    button.clickHandler(() => {
      this.closeNotification(notification);
    })
  }

  closeHandler(notification: INotification) {
    if (notification.closeHandler && typeof notification.closeHandler === 'function') {
      notification.closeHandler(() => {
        this.closeNotification(notification);
      })
    } else {
      this.closeNotification(notification);
    }
  }

  closeNotification(notification: INotification) {
    notification.show = false;
    setTimeout(() => {
      notification.timeOuts.forEach((timeOut: any) => {
        clearTimeout(timeOut);
      });
      this.notifications.splice(this.notifications.indexOf(notification), 1);
    }, 200);
  }
}


interface INotification extends INotificationServiceOptions {
  show: boolean;
  timeOuts: any[];
}
