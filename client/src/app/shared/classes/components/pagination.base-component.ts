import { Directive, EventEmitter, Input, OnDestroy, OnInit, Output } from "@angular/core";

@Directive()
export class PaginationBaseComponent implements OnInit, OnDestroy {
  @Input('page') page!: number;
  @Input('limit') limit!: number;
  @Input('numTotal') numTotal!: number;

  @Output('pageChangeHandler') pageChangeEmitter: EventEmitter<number> = new EventEmitter();

  private get _pageQTY(): number {
    return (Math.floor(this.numTotal / this.limit) || 0) + 1;
  }
  public get pages() {
    return [...Array(this._pageQTY).keys()].map((i: number) => i + 1 );
  }
  public get currentPage() {
    return this.page || 1;
  }

  

  constructor() {

  }

  ngOnInit() {

  }

  ngOnDestroy() {

  }


  public canGoLeft() {
    return this.page > 1;
  }

  public canGoRight() {
    return this.page < this._pageQTY
  }

  public isActivePage(page: number) {
    return page === this.page;
  }
  public goLeft() {
    if (!this.canGoLeft()) return;
    this.goToPage(this.currentPage - 1);
  }

  public goRight() {
    if (!this.canGoRight()) return;
    this.goToPage(this.currentPage + 1);
  }

  public goToPage(pageNum: number) {
    if (!pageNum || pageNum === this.currentPage) return;
    this.pageChangeEmitter.emit(pageNum);
  }
}