import * as _ from 'lodash';

export class Language {
  public name!: string;
  public codeISOV1!: string;
  public codeISOV2!: string;

  public createdAt?: Date;
  public updatedAt?: Date;

  constructor(language?: Language) {
    if (language) {
      _.merge(this, language);
    }
  }
}