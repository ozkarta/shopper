export class Utils {
  static makeRandomString(upperCaseQTY: number = 1, lowerCaseQTY: number = 5, numericQTY: number = 1, specialCharactersQTY: number = 0) {

    let upperCaseSymbols: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    let lowerCaseSymbols: string = 'abcdefghijklmnopqrstuvwxyz'
    let numericSymbols: string = '0123456789';
    let specialCharacterSymbols: string = '~!@#$%^&*()_+={[}]|\:;<,>.?/';
  
    let result = `${this.makePasswordSequence(upperCaseQTY, upperCaseSymbols)}${this.makePasswordSequence(numericQTY, numericSymbols)}${this.makePasswordSequence(specialCharactersQTY, specialCharacterSymbols)}${this.makePasswordSequence(lowerCaseQTY, lowerCaseSymbols)}`;
    return this.shuffleArray(result.split('')).join('');
  }

  // =======================================================================================================
  private static makePasswordSequence(length: number, characters: string) {
    var result = '';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  
  private static shuffleArray(array: any[]): any[] {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }
}