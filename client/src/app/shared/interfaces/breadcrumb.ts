import { ILocalizationString } from "./localization-string";

export interface IBreadCrumbDataItem {
  title: ILocalizationString,
  path: string,
  queryParams?: any,
  queryParamsHandling?: 'merge' | 'preserve'
}