export interface ILanguage {
  name: string;
  codeISOV1: string;
  codeISOV2: string;

  createdAt?: Date;
  updatedAt?: Date;
}
