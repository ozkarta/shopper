export interface ILocalizationString {
  en: string;
  ge: string;
}
