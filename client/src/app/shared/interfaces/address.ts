import { ICountry } from './coountry';

export interface IAddress {
  street: string;
  city: string;
  province: string;
  country: ICountry;
  zip: string;

  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}
