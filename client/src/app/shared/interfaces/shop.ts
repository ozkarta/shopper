import { IAddress } from './address';
import { IBusiness } from './business';
import { ICategory } from './category';
import { IFeedback } from './feedback';
import { ILocalizationString } from './localization-string';
import { IMedia } from './media';
import { IProduct } from './product';
import { IRating } from './rating';
import { ISales } from './sales';
import { IUser } from './user';

export interface IShop {
  owner: IUser;

  business: IBusiness;

  title: ILocalizationString;
  friendlyId: ILocalizationString;
  shortDescription: ILocalizationString;
  longDescription: ILocalizationString;
  termsAndCondition: ILocalizationString;

  addresses: IAddress[];
  phones: string[];
  emails: string[];
  facebook: string;

  categories: ICategory[];
  keywords: string[];
  // products may not be referenced here
  products: IProduct[];

  media: IMedia;
  rating: IRating[]; //
  sales: ISales[];
  feedback: IFeedback[];

  // SYSTEM
  enabled: boolean;
  deleted: boolean;

  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}
