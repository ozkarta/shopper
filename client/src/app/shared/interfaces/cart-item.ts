import { IProduct } from "./product";

export interface ICartItem {
  product: IProduct;
  qty: number;
  userId: string;
  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}