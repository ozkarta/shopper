import { IFile } from "./file";

export interface IMedia {  
  originalImage: IFile | string;  
  highResolutionImage: IFile | string;  
  mediumResolutionImage: IFile | string;  
  lowResolutionImage: IFile | string;

  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}
