import { ICategory } from './category';
import { IFeedback } from './feedback';
import { ILocalizationString } from './localization-string';
import { IMedia } from './media';
import { IRating } from './rating';
import { ISales } from './sales';
import { IShop } from './shop';

export interface IProduct {
  shop: IShop;
  friendlyId: ILocalizationString;
  title: ILocalizationString;
  shortDescription: ILocalizationString;
  longDescription: ILocalizationString;
  media: {
    item: IMedia;
    isPrimary: boolean;
  }[];
  categories: ICategory[];
  keywords: string[];
  
  quantity: number;
  price: number;

  rating: IRating[];
  sales: ISales[];
  feedback: IFeedback[];

  enabled: boolean;
  deleted: boolean;
  
  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}
