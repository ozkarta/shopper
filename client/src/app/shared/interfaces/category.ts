import { ILocalizationString } from './localization-string';

export interface ICategory {

  categoryName: ILocalizationString;
  friendlyId: ILocalizationString;
  includeInSearch: boolean;
  sortWeight: number;

  parentCategory: ICategory | string;
  childCategories: ICategory[];

  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;

  // virtuals
  hasChild: boolean;
  hasPopulatedChild: boolean;
}
