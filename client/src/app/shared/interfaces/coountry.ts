export interface ICountry {
  name: string;
  alpha_2: string;
  alpha_3: string;
  countryCode: string;
  iso_3166_2: string;
  region: string;
  subRegion: string;
  intermediateRegion: string;
  regionCode: string;
  subRegionCode: string;
  intermediateRegionCode: string;
}
