export interface IFeedback {

  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}