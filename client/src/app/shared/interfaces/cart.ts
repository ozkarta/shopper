import { ICartItem } from "./cart-item";

export interface ICart {
  userId: string;
  cartItems: ICartItem[];

  numTotal: number;
  subTotal: number;
  shipping: number;
  tax: number;
  total: number;

  deleted?: boolean;

  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}