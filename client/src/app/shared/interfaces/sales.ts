export interface ISales {
  // ==========================================
  createdAt?: Date;
  updatedAt?: Date;
  _id: string;
  id: string;
}