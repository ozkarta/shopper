import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { IProduct } from "../interfaces/product";
const API_URL = environment.serverUrl + '/api/v1';
@Injectable()
export class ProductAPIService {
  constructor(
    private httpClient: HttpClient
  ) {}

  getProductsByQuery(params: any = {}): Observable<{items: any[], numTotal: number}> {
    return this.httpClient.get<{items: any[], numTotal: number}>(`${API_URL}/product/query`, {params});
  }

  getProductById(productId: string): Observable<IProduct> {
    return this.httpClient.get<IProduct>(`${API_URL}/product/${productId}`);
  }
}