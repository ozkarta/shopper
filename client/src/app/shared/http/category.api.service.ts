import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ICategory } from '../interfaces/category';

const API_URL = environment.serverUrl + '/api/v1';
@Injectable()

export class CategoryAPIService {
  constructor(private httpClient: HttpClient) { }

  public getCategories(categoryId?: string, depth?: number):Observable<ICategory[]> {
    let queryParams: HttpParams = new HttpParams();
    if (!!categoryId) {
        queryParams = queryParams.set('categoryId', categoryId);
    }
    if (depth && depth > 0) {
        queryParams = queryParams.set('depth', depth);
    }
    return this.httpClient.get<ICategory[]>(`${API_URL}/category`, {params: queryParams});
  }

  public getAscendantCategories(categoryId: string): Observable<ICategory[]> {
    return this.httpClient.get<ICategory[]>(`${API_URL}/category/ascendants/${categoryId}`);
  }
}