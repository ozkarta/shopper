import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpContext, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BYPASS_TOKEN_INTERCEPTION } from '../interceptors/token-interceptor.service';

const API_URL = environment.serverUrl + '/api/v1';
@Injectable()

export class UserAPIService {
  constructor(private httpClient: HttpClient) { }
  // =============================================================================
  getMe(): Observable<any> {
    return this.httpClient.get(`${API_URL}/user/me`);
  }

  getByQuery(params: any): Observable<any> {
    return this.httpClient.get(`${API_URL}/user/query`, {params});
  }
  // =============================================================================
  loginUser(userData: any): Observable<any> {
    return this.httpClient.post(`${API_URL}/user/login`, userData);
  }

  logOut(): Observable<any> {
    return this.httpClient.post(`${API_URL}/user/logout`, {});
  }

  refreshTokens(): Observable<any> {
    return this.httpClient.post(`${API_URL}/user/refresh-tokens`, {}, {context: (new HttpContext()).set(BYPASS_TOKEN_INTERCEPTION, true)}); 
  }

  registerPreAuthUser(payload: any): Observable<any> {
    return this.httpClient.post(`${API_URL}/user/preauth-client-user`, payload);
  }
}