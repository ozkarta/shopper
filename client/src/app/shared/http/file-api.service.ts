import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { IFile } from "../interfaces/file";
import { IMedia } from "../interfaces/media";
const API_URL = environment.serverUrl;

@Injectable()
export class FileAPIService {
  constructor(
    private httpClient: HttpClient
  ) {}

  public uploadSingleImage(file: Blob, fileName?: string): Observable<IMedia> {
    const formData = new FormData();
    formData.append('image-file', file, fileName);
    return this.httpClient.post<IMedia>(`${API_URL}/api/v1/file/image`, formData);
  }

  public uploadMultipleImages(files: Blob[]): Observable<IFile[]> {
    const formData = new FormData();
    for (let file of files) {
      formData.append('image-files', file);
    }
    return this.httpClient.post<IFile[]>(`${API_URL}/api/v1/file/image`, formData);
  }

}