import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { ICart } from "../interfaces/cart";
import { ICartItem } from "../interfaces/cart-item";
const API_URL = environment.serverUrl + '/api/v1';

@Injectable()
export class CartAPIService {
  constructor(
    private httpClient: HttpClient
  ) {}

  getClientCart(): Observable<ICart> {
    return this.httpClient.get<ICart>(`${API_URL}/cart`);
  }

  addProductToCart(productId: string, qty: number) {
    return this.httpClient.post<ICart>(`${API_URL}/cart`, {productId, qty});
  }

  updateSingleCartItem(cartId: string, payload: ICartItem) {
    return this.httpClient.put<ICart>(`${API_URL}/cart/${cartId}`, payload);
  }

  updateCartItems(cartItems: ICartItem[]): Observable<ICart> {
    return this.httpClient.put<ICart>(`${API_URL}/cart`, cartItems);
  }

  removeCartItem(cartItemId: string) {
    return this.httpClient.delete<ICart>(`${API_URL}/cart/${cartItemId}`, {});
  }

}