import { Injectable } from '@angular/core';
//import { fetchBrowserDetails } from '../app/lib/join/helper'

@Injectable({ providedIn: 'root' })
export class ApplicationStateService {
  get isMobile() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      return true;
    } else {
      return false;
    }
  }
}
