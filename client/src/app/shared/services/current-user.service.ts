import { HttpContextToken } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject, of, throwError } from 'rxjs';
import { map, catchError, concatMap } from 'rxjs/operators';
import { AppLanguages, Currencies, UserRoles } from '../enums';
import { CartAPIService } from '../http/cart-api.service';
import { UserAPIService } from '../http/user-api.service';
import { ICart } from '../interfaces/cart';
import { IUser } from '../interfaces/user';

@Injectable({providedIn: 'root'})
export class CurrentUserService {
  // =====================================USER=================================================
  private _currentUser: BehaviorSubject<IUser | null> = new BehaviorSubject<IUser | null>(null);
  public currentUserObservable: Observable<any> = this._currentUser.asObservable();
  public get user(): IUser | null {
    return this._currentUser.value;
  }
  public get initialUser(): any {
    return {
      language: AppLanguages.ENGLISH,
      currency: Currencies.GEL,
    }
  }
  // ======================================CART==================================================
  private _cart: BehaviorSubject<ICart | null> = new BehaviorSubject<ICart | null>(null);
  public cartObservable: Observable<ICart | null> = this._cart.asObservable();
  public get cart(): ICart | null {
    return this._cart.value;
  }
  // ================================================
  private readonly accessTokenKey = 'access-token';
  private readonly refreshTokenKey = 'refresh-token';
  private readonly ExpectedCurrentUserRole  = 'client';

  constructor(
    private userApiService: UserAPIService,
    private cartAPIService: CartAPIService,
    ) {}

  logInUser(data: any): Observable<any> {
    return this.userApiService.loginUser(data).pipe(
      map(({accessToken, refreshToken}) => {
        this.setAuthParams({accessToken, refreshToken});
        return {accessToken, refreshToken};
      }),
      concatMap(() => {
        return this.userApiService.getMe().pipe(
          map(currentUser => {
            return this.handleUserLoad(currentUser);
          }),
        );
      }),
    )
  }

  logInPreAuthUser(data: any): Observable<any> {
    return this.userApiService.registerPreAuthUser(data).pipe(
      map(({accessToken, refreshToken}) => {
        this.setAuthParams({accessToken, refreshToken});
        return {accessToken, refreshToken};
      }),
      concatMap(() => {
        return this.userApiService.getMe().pipe(
          map(currentUser => {
            console.log('we got current user...');
            return this.handleUserLoad(currentUser);
          }),
        );
      }),
    )
  }

  logOutUser() {
    this.userApiService.logOut().pipe(
      map((result: any) => {
        return result;
      }),
      catchError((error: any) => {
        console.log(error);
        return of(null);
      })
    ).subscribe(
      () => {
        localStorage.removeItem(this.accessTokenKey);
        localStorage.removeItem(this.refreshTokenKey);
        this._currentUser.next(null);
      }
    );
  }

  isSigned() {
    // this is for test only
    return !!localStorage.getItem(this.accessTokenKey) && !!this.user && !!this.user['email'] && this.user['role'] === 'client';
  }

  getAuthParams() {
    return {
      'access-token': localStorage.getItem(this.accessTokenKey) || '',
    }
  }

  getAuthParamsWithRefreshToken() {
    return {
      'access-token': localStorage.getItem(this.accessTokenKey) || '',
      'refresh-token': localStorage.getItem(this.refreshTokenKey) || '',
    }
  }

  loadUser(): Observable<any> {
    let accessToken = this.getAuthParams()['access-token'];
    if (accessToken) {
      return this.userApiService.getMe().pipe(
        map(currentUser => {
          return this.handleUserLoad(currentUser);
        }),
        catchError((error: any) => {
          this.logOutUser();
          return throwError(() => error);
        })
      )
    }
    return throwError(() => new Error('Auth Params not found'));
  }

  loadCartData(): Observable<any> {
    return this.cartAPIService.getClientCart().pipe(
      map((cart: ICart) => {
        this.setCart(cart);
        return cart;
      }),
      catchError((error: any) => {
        console.log(error);
        return of(null);
      })
    )
  }

  public setCart(cart: ICart) {
    this._cart.next(cart);
  }
  // =============================================================================================
  private handleUserLoad(currentUser: any) {
    if (!currentUser) {
      this.logOutUser();
      return of(null);
    }
    if (![UserRoles.CLIENT, UserRoles.VISITOR].includes(currentUser['role'])) {
      this.logOutUser();
      return of(null);
    }
    this.setCurrentUser(currentUser);
    return currentUser;
  }

  private setCurrentUser(data: any) {
    this._currentUser.next(data);
  }
  // ===========================================================================================

  public setAuthParams({accessToken, refreshToken}: any) {
    localStorage.setItem(this.accessTokenKey, accessToken);
    localStorage.setItem(this.refreshTokenKey, refreshToken);
  }

  public setCurrency(currency: Currencies) {
    let _userCP = Object.assign({}, this.user, {currency});
    this.setCurrentUser(_userCP);
  }

  public setLanguage(language: AppLanguages) {
    let _userCP = Object.assign({}, this.user, {language});
    this.setCurrentUser(_userCP);
  }
}