import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject } from 'rxjs';

@Injectable()

export class LocalizationService {
  private _currentLanguage: BehaviorSubject<string> = new BehaviorSubject('en');
  public currentLanguage: Observable<string> = this._currentLanguage.asObservable();
  
  triggersetCurrentLanguage(language: string) {
    this._currentLanguage.next(language);
  }
}