import { Injectable } from '@angular/core';
import * as FingerprintJS from '@fingerprintjs/fingerprintjs';
@Injectable({providedIn: 'root'})
export class FingerprintService {
  private fpPromise: Promise<any> = FingerprintJS.load();
  private readonly visitorFIngerprintKey = "visitor_fingerprint"
  constructor() { }
  
  getFingerPrint(): Promise<string> {
    let fingerprint = this.getFingerprintFromStorage();
    if (!!fingerprint) {
      return Promise.resolve(fingerprint);
    }
    return this.fpPromise
          .then((agent: any) => {
            return agent.get()
          })
          .then((result: any) => {
            this.setFingerprintInStorage(result.visitorId);
            console.log(result.visitorId);
            return result.visitorId;
          });
  }

  private setFingerprintInStorage(fingerprint: string) {
    localStorage.setItem(this.visitorFIngerprintKey, fingerprint)
  }

  private getFingerprintFromStorage(): string | null{
    return localStorage.getItem(this.visitorFIngerprintKey);
  }
}