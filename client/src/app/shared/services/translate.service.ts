import { Injectable, ApplicationRef } from '@angular/core';
// import { CMSAPIService } from '../http/CMSAPIService';
import { BehaviorSubject, Observable } from 'rxjs';
import { Language } from '../classes/language';
import { ILanguage } from '../interfaces/language';

@Injectable()
export class TranslateService {
  private $variables: any[] = [];
  private _variables: BehaviorSubject<any> = new BehaviorSubject(null);
  public variables: Observable<any[]> = this._variables.asObservable();

  private $languages: ILanguage[] = [];
  private _languages: BehaviorSubject<any> = new BehaviorSubject(null);
  public languages: Observable<ILanguage[]> = this._languages.asObservable();


  private $selectedLanguage!: ILanguage;
  private _selectedLanguage: BehaviorSubject<any> = new BehaviorSubject(null);
  public selectedLanguage: Observable<ILanguage> = this._selectedLanguage.asObservable();

  // ================================================================================
  private defaultLanguage: ILanguage = new Language({
    name: 'English',
    codeISOV1: 'en',
    codeISOV2: 'eng',
  });
  private localStorageLanguage!: ILanguage;

  constructor() {

    this._variables.subscribe(variables => {
      this.$variables = variables || [];
    });

    this._languages.subscribe(languages => {

      this.$languages = languages || [];
      // if (!languages) return;
      try {
        this.localStorageLanguage = <ILanguage>JSON.parse(localStorage.getItem('system_lang') || '');

        if (!this.localStorageLanguage) {
          this.localStorageLanguage = this.defaultLanguage;
        }
  
        localStorage.setItem('system_lang', JSON.stringify(this.localStorageLanguage));
        this.setSelectedLanguage(this.localStorageLanguage);
      } catch (error) {
        console.log(error);
        localStorage.removeItem('system_lang');
        this.setSelectedLanguage(this.defaultLanguage);
      }

    });

    this._selectedLanguage.subscribe((selectedLanguage: Language) => {
      this.$selectedLanguage = selectedLanguage;
      if (!selectedLanguage) return;
      localStorage.setItem('system_lang', JSON.stringify(selectedLanguage));
    });
  }

  setLanguages(languages: any) {
    this._languages.next(languages);
  }

  setVariables(variables: any) {
    this._variables.next(variables);
  }

  setSelectedLanguage(language: ILanguage) {
    this._selectedLanguage.next(language);
  }


  // =============
  private getLanguageByCodeISO(codeISO: string) {
    return this.$languages.filter((language: ILanguage) => !!codeISO && (language.codeISOV1 === codeISO || language.codeISOV2 === codeISO))[0];
  }

  public getTranslation(uiComponentName: string, uiComponentVariableName: string, languageCodeISO?: string): string {
    let matchesComponent = this.$variables.filter(item => item.uiComponentName === uiComponentName);
    let matchesVariableName = matchesComponent.filter(item => item.uiComponentVariableName === uiComponentVariableName)[0];
    if (!matchesVariableName) {
      if (uiComponentName !== '@GLOBAL@') {
        return this.getTranslation('@GLOBAL@', uiComponentVariableName);
      }
      return uiComponentVariableName;
    }
    
    let _lang = languageCodeISO || this.$selectedLanguage.codeISOV1 || this.$selectedLanguage.codeISOV2;
    for (let translation of matchesVariableName.translations) {
      if (translation.languageCodeISO === _lang) {
        return translation.uiComponentVariableValue;
      }
    }
    if (uiComponentName !== '@GLOBAL@') {
      return this.getTranslation('@GLOBAL@', uiComponentVariableName);
    }
    return uiComponentVariableName;
  }

  public getLocalizationStringValue(localizationString: any) {
    return (localizationString && 
           (localizationString[this.$selectedLanguage.codeISOV1] || localizationString[this.$selectedLanguage.codeISOV2])) || ''
  }

  public getLocalStorageLanguage() {
    return this.localStorageLanguage;
  }
}


