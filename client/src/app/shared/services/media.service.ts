import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { mediaConstants } from "../constants/media-files";
import { MediaImageResolutions } from "../enums";
import { IMedia } from "../interfaces/media";

@Injectable({providedIn: 'root'})
export class MediaService {
  getProductMediaFileUrlByMediaImageResolutions(media: IMedia, mediaImageResolution: MediaImageResolutions): string {
    let mediaConstraint = mediaConstants.product;
    let url = this.generateUrl(media, mediaImageResolution, `${environment.serverUrl}/api/v1/file/image/`, mediaConstraint);
    return url;
  }

  getShopMediaFileUrlByMediaImageResolutions(media: IMedia, mediaImageResolution: MediaImageResolutions): string {
    let mediaConstraint = mediaConstants.shop;
    let url = this.generateUrl(media, mediaImageResolution, `${environment.serverUrl}/api/v1/file/image/`, mediaConstraint);
    return url;
  }

  private generateUrl(media: IMedia, mediaImageResolution: MediaImageResolutions, _url: string, mediaConstraint: any): string{
    let url;
    switch (mediaImageResolution) {
      case MediaImageResolutions.HIGH_RESOLUTION_IMAGE: 
        if (media?.highResolutionImage && typeof media?.highResolutionImage) {
          if (typeof media?.highResolutionImage === 'object' && media?.highResolutionImage?._id) {
            url = `${_url}${media?.highResolutionImage._id}`
          } else {
            url = `${_url}${media?.highResolutionImage}`
          }
        } else {
          url = mediaConstraint.highResolutionImage
        }
        break;
      case MediaImageResolutions.MEDIUM_RESLUTION_IMAGE:
        if (media?.mediumResolutionImage && typeof media?.mediumResolutionImage) {
          if (typeof media?.mediumResolutionImage === 'object' && media?.mediumResolutionImage?._id) {
            url = `${_url}${media?.mediumResolutionImage._id}`
          } else {
            url = `${_url}${media?.mediumResolutionImage}`
          }
        } else {
          url = mediaConstraint.mediumResolutionImage
        }
        break;
      case MediaImageResolutions.LOW_RESLUTION_IMAGE: 
        if (media?.lowResolutionImage && typeof media?.lowResolutionImage) {
          if (typeof media?.lowResolutionImage === 'object' && media?.lowResolutionImage?._id) {
            url = `${_url}${media?.lowResolutionImage._id}`
          } else {
            url = `${_url}${media?.lowResolutionImage}`
          }
        } else {
          url = mediaConstraint.lowResolutionImage
        }
        break;
      case MediaImageResolutions.ORIGINAL_IMAGE:
        if (media?.originalImage && typeof media?.originalImage) {
          if (typeof media?.originalImage === 'object' && media?.originalImage?._id) {
            url = `${_url}${media?.originalImage._id}`
          } else {
            url = `${_url}${media?.originalImage}`
          }
        } else {
          url = mediaConstraint.originalImage
        }
        break;
      default: url = mediaConstraint.lowResolutionImage;
    }
    return url;
  }
}

