import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({providedIn: 'root'})
export class SpinnerService {
    private _spinningSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private minWaitMS: number = 1000;
    private enableDate!: Date | null;
    private disableTimeout: any;
    private spinningbjectQTY: number = 0;

    public get spinningObservable(): Observable<boolean> {
        return this._spinningSubject.asObservable();
    }

    public get isSpinnerEnabled() : boolean {
        return this._spinningSubject.value;
    }

    public start() {
        this.enableDate = new Date();
        if (!!this.disableTimeout) {
            clearTimeout(this.disableTimeout);
        }
        this.spinningbjectQTY++;
        this._spinningSubject.next(true);
    }

    public stop() {
        if (!this.enableDate) return;

        this.spinningbjectQTY--;
        if (this.spinningbjectQTY > 0) return;

        let diff = (new Date()).getTime() - this.enableDate.getTime();

        if (diff >= this.minWaitMS)  {
            this._spinningSubject.next(false);
            this.enableDate = null;
        }
        this.disableTimeout = setTimeout(() => {
            this._spinningSubject.next(false);
            this.enableDate = null;
        }, this.minWaitMS - diff);
    }

    constructor() {
    }
}