import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpStatusCode, HttpContextToken, HttpHeaders, HttpContext } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError as obsThrowError, throwError } from 'rxjs';
import { map, switchMap, tap, filter, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { catchError, mergeMap, } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserAPIService } from '../http/user-api.service';
import { CurrentUserService } from '../services/current-user.service';

export const BYPASS_TOKEN_INTERCEPTION = new HttpContextToken(() => false);
@Injectable({providedIn: 'root'})
export class  TokenInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private currentUserService: CurrentUserService, private userAPIService: UserAPIService, private router: Router) {
   }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headersObj: {[key: string]: string} = {
      'Cache-Control':  'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
      'Pragma': 'no-cache',
      'Expires': '0'
    };
    
    if (req.context.get(BYPASS_TOKEN_INTERCEPTION) === true) {
      let interceptedRequest = this.interceptAccessAndRefreshTokens(req, headersObj);
      return next.handle(interceptedRequest);
    }
    let interceptedRequest = this.interceptAccessToken(req, headersObj);

    return next.handle(interceptedRequest).pipe(
      catchError((err: HttpErrorResponse) => {
        // Refresh Token Expired or does not exist
        if (err.status === 487) {
          this.currentUserService.logOutUser();
          this.router.navigate(['/']);          
        }
        // Access Token Expired
        if (err.status === HttpStatusCode.Gone) {
          if (this.refreshTokenInProgress) {
            return this.refreshTokenSubject.pipe(
              filter((result) => !result),
              take(1),
              switchMap(() => next.handle(this.interceptAccessToken(req, headersObj)))
            );
          } else {
            this.setRefreshTokenProgress(true)
            return this.userAPIService.refreshTokens().pipe(
              catchError((error: any) => {
                this.currentUserService.logOutUser();
                this.router.navigate(['/']);
                return throwError(() => error);
              }),
              map(({accessToken, refreshToken}) => {
                this.currentUserService.setAuthParams({accessToken, refreshToken});
                return {accessToken, refreshToken};
              }),
              switchMap(() => {
                return next.handle(this.interceptAccessToken(req, headersObj));
              }),
              map((data: any) => {
                this.setRefreshTokenProgress(false);
                return data;
              }),
              catchError((error) => {
                this.setRefreshTokenProgress(false);
                return throwError(() => error);
              })
            )
          }
        }
        // ============================================================================
        return throwError(() => err);
      })
    );
  }
  // ========================================================================================================================================

  private setRefreshTokenProgress(prgress: boolean) {
    this.refreshTokenInProgress = prgress;
    this.refreshTokenSubject.next(this.refreshTokenInProgress);
  }

  private interceptAccessAndRefreshTokens(originalRequest: HttpRequest<any>, baseHeaderObject: any): HttpRequest<any> {
    let tokens: {[key: string]: string} = this.currentUserService.getAuthParamsWithRefreshToken();
    return this.interceptHeaders(originalRequest, baseHeaderObject, tokens);
  }

  private interceptAccessToken(originalRequest: HttpRequest<any>, baseHeaderObject: any): HttpRequest<any> {
    let tokens: {[key: string]: string} = this.currentUserService.getAuthParams();
    return this.interceptHeaders(originalRequest, baseHeaderObject, tokens);  
  }

  private interceptHeaders(originalRequest: HttpRequest<any>, baseHeaderObject: any, tokens: {[key: string]: string}) {
    let headers = this.alterHeaders(originalRequest.headers, Object.assign({}, baseHeaderObject, tokens));
    let interceptedRequest: any = originalRequest.clone({
      headers
    });
    return interceptedRequest;
  }

  private alterHeaders(headers: HttpHeaders, keyValuePair: {[key: string]: string}) {
    let _headers = headers;
    for (let key of Object.keys(keyValuePair)) {
      _headers = _headers.set(key, keyValuePair[key]);
    }
    return _headers;
  }
}