import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// modules

// Components

const declatations: any[] = [
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

    ],
    exports: [
      ...declatations,
    ],
    declarations: [
      ...declatations
    ]
})
export class ComponentsModule {}
