import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CategoryAPIService } from "./http/category.api.service";
import { TranslateService } from "./services/translate.service";
import { LocalizationPipe } from "./pipes/localization.pipe";
import { ComponentsModule } from "./components/components.module";
import { TokenInterceptor } from "./interceptors/token-interceptor.service";
import { CurrentUserService } from "./services/current-user.service";
import { UserAPIService } from "./http/user-api.service";
import { ProductAPIService } from "./http/product.api.service";
import { FingerprintService } from "./services/fingerprint.service";
import { CartAPIService } from "./http/cart-api.service";
import { FileAPIService } from "./http/file-api.service";



const declarationComponents: any[] = [
  // pipes
  LocalizationPipe
]

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        ComponentsModule
    ],
    exports: [
      ...declarationComponents,
      ComponentsModule
    ],
    declarations: declarationComponents,
    providers: [
      // API
      CategoryAPIService,
      UserAPIService,
      ProductAPIService,
      CartAPIService,
      FileAPIService,
      // Services
      TranslateService,
    ],
  entryComponents: [
  ]
})
export class SharedModule {
  constructor() {}

  public static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        CurrentUserService,
        FingerprintService,
      ],
    };
  }
}
