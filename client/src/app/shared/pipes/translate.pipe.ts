import { Pipe, PipeTransform } from '@angular/core';

import { TranslateService } from '../services/translate.service';

@Pipe({ 
  name: 'translate', pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(
    private translateService: TranslateService
  ) {}

  transform(value: any, component: string = '@GLOBAL@'): any {
    return this.translateService.getTranslation(component, value);
  }
}
