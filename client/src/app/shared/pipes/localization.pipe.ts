import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '../services/translate.service';
import { ILocalizationString } from '../interfaces/localization-string';

@Pipe({ 
  name: 'localize', pure: false
})
export class LocalizationPipe implements PipeTransform {

  constructor(
    private translateService: TranslateService,
    private domSanitizer: DomSanitizer
  ) {}

  transform(value: ILocalizationString, sanitize: boolean = false): any {
    let result = (value? this.translateService.getLocalizationStringValue(value) : '');
    return !sanitize?  result: this.domSanitizer.bypassSecurityTrustHtml(result);
  }
}
