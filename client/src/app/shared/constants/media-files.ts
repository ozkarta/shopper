export const mediaConstants: any = {
  shop: {
    highResolutionImage: '/assets/images/shop/high-resolution.png',
    mediumResolutionImage: '/assets/images/shop/medium-resolution.jpg',
    lowResolutionImage: '/assets/images/shop/low-resolution.jpg',
  },
  product: {
    highResolutionImage: '/assets/images/product/high-resolution.jpg',
    mediumResolutionImage: '/assets/images/product/medium-resolution.jpg',
    lowResolutionImage: '/assets/images/product/low-resolution.jpg',
  },
}