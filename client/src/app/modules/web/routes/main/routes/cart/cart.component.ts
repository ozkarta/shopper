import { Component, OnDestroy, OnInit } from '@angular/core';
import { result } from 'lodash';
import { catchError, concatMap, debounceTime, distinctUntilChanged, map, of, Subject, Subscription, throwError } from 'rxjs';
import { mediaConstants } from 'src/app/shared/constants/media-files';
import { Currencies, MediaImageResolutions } from 'src/app/shared/enums';
import { CartAPIService } from 'src/app/shared/http/cart-api.service';
import { ICart } from 'src/app/shared/interfaces/cart';
import { ICartItem } from 'src/app/shared/interfaces/cart-item';
import { IMedia } from 'src/app/shared/interfaces/media';
import { IProduct } from 'src/app/shared/interfaces/product';
import { IUser } from 'src/app/shared/interfaces/user';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';
import { MediaService } from 'src/app/shared/services/media.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';

@Component({
  selector: 'app-web-cart-component',
  templateUrl: 'cart.component.html',
  styleUrls: ['cart.component.css']
})

export class WebCartComponent implements OnInit, OnDestroy {
  public cart!: ICart | null;
  private subscription: Subscription = new Subscription();
  private currentUser: IUser | null = this.currentUserService.user;

  get userCurrency(): Currencies {
    return this.currentUser?.currency || Currencies.GEL;
  }

  get isCartEmpty() {
    return !(this.cart?.cartItems && this.cart?.cartItems?.length > 0);
  }
  
  constructor(
    private currentUserService: CurrentUserService,
    private spinnerService: SpinnerService,
    private mediaService: MediaService,
    private cartAPIService: CartAPIService,
  ) { }

  ngOnInit() {
    this.subscription.add(this.subscribeCurrentUser());
    this.subscription.add(this.subscribeCartData());
    this.loadCartData().subscribe();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  subscribeCurrentUser(): Subscription {
    return this.currentUserService.currentUserObservable.subscribe((user: IUser) => this.currentUser = user);
  }

  subscribeCartData(): Subscription {
    return this.currentUserService.cartObservable.subscribe((cart: ICart | null) => this.cart = cart);
  }

  loadCartData() {
    this.spinnerService.start();
    return this.currentUserService.loadCartData().pipe(
      map((cartData: ICart) => {
        this.cart = cartData;
        this.spinnerService.stop();
        return cartData;
      }),
      catchError((error: any) => {
        console.log(error);
        this.spinnerService.stop();
        return throwError(() => error);
      })
    );
  }

  removeCartItem(cartItem: ICartItem) {
    this.spinnerService.start();
    this.cartAPIService.removeCartItem(cartItem._id).pipe(
      concatMap((result: any) => this.loadCartData()),
      map((result: any) => {
        this.spinnerService.stop();
        return result;
      }),
      catchError((error: any) => {
        console.log(error);
        this.spinnerService.stop();
        return throwError(() => error);
      })
    ).subscribe();
  }

  cartItemChangeHandler(cartItem: ICartItem) {
    // console.log(cartItem);
  }

  cartItemQuantityUpdatehandler(cartItem: ICartItem) {
    let cartItemBeforeUpdate: ICartItem | undefined = this.cart?.cartItems.filter((__cartItem: ICartItem) => __cartItem._id === cartItem._id)[0];
    if (!cartItemBeforeUpdate) return;
    let qtyToApply = cartItem.qty - cartItemBeforeUpdate.qty;
    if (qtyToApply === 0) return; // there is no qty to apply
    if (cartItem.qty === 0) return this.removeCartItem(cartItem);
    
    // ==========================================================
    this.spinnerService.start();
    this.cartAPIService.addProductToCart(cartItem.product._id, qtyToApply).pipe(
      concatMap(() => {
        return this,this.currentUserService.loadCartData();
      }),
      map((result: ICart) => {
        this.spinnerService.stop();
        return result;
      }),
      catchError((error: any) => {
        console.log(error);
        this.spinnerService.stop();
        return of(null);
      })
    ).subscribe();
  }

  trackCartItemsByFN(index: number, cartItem: ICartItem) {
    return cartItem._id;
  }
}