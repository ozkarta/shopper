import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { catchError, map, Observable, of } from "rxjs";
import { CategoryAPIService } from "src/app/shared/http/category.api.service";
import { ProductAPIService } from "src/app/shared/http/product.api.service";
import { SpinnerService } from "src/app/shared/services/spinner.service";

@Injectable({ providedIn: 'root' })
export class CategoryResolver implements Resolve<any> {
  constructor(
    private categoryAPIService: CategoryAPIService,
    private spinnerService: SpinnerService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    let categoryId: string | undefined;
    if (!!(<any>route.queryParams).categoryId) {
      categoryId = (<any>route.queryParams).categoryId;
    }
    this.spinnerService.start();
    return this.categoryAPIService.getCategories(categoryId, 1).pipe(
      map((result: any) => {
        return result || [];
      }),
      catchError((error: any) => {
        console.log(error);
        return of(null)
      }),
      map((result: any) => {
        this.spinnerService.stop();
        return result;
      })
    );
  }
}

@Injectable({ providedIn: 'root' })
export class AscendantCategoriesResolver implements Resolve<any> {
  constructor(
    private categoryAPIService: CategoryAPIService,
    private spinnerService: SpinnerService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    let categoryId: string;
    if (!!(<any>route.queryParams).categoryId) {
      categoryId = (<any>route.queryParams).categoryId;
    } else {
      return of([]);
    }
    this.spinnerService.start();
    return this.categoryAPIService.getAscendantCategories(categoryId).pipe(
      map((result: any) => {
        return result || [];
      }),
      catchError((error: any) => {
        console.log(error);
        return of(null)
      }),
      map((result: any) => {
        this.spinnerService.stop();
        return result;
      })
    );
  }
}

/*
@Injectable({ providedIn: 'root' })
export class ProductsResolver implements Resolve<any> {
  constructor(
    private categoryAPIService: ProductAPIService,
    private spinnerService: SpinnerService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    let categoryId: string | undefined;
    if (!!(<any>route.queryParams).categoryId) {
      categoryId = (<any>route.queryParams).categoryId;
    }
    this.spinnerService.start();
    return this.categoryAPIService.getProductById(categoryId, 1).pipe(
      map((result: any) => {
        return result || [];
      }),
      catchError((error: any) => {
        console.log(error);
        return of(null)
      }),
      map((result: any) => {
        this.spinnerService.stop();
        return result;
      })
    );
  }
}

function removeUndefinedKeys(data: any) {
  Object.keys(data).forEach((key: string) => {
    if (data[key] === undefined) {
      delete data[key];
    }
  })
}
*/