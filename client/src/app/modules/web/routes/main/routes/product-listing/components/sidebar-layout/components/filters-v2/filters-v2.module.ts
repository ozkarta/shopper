import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// ==============================================================================
import { ProductListingSidebarLayoutFiltersV2Component } from "./filters-v2.component";
import { WebSharedModule } from "src/app/modules/web/shared/shared.module";


// ============================================
const declarations: any[] = [
    ProductListingSidebarLayoutFiltersV2Component
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        WebSharedModule,
    ],
    providers: [
    ],
    declarations: [
        ...declarations
    ],
    exports: [
        ...declarations
    ]
})
export class SidebarLayoutFiltersV2Module {

}
