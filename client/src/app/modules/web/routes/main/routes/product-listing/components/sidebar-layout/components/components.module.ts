import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SidebarLayoutFiltersModule } from "./filters/filters.module";
import { ProductListingSidebarLayoutProductsComponent } from "./products/products.component";
import { SidebarLayoutFiltersV2Module } from "./filters-v2/filters-v2.module";


// ============================================

const declarations: any = [
    ProductListingSidebarLayoutProductsComponent,
];

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

        SidebarLayoutFiltersModule,
        SidebarLayoutFiltersV2Module,
    ],
    providers: [
    ],
    declarations: [
        ...declarations,
    ],
    exports: [
        declarations,
        SidebarLayoutFiltersModule,
        SidebarLayoutFiltersV2Module,
    ]
})
export class ComponentsModule {

}
