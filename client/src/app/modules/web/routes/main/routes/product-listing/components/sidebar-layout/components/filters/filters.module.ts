import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// ==============================================================================
import { ProductListingSidebarLayoutFiltersComponent } from "./filters.component";


// ============================================
const declarations: any[] = [
    ProductListingSidebarLayoutFiltersComponent
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        ...declarations
    ],
    exports: [
        ...declarations
    ]
})
export class SidebarLayoutFiltersModule {

}
