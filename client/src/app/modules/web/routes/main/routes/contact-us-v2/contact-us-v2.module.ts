import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { ContactUsV2Component } from './contact-us-v2.component';

const routes: Routes = [
    {
        path: "",
        component: ContactUsV2Component,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        ContactUsV2Component,
    ]
})
export class ContactUsV2Module {

}
