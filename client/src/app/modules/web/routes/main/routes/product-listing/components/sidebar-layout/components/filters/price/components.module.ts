import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


// ============================================

const declarations: any = [
    
];

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        ...declarations,
    ],
    exports: [
        declarations
    ]
})
export class ComponentsModule {

}
