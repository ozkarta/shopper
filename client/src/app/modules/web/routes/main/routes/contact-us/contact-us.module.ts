import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { ContactUsComponent } from './contact-us.component';

const routes: Routes = [
    {
        path: "",
        component: ContactUsComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        ContactUsComponent,
    ]
})
export class ContactUsModule {

}
