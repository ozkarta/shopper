import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { WebCartComponent } from './cart.component';
import { WebSharedModule } from "src/app/modules/web/shared/shared.module";
import { ComponentsModule } from "./components/components.module";

const routes: Routes = [
    {
        path: "",
        component: WebCartComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,

        WebSharedModule,
        ComponentsModule,
    ],
    providers: [
    ],
    declarations: [
        WebCartComponent,
    ]
})
export class CartModule {

}
