import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
    {
        path: "",
        component: NotFoundComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        NotFoundComponent,
    ]
})
export class NotFoundModule {

}
