import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import {
  BehaviorSubject,
  catchError,
  distinctUntilChanged,
  map,
  Observable,
  of,
  share,
  Subscription,
  switchMap,
  tap,
  throwError,
  zip
} from 'rxjs';
import { Utils } from 'src/app/shared/classes/utils';
import { ProductAPIService } from 'src/app/shared/http/product.api.service';
import { IBreadCrumbDataItem } from 'src/app/shared/interfaces/breadcrumb';
import { ICategory } from 'src/app/shared/interfaces/category';
import { IProduct } from 'src/app/shared/interfaces/product';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import * as _ from 'lodash';
import { CategoryAPIService } from 'src/app/shared/http/category.api.service';

@Component({
  selector: 'app-web-product-listing-component',
  templateUrl: 'product-listing.component.html',
  styleUrls: ['product-listing.component.css'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {

  public products: IProduct[] = [];
  public productsNumTotal!: number;

  public categories: ICategory[] = []


  // ====================================================
  public breadCrumbData: IBreadCrumbDataItem[] = [];
  private ascendantCategoriesSubject: BehaviorSubject<ICategory[]> = new BehaviorSubject<ICategory[]>([]);
  private subscription: Subscription = new Subscription();


  public layoutOptions: ILayoutOption[] = [
    {
      title: 'List',
      class: 'list',
      features: false,
      xlinkHref: 'images/svg/sprite.svg#layout-list-16x16'
    },
    {
      title: 'Grid',
      class: 'grid-3-sidebar',
      features: false,
      xlinkHref: 'images/svg/sprite.svg#layout-grid-16x16'
    },
    {
      title: 'Grid With Features',
      class: 'grid-3-sidebar',
      features: true,
      xlinkHref: 'images/svg/sprite.svg#layout-grid-with-details-16x16'
    },
  ];
  public activeLayout: ILayoutOption = this.layoutOptions[0];

  public sortOptions: any[] = [
    {
      title: {
        en: 'Create Date(DESC)',
        ge: 'თარიღი(კლებადობით)',
      },
      field: 'createdAt',
      order: '-',
      id: Utils.makeRandomString(5,5,5,5),
    },
    {
      title: {
        en: 'Create Date(ASC)',
        ge: 'თარიღი(ზრდადობით)',
      },
      field: 'createdAt',
      order: '+',
      id: Utils.makeRandomString(5,5,5,5),
    },
    {
      title: {
        en: 'Price(ASC)',
        ge: 'ფასი(ზრდადობით)',
      },
      field: 'price',
      order: '+',
      id: Utils.makeRandomString(5,5,5,5),
    },
    {
      title: {
        en: 'Price(DESC)',
        ge: 'ფასი(კლებადობით)',
      },
      field: 'price',
      order: '-',
      id: Utils.makeRandomString(5,5,5,5),
    },
  ];
  public activeSortOption: any = this.sortOptions[0];

  public productsQuery: any = {
    keyword: '',
    page: 1,
    limit: 15,
    sortBy: this.activeSortOption.field + this.activeSortOption.order,
  };
  private loadDataSubject: BehaviorSubject<any> = new BehaviorSubject<any>(this.productsQuery);

  constructor(
    private spinnerService: SpinnerService,
    private productAPIService: ProductAPIService,
    private categoryAPIService: CategoryAPIService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.subscription.add(this.subscribeQueryParamsbservable());
    this.subscription.add(this.subscribeLoadDataSubjectObservable());
    this.subscription.add(this.subscribeLoadProductsObservable());

    this.subscription.add(this.subscribeAscendantCategories())
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.spinnerService.isSpinnerEnabled) {
      this.spinnerService.stop();
    }
  }

  // ==================================SUBSCRIPTIONS============================================
  subscribeQueryParamsbservable(): Subscription {
    return this.queryParamsbservable().subscribe();
  }

  subscribeLoadDataSubjectObservable(): Subscription {
    return this.loadDataSubjectObservable().subscribe();
  }

  subscribeLoadProductsObservable(): Subscription {
    return this.loadProductsObservable().subscribe();
  }

  subscribeAscendantCategories(): Subscription {
    return this.ascendantCategoriesSubject.subscribe((categories: ICategory[]) => {
      this.breadCrumbData = [{
        title: {
          en: 'Products',
          ge: 'პრდუქტები',
        },
        path: '/products',
      }];
      for (let category of categories) {
        let breadCrumbDataItem: IBreadCrumbDataItem = {
          title: category.categoryName,
          path: `/products`,
          queryParams: {categoryId: category._id},
          queryParamsHandling: 'merge',
        }
        this.breadCrumbData.push(breadCrumbDataItem);
      }
    })
  }

  subscribeRouteData(): Subscription {
    return this.route.data.subscribe((data: any) => {
      this.ascendantCategoriesSubject.next(data.ascendantCategories || []);
    });
  }
  // ===========================================================================================
  // ==================================OBSERVABLES==============================================
  queryParamsbservable(): Observable<any> {
    return this.route.queryParams.pipe(
      map((queryParams: any) => {
        this.productsQuery = this.parseQueryParamsAndMergeWithCurrent(this.productsQuery, queryParams);
        this.loadDataSubject.next(this.productsQuery);
        return queryParams;
      }),
    )
  }

  loadDataSubjectObservable(): Observable<any> {
    return this.loadDataSubject.pipe(
      map((query: any) => {
        this.router.navigate([`/products`], { 
          queryParams:  query,
          queryParamsHandling: 'merge', 
        });
        return query;
      }),
      share()
    );
  }

  loadProductsObservable(): Observable<any> {
    return this.loadDataSubjectObservable().pipe(
      distinctUntilChanged(_.isEqual),
      tap(() => this.spinnerService.start()),
      switchMap((query: any) => {
        return zip([
          this.loadProductDataDatabservable(query),
          this.loadCategoryDataObservable(query),
          this.loadAscendantCategoryDataObservable(query),
        ]);
      }),
      map((result: any) => {
        this.spinnerService.stop();
        return result;
      }),
      catchError((error: any) => {
        this.spinnerService.stop()
        console.log(error);
        return throwError(() => error);
      }),
    )
  }
  // ===========================================================================================

  // ============================================================================================
  private parseQueryParamsAndMergeWithCurrent(_currentQuery: any, queryParams: any) {
    let currentQuery = Object.assign({}, _currentQuery);
    let {keyword, page, limit, sortBy, categoryId} = queryParams || {};
    currentQuery = this.removeUndefinedKeys(Object.assign({}, currentQuery, {keyword: keyword || _currentQuery.keyword, page: page || _currentQuery.page, limit: limit | _currentQuery.limit, sortBy, categoryId}));
    return currentQuery;
  }

  private loadProductDataDatabservable(query: any): Observable<any> {
    return this.productAPIService.getProductsByQuery(query).pipe(
      map(({ items, numTotal }) => {
        this.products = items || [];
        this.productsNumTotal = numTotal || 0;
        return items;
      }),
      catchError((error) => {
        throw error;
      })
    );
  }

  private loadCategoryDataObservable(query: any): Observable<any> {
    return this.categoryAPIService.getCategories(query.categoryId, 1).pipe(
      map((result: any) => {
        this.categories = result || [];
        return result || [];
      }),
      catchError((error: any) => {
        console.log(error);
        return of(null)
      }),
      map((result: any) => {
        this.spinnerService.stop();
        return result;
      })
    );
  }

  private loadAscendantCategoryDataObservable(query: any): Observable<any> {
    if (!query.categoryId) {
      this.ascendantCategoriesSubject.next([]);
      return of(null);
    }
    return this.categoryAPIService.getAscendantCategories(query.categoryId).pipe(
      map((result: any) => {
        this.ascendantCategoriesSubject.next(result || []);
        return result || [];
      }),
      catchError((error: any) => {
        console.log(error);
        return of(null)
      }),
    );
  }


  private removeUndefinedKeys(data: any) {
    Object.keys(data).forEach((key: string) => {
      if (data[key] === undefined) {
        delete data[key];
      }
    });
    return data;
  }

  pageChangeHandler(pageNum: number) {
    this.productsQuery.page = pageNum;
    this.loadDataSubject.next(this.productsQuery);
  }

  setActiveLayout(layout: ILayoutOption) {
    this.activeLayout = layout;
  }

  productLimitChange(event: Event) {
    let value = (<any>(event.target)).value;
    this.productsQuery.limit = parseInt(value);
    this.productsQuery.page = 1;
    this.loadDataSubject.next(this.productsQuery);
  }

  sortOptionChangeHandler(event: Event) {
    let value = (<any>(event.target)).value;
    this.productsQuery.page = 1;
    this.productsQuery.sortBy = value;
    this.loadDataSubject.next(this.productsQuery);
  }
}

interface ILayoutOption {
  title: string;
  class: string;
  features: boolean;
  xlinkHref: string;
}