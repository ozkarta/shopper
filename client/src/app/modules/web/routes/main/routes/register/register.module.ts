import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { RegisterComponent } from './register.component';

const routes: Routes = [
    {
        path: "",
        component: RegisterComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        RegisterComponent,
    ]
})
export class RegisterModule {

}
