import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// ============================================
import { SidebarLayoutModule } from "./sidebar-layout/sidebar-layout.module";
import { ProductItemComponent } from "./product-item/product-item.component";
import { WebSharedModule } from "src/app/modules/web/shared/shared.module";

const declarations: any = [
    ProductItemComponent
];

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        SidebarLayoutModule,
        WebSharedModule
    ],
    providers: [
    ],
    declarations: [
        ...declarations,
    ],
    exports: [
        declarations,
        SidebarLayoutModule
    ]
})
export class ComponentsModule {

}
