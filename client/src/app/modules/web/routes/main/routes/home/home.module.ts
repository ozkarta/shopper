import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { HomeComponent } from './home.component';
import { ComponentsModule } from './components/components.module';
const routes: Routes = [
    {
        path: "",
        component: HomeComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        ComponentsModule,
    ],
    providers: [
    ],
    declarations: [
        HomeComponent,
    ]
})
export class HomeModule {

}
