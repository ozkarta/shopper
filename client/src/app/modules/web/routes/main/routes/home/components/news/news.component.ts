import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
declare var $: any;

@Component({
    selector: 'app-web-home-news-component',
    templateUrl: 'news.component.html',
    styleUrls: ['news.component.css']
})
export class HomeNewsComponent implements OnInit {
    private owl: any;
    private readonly options = {
        margin: 30,
        nav: false,
        dots: false,
        loop: true
    };
    private readonly layoutOptions: {[key: string]: any} = {
        'grid-nl': {

            responsive: {
                992: {items: 3},
                768: {items: 2},
                0: {items: 1}
            }
        },
        'list-sm': {
            responsive: {
                992: {items: 2},
                0: {items: 1}
            }
        }
    };
    @ViewChild('carouselContainer', {static: true}) carouselContainer!: ElementRef;
    @ViewChild('carousel', {static: true}) carousel!: ElementRef;

    constructor() {

    }

    ngOnInit() {
        if (this.carouselContainer) {
            const layout = $(this.carouselContainer.nativeElement).data('layout');
            this.owl = $(this.carousel.nativeElement, this.carouselContainer.nativeElement);
            this.owl.owlCarousel($.extend({}, this.options, this.layoutOptions[layout]));
        }
    }

    leftArrowClickHandler() {
        this.owl.trigger('prev.owl.carousel', [500]);
    }

    rightArrowClickHandler() {
        this.owl.trigger('next.owl.carousel', [500]);
    }
}