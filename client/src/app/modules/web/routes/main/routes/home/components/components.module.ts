import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeSlideShowFullComponent } from "./slideshow-full/slideshow-full.component";
import { HomeSlideshowComponent } from "./slideshow/slideshow.component";
import { HomeFeaturesComponent } from "./features/features.component";
import { HomeFeaturedProductsComponent } from "./featured-products/featured-products.component";
import { HomeBannerComponent } from "./banner/banner.component";
import { HomeBestSellersComponent } from "./best-sellers/best-sellers.component";
import { HomePopularCategoriesComponent } from "./popular-categories/popular-categories.component";
import { HomeNewArrivalProductsComponent } from "./new-arrivals/new-arrivals.component";
import { HomeNewsComponent } from "./news/news.component";
import { HomeBrandsComponent } from "./brands/brands.component";
import { HomeProductsComponent } from "./products/products.component";

// ============================================

const declarations: any = [
    HomeSlideshowComponent,
    HomeSlideShowFullComponent,
    HomeFeaturesComponent,
    HomeFeaturedProductsComponent,
    HomeBannerComponent,
    HomeBestSellersComponent,
    HomePopularCategoriesComponent,
    HomeNewArrivalProductsComponent,
    HomeNewsComponent,
    HomeBrandsComponent,
    HomeProductsComponent,
];

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        ...declarations,
    ],
    exports: [
        declarations
    ]
})
export class ComponentsModule {

}
