import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
declare var $: any;

@Component({
    selector: 'app-web-home-slideshow-full-component',
    templateUrl: 'slideshow-full.component.html',
    styleUrls: ['slideshow-full.component.css']
})

export class HomeSlideShowFullComponent implements OnInit{
    @ViewChild('slideshowCarouselElement', {static: true}) slideshowCarouselElement!: ElementRef;

    constructor() {

    }

    ngOnInit(): void {
        if (this.slideshowCarouselElement) {
            $(this.slideshowCarouselElement.nativeElement).owlCarousel({
                items: 1,
                nav: false,
                dots: true,
                loop: true
            });
        }    
    }
}