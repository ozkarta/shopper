import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { ActivatedRoute, Router, ResolveEnd } from '@angular/router';
import { ICategory } from "src/app/shared/interfaces/category";
import { Subscription, map } from 'rxjs';

@Component({
    selector: 'app-web-product-listing-sidebar-layout-filters-v2-component',
    templateUrl: 'filters-v2.component.html',
    styleUrls: ['filters-v2.component.css']
})
export class ProductListingSidebarLayoutFiltersV2Component implements OnInit, OnDestroy {
  @Input('categories') categories: ICategory[] = [];
  private subscription: Subscription = new Subscription();
  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {}
  toggleElement(element: HTMLElement) {
    element.setAttribute('is-open', `${!(element.getAttribute('is-open') === 'true')}`)
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}