import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { IProduct } from "src/app/shared/interfaces/product";

@Component({
    selector: 'app-web-product-details-info-component',
    templateUrl: 'product-info.component.html',
    styleUrls: ['product-info.component.css']
})
export class ProductDetailsInfoComponent implements OnInit, OnDestroy{
    @Input('product') product!: IProduct;
  
    ngOnInit() {
      
    }
  
    ngOnDestroy(): void {
        
    }
  }