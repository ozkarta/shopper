import { Component, Input, Output, OnInit, OnDestroy, EventEmitter } from "@angular/core";
import { IProduct } from "src/app/shared/interfaces/product";

@Component({
    selector: 'app-web-product-details-sidebar-component',
    templateUrl: 'product-sidebar.component.html',
    styleUrls: ['product-sidebar.component.css']
})
export class ProductDetailsSidebarComponent implements OnInit, OnDestroy{
  @Input('product') product!: IProduct;
  @Output('addToCartEventHandler') addToCartEventEmitter: EventEmitter<number> = new EventEmitter();

  ngOnInit() {
    
  }

  ngOnDestroy(): void {
      
  }

  addProductToCart(qty: number = 1) {
    this.addToCartEventEmitter.emit(qty);
  }
  
}