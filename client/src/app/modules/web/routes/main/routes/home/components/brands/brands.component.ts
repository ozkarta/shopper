import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
declare var $: any;

@Component({
    selector: 'app-web-home-brands-component',
    templateUrl: 'brands.component.html',
    styleUrls: ['brands.component.css']
})

export class HomeBrandsComponent implements OnInit {
    @ViewChild('carouselElement', {static: true}) carouselElement!: ElementRef;
    ngOnInit(): void {
        if (this.carouselElement) {
            $(this.carouselElement.nativeElement).owlCarousel({
                nav: false,
                dots: false,
                loop: true,
                responsive: {
                    1200: {items: 6},
                    992: {items: 5},
                    768: {items: 4},
                    576: {items: 3},
                    0: {items: 2}
                }
            });
        }
    }
}