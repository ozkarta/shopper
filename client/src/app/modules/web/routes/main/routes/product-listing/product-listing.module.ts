import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { ProductDetailsComponent } from './product-listing.component';
import { ComponentsModule as GlobalComponentsModule } from 'src/app/modules/web/routes/main/components/components.module';
import { ComponentsModule } from './components/components.module';
import { AscendantCategoriesResolver, CategoryResolver } from "./product-listing.resolver";
import { WebSharedModule } from "src/app/modules/web/shared/shared.module";

const routes: Routes = [
    {
        path: "",
        component: ProductDetailsComponent,
        resolve: {
            // categories: CategoryResolver,
            // ascendantCategories: AscendantCategoriesResolver
        },
        // runGuardsAndResolvers: 'paramsOrQueryParamsChange'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,

        WebSharedModule,

        GlobalComponentsModule,
        ComponentsModule,
        
    ],
    providers: [
    ],
    declarations: [
        ProductDetailsComponent,
    ],
    exports: [
        
    ]
})
export class ProductListingModule {

}
