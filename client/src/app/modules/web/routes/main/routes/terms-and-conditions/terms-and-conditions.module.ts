import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { TermsAndConditionsComponent } from './terms-and-conditions.component';

const routes: Routes = [
    {
        path: "",
        component: TermsAndConditionsComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        TermsAndConditionsComponent,
    ]
})
export class TermsAndConditionsModule {

}
