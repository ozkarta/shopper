import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { ProductDetailsComponent } from './product-details.component';
import { ComponentsModule } from "./components/components.module";
import { ComponentsModule as LocalComponentsModule } from './components/components.module';
import { WebSharedModule } from "src/app/modules/web/shared/shared.module";

const routes: Routes = [
    {
        path: "",
        component: ProductDetailsComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        ComponentsModule,
        LocalComponentsModule,

        WebSharedModule,
    ],
    providers: [
    ],
    declarations: [
        ProductDetailsComponent,
    ]
})
export class ProductDetailsModule {

}
