import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
declare var $: any;

@Component({
    selector: 'app-web-product-detais-related-products-component',
    templateUrl: 'related-products.comopnent.html',
    styleUrls: ['related-products.comopnent.css']
})

export class RelatedProductsComponent implements OnInit {
    @ViewChild('carouselContainer', {static: true}) carouselContainer!: ElementRef;
    @ViewChild('carousel', {static: true}) carousel!: ElementRef;


    private owl: any;
    private readonly options = {
        items: 4,
        margin: 14,
        nav: false,
        dots: false,
        loop: true,
        stagePadding: 1
    };
    private readonly layoutOptions: {[key: string]: any} = {
        'grid-4': {
            responsive: {
                1200: {items: 4, margin: 14},
                992:  {items: 4, margin: 10},
                768:  {items: 3, margin: 10},
                576:  {items: 2, margin: 10},
                475:  {items: 2, margin: 10},
                0:    {items: 1}
            }
        },
        'grid-4-sm': {
            responsive: {
                1200: {items: 4, margin: 14},
                992:  {items: 3, margin: 10},
                768:  {items: 3, margin: 10},
                576:  {items: 2, margin: 10},
                475:  {items: 2, margin: 10},
                0:    {items: 1}
            }
        },
        'grid-5': {
            responsive: {
                1200: {items: 5, margin: 12},
                992:  {items: 4, margin: 10},
                768:  {items: 3, margin: 10},
                576:  {items: 2, margin: 10},
                475:  {items: 2, margin: 10},
                0:    {items: 1}
            }
        },
        'horizontal': {
            items: 3,
            responsive: {
                1200: {items: 3, margin: 14},
                992:  {items: 3, margin: 10},
                768:  {items: 2, margin: 10},
                576:  {items: 1},
                475:  {items: 1},
                0:    {items: 1}
            }
        },
    }

    ngOnInit(): void {
        if (this.carouselContainer) {
            const layout = $(this.carouselContainer.nativeElement).data('layout');
            // this.owl = $(this.carousel.nativeElement, this.carouselContainer.nativeElement);
            // this.owl.owlCarousel($.extend({}, this.options, this.layoutOptions[layout]));

            this.owl = $(this.carousel.nativeElement, this.carouselContainer.nativeElement);

            this.owl.owlCarousel($.extend({}, this.options, this.layoutOptions[layout]));
        }
    }

    // =============================================================================

    leftArrowClickHandler() {
        this.owl.trigger('prev.owl.carousel', [500]);
    }

    rightArrowClickHandler() {
        this.owl.trigger('next.owl.carousel', [500]);
    }
}