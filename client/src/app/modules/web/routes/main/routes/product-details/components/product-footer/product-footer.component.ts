import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { IProduct } from "src/app/shared/interfaces/product";

@Component({
    selector: 'app-web-product-details-footer',
    templateUrl: 'product-footer.component.html',
    styleUrls: ['product-footer.component.css']
})
export class ProductDetailsFooteer implements OnInit, OnDestroy{
    @Input('product') product!: IProduct;
  
    ngOnInit() {
      
    }
  
    ngOnDestroy(): void {
        
    }
  }