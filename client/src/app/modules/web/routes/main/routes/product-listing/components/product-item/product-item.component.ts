import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { catchError, concatMap, map, of, Subscription } from "rxjs";
import { Currencies, MediaImageResolutions, NotificationTypes } from "src/app/shared/enums";
import { CartAPIService } from "src/app/shared/http/cart-api.service";
import { ICart } from "src/app/shared/interfaces/cart";
import { IMedia } from "src/app/shared/interfaces/media";
import { IProduct } from "src/app/shared/interfaces/product";
import { IUser } from "src/app/shared/interfaces/user";
import { CurrentUserService } from "src/app/shared/services/current-user.service";
import { MediaService } from "src/app/shared/services/media.service";
import { NotificationService } from "src/app/shared/services/notification.service";
import { SpinnerService } from "src/app/shared/services/spinner.service";

@Component({
  selector: 'app-web-product-listing-product-item-component',
  templateUrl: 'product-item.component.html',
  styleUrls: ['product-item.component.css']
})

export class ProductItemComponent implements OnInit, OnDestroy{
  @Input('product') product!: IProduct;
  private currentUser: IUser | null = this.currentUserService.user;

  get userCurrency(): Currencies {
    return this.currentUser?.currency || Currencies.GEL;
  }

  private subscrition: Subscription = new Subscription();
  public get isProductAddedInCart() {
    return false;
  }

  constructor(
    private cartAPIService: CartAPIService,
    private spinnerService: SpinnerService,
    private currentUserService: CurrentUserService,
    private notificationService: NotificationService,
    private mediaService: MediaService
  ) {}

  ngOnInit(): void {
    this.subscrition.add(this.subscribeCurrentUser())
  }

  ngOnDestroy(): void {
    if (this.subscrition) {
      this.subscrition.unsubscribe
    }
  }

  // ==========================================================
  subscribeCurrentUser():Subscription {
    return this.currentUserService.currentUserObservable.subscribe((user: IUser) => this.currentUser = user);
  }
  // ==========================================================

  getProductPrimaryImage() {
    let primaryImage: IMedia = ((this.product?.media || []).filter((item: any) => item.isPrimary)[0])?.item || null;
    return this.mediaService.getProductMediaFileUrlByMediaImageResolutions(primaryImage, MediaImageResolutions.HIGH_RESOLUTION_IMAGE);
  }

  addProductToCart(product: IProduct, qty: number) {
    if (!product || qty <=0 || this.isProductAddedInCart) return;
    this.spinnerService.start();
    this.cartAPIService.addProductToCart(product._id, qty).pipe(
      concatMap(() => {
        return this.currentUserService.loadCartData();
      }),
      map((result: ICart) => {
        this.spinnerService.stop();
        this.showSuccessAfterAddedToCart();
        return result;
      }),
      catchError((error: any) => {
        console.log(error);
        this.spinnerService.stop();
        return of(null);
      })
    ).subscribe();
  }

  showSuccessAfterAddedToCart() {
    this.notificationService.show({
      message: 'Product was successfully added to cart',
      type: NotificationTypes.SUCCESS,
      autoClose: true,
      autoCloseAfter: 5 * 1000,
      showCloseBadge: true
    })
  }

  getProductPriceByCurrentUserCurrency(currency: Currencies): number {
    return this.product.price;
  }
}