import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WebSharedModule } from 'src/app/modules/web/shared/shared.module';
import { CartListItemComponent } from './cart-list-item/cart-list-item.component';

var declarations = [
  CartListItemComponent
]

@NgModule({
  imports: [
    CommonModule,
    WebSharedModule,
    RouterModule.forChild([])
  ],
  exports: [
    ...declarations
  ],
  declarations: [
    ...declarations
  ],
  providers: [],
})
export class ComponentsModule { }
