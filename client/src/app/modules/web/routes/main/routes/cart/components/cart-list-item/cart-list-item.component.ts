import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { debounceTime, distinctUntilChanged, map, Subject, Subscription } from 'rxjs';
import { MediaImageResolutions } from 'src/app/shared/enums';
import { ICartItem } from 'src/app/shared/interfaces/cart-item';
import { IMedia } from 'src/app/shared/interfaces/media';
import { IProduct } from 'src/app/shared/interfaces/product';
import { MediaService } from 'src/app/shared/services/media.service';
import * as _ from 'lodash';

@Component({
  selector: '[cart-list-item-component]',
  templateUrl: 'cart-list-item.component.html'
})

export class CartListItemComponent implements OnInit, OnDestroy {
  private _cartItem!: ICartItem;
  public get cartItem() {
    return this._cartItem;
  }
  @Input() set cartItem(val: ICartItem) {
    if (val) {
      this._cartItem = _.cloneDeep(val);
    }
  };
  private cartItemQTYChangeSubject: Subject<number> = new Subject();
  private subscription: Subscription = new Subscription();
  @Output('removeCartItemHandler') removeCartItemEmitter: EventEmitter<ICartItem> = new EventEmitter();
  @Output('cartItemChangeHandler') cartItemChangeEmitter: EventEmitter<ICartItem> = new EventEmitter();
  @Output('cartItemQuantityUpdatehandler') cartItemQuantityUpdateEmitter: EventEmitter<ICartItem> = new EventEmitter();


  constructor(
    private mediaService: MediaService,
  ) { }

  ngOnInit() {
    this.subscription.add(this.subscribeCartItemQTYChangeSubject());
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


  getProductPrimaryImage(product: IProduct) {
    let primaryImage: IMedia = ((product?.media || []).filter((item: any) => item.isPrimary)[0])?.item || null;
    return this.mediaService.getProductMediaFileUrlByMediaImageResolutions(primaryImage, MediaImageResolutions.LOW_RESLUTION_IMAGE);
  }

  addQTY(qty: number) {
    this.cartItemQTYChangeSubject.next(qty);
  }

  removeCartItem() {
    this.removeCartItemEmitter.emit(this.cartItem);
  }

  subscribeCartItemQTYChangeSubject(): Subscription {
    return this.cartItemQTYChangeSubject.asObservable().pipe(
      map((newQTY: number) => {
        this.cartItem.qty += newQTY;
        if (this.cartItem.qty < 0) {
          this.cartItem.qty = 0;
        }
        this.cartItemChangeEmitter.emit(this.cartItem);
        return this.cartItem.qty;
      }),
      debounceTime(500),
      distinctUntilChanged(),
      map((a: any) => {
        this.cartItemQuantityUpdateEmitter.emit(this.cartItem);
      })
    ).subscribe();
  }
}