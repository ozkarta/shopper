import { Component, Input } from '@angular/core';
import { ICategory } from 'src/app/shared/interfaces/category';

@Component({
  selector: 'app-web-product-listing-sidebar-layout-component',
  templateUrl: 'sidebar-layout.component.html',
  styleUrls: ['sidebar-layout.component.css'],
})
export class ProductListingSidebarLayoutComponent {
    @Input('categories') categories!: ICategory[];
}
