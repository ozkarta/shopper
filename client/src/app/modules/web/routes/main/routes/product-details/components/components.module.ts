import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RelatedProductsComponent } from "./related-products/related-products.comopnent";
import { ProductGalleryComponent } from "./product-gallery/product-gallery.component";
import { ProductDetailsInfoComponent } from "./product-info/product-info.component";
import { ProductDetailsSidebarComponent } from "./product-sidebar/product-sidebar.component";
import { ProductDetailsFooteer } from "./product-footer/product-footer.component";
import { WebSharedModule } from "src/app/modules/web/shared/shared.module";
// modules

// Components

const declatations: any[] = [
    RelatedProductsComponent,
    ProductGalleryComponent,
    ProductDetailsInfoComponent,
    ProductDetailsSidebarComponent,
    ProductDetailsFooteer
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

        WebSharedModule

    ],
    exports: [
      ...declatations,
      
    ],
    declarations: [
      ...declatations
    ]
})
export class ComponentsModule {}
