import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// ==============================================================================
import { ProductListingSidebarLayoutComponent } from "./sidebar-layout.component";
import { ComponentsModule } from "./components/components.module";

// ============================================
const declarations: any[] = [
    ProductListingSidebarLayoutComponent,
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

        ComponentsModule,

    ],
    providers: [
    ],
    declarations: [
        ...declarations
    ],
    exports: [
        ...declarations
    ]
})
export class SidebarLayoutModule {

}
