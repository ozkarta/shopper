import { Component } from "@angular/core";

@Component({
  selector: 'app-web-not-found-component',
  templateUrl: 'not-found.component.html',
  styleUrls: ['not-found.component.css']
})

export class NotFoundComponent {
  constructor() {
  }
}
