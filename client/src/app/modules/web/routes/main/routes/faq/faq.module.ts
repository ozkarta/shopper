import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { FAQComponent } from './faq.component';

const routes: Routes = [
    {
        path: "",
        component: FAQComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        FAQComponent,
    ]
})
export class FAQModule {

}
