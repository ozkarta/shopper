import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { AboutUsComponent } from './about-us.component';

const routes: Routes = [
    {
        path: "",
        component: AboutUsComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule
    ],
    providers: [
    ],
    declarations: [
        AboutUsComponent,
    ]
})
export class AboutUsModule {

}
