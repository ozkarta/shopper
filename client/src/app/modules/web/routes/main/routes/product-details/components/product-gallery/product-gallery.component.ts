import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  OnDestroy,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { MediaImageResolutions } from 'src/app/shared/enums';
import { IMedia } from 'src/app/shared/interfaces/media';
import { IProduct } from 'src/app/shared/interfaces/product';
import { MediaService } from 'src/app/shared/services/media.service';
declare var $: any;

@Component({
  selector: 'app-web-product-details-gallery-component',
  templateUrl: 'product-gallery.component.html',
  styleUrls: ['product-gallery.component.css'],
})
export class ProductGalleryComponent implements OnInit, OnDestroy, OnChanges {
  @Input('product') product!: IProduct;
  @Input('media') media!: {item: IMedia, isPrimary: boolean}[];

  public readonly options: any = {
    dots: false,
    margin: 10,
  };
  public readonly layoutOptions: { [key: string]: any } = {
    standard: {
      responsive: {
        1200: { items: 5 },
        992: { items: 4 },
        768: { items: 3 },
        480: { items: 5 },
        380: { items: 4 },
        0: { items: 3 },
      },
    },
    sidebar: {
      responsive: {
        768: { items: 4 },
        480: { items: 5 },
        380: { items: 4 },
        0: { items: 3 },
      },
    },
    columnar: {
      responsive: {
        768: { items: 4 },
        480: { items: 5 },
        380: { items: 4 },
        0: { items: 3 },
      },
    },
    quickview: {
      responsive: {
        1200: { items: 5 },
        768: { items: 4 },
        480: { items: 5 },
        380: { items: 4 },
        0: { items: 3 },
      },
    },
  };
  @ViewChild('gallery', { static: true }) gallery!: ElementRef;

  constructor(
    private mediaService: MediaService
  ) {}

  ngOnInit() {
    // this.initProductGallery(this.gallery.nativeElement);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['media']) {
        setTimeout(() => {
            this.initProductGallery(this.gallery.nativeElement);
        }, 0);
    }
  }

  ngOnDestroy(): void {}

  initProductGallery(element: any, layout?: 'standard' | 'sidebar' | 'columnar' | 'quickview') {
    layout = layout !== undefined ? layout : 'standard';

    const gallery = $(element);
    const image = gallery.find('.product-gallery__featured .owl-carousel');
    const carousel = gallery.find('.product-gallery__carousel .owl-carousel');

    image.owlCarousel({ items: 1, dots: false })
        .on('changed.owl.carousel', (el: any) => {
            let current = el.item.index;

            carousel
                .find('.product-gallery__carousel-item')
                .removeClass('product-gallery__carousel-item--active')
                .eq(current)
                .addClass('product-gallery__carousel-item--active');
            const onscreen = carousel.find('.owl-item.active').length - 1;
            const start = carousel.find('.owl-item.active').first().index();
            const end = carousel.find('.owl-item.active').last().index();

            if (current > end) {
            carousel.data('owl.carousel').to(current, 100, true);
            }
            if (current < start) {
            carousel.data('owl.carousel').to(current - onscreen, 100, true);
            }
        });

    carousel
      .on('initialized.owl.carousel', function () {
        carousel
          .find('.product-gallery__carousel-item')
          .eq(0)
          .addClass('product-gallery__carousel-item--active');
      })
      .owlCarousel($.extend({}, this.options, this.layoutOptions[layout]));

    carousel.on('click', '.owl-item', function (this: any, e: any) {
      e.preventDefault();

      image.data('owl.carousel').to($(this).index(), 300, true);
    });
  }

  getProductPrimaryImage() {
    let primaryImage: IMedia = ((this.product?.media || []).filter((item: any) => item.isPrimary)[0])?.item || null;
    return this.mediaService.getProductMediaFileUrlByMediaImageResolutions(primaryImage, MediaImageResolutions.HIGH_RESOLUTION_IMAGE)
  }

  getProductRegularImages() {
    let regularImages: IMedia[] = ((this.product?.media || []).filter((item: any) => !item.isPrimary)).map(i => i.item);
    return regularImages.map((image: IMedia) => this.mediaService.getProductMediaFileUrlByMediaImageResolutions(image, MediaImageResolutions.HIGH_RESOLUTION_IMAGE))
  }

  getProductImages() {
    return [this.getProductPrimaryImage(), ... this.getProductRegularImages()];
  }
}
