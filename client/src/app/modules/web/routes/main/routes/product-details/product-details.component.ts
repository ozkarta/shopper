import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SpinnerService } from 'src/app/shared/services/spinner.service';
import { Subscription, map, Observable, catchError, of, mergeMap, concatMap } from 'rxjs';
import { IProduct } from 'src/app/shared/interfaces/product';
import { ProductAPIService } from 'src/app/shared/http/product.api.service';
import { CartAPIService } from 'src/app/shared/http/cart-api.service';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { NotificationTypes } from 'src/app/shared/enums';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';
import { ICart } from 'src/app/shared/interfaces/cart';

@Component({
  selector: 'app-web-product-details-component',
  templateUrl: 'product-details.component.html',
  styleUrls: ['product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private productId!: string;
  public product!: IProduct | undefined;
  constructor(
    private ruoter: Router, 
    private route: ActivatedRoute,
    private spinnerService: SpinnerService,
    private productAPIService: ProductAPIService,
    private cartAPIService: CartAPIService,
    private notificationService: NotificationService,
    private currentUserService: CurrentUserService,
  ) {}

  ngOnInit(): void {
    this.subscription.add(this.getRuoterParamsObservable().pipe(
      map((params: any) => {
        // it's very important to rerender gallery and initialize owl again...
        this.product = undefined;
        return params.productId;
      }),
      mergeMap((productId: string) => {
        return this.loadProductObservable(productId);
      })
    ).subscribe());
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  // ========================================================================
  private getRuoterParamsObservable(): Observable<any> {
    return this.route.params.pipe((params: any) => {
        this.productId = params.productId;
        return params;
    })
  }

  private loadProductObservable(productId: string): Observable<IProduct | null> {
    this.spinnerService.start();
    return this.productAPIService.getProductById(productId).pipe(
      map((product: IProduct) => {
        this.product = product;
        this.spinnerService.stop();
        return product;
      }),
      catchError((error: any) => {
        console.log(error);
        this.spinnerService.stop();
        return of(null);
      })
    )
  }

  addProductToCart(qty: number) {
    if (!this.product || qty <= 0) return;
    this.spinnerService.start();
    this.cartAPIService.addProductToCart(this.product._id, qty).pipe(
      concatMap(() => {
        return this.currentUserService.loadCartData();
      }),
      map((result: ICart) => {
        this.spinnerService.stop();
        this.showSuccessAfterAddedToCart();
        return result;
      }),
      catchError((error: any) => {
        console.log(error);
        this.spinnerService.stop();
        return of(null);
      })
    ).subscribe();
  }

  showSuccessAfterAddedToCart() {
    this.notificationService.show({
      message: 'Product was successfully added to cart',
      type: NotificationTypes.SUCCESS,
      autoClose: true,
      autoCloseAfter: 5 * 1000,
      showCloseBadge: true
    })
  }
}
