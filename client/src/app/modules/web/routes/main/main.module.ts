import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { ComponentsModule } from './components/components.module';
import { WebSharedModule } from "../../shared/shared.module";
import { WebMainComponent } from "./main.component";
import { HeaderShopCategoriesResolver } from "./main.resolver";
import { CanActivateWebMain } from "./main.guard";

const webModuleRoutes: Routes = [
    {
        path: "",
        component: WebMainComponent,
        canActivate: [
            CanActivateWebMain
        ],
        resolve: {
          headerShopCategories: HeaderShopCategoriesResolver,
        },
        children: [
          { path: "", loadChildren: () => import("./routes/home/home.module").then(mod => mod.HomeModule ) },
          { path: "login", loadChildren: () => import("./routes/login/login.module").then(mod => mod.LoginModule ) },
          { path: "register", loadChildren: () => import("./routes/register/register.module").then(mod => mod.RegisterModule ) },
          { path: 'cart', loadChildren: () => import('./routes/cart/cart.module').then(m => m.CartModule) },
          { path: "about-us", loadChildren: () => import("./routes/about-us/about-us.module").then(mod => mod.AboutUsModule ) },
          { path: "contact-us", loadChildren: () => import("./routes/contact-us/contact-us.module").then(mod => mod.ContactUsModule ) },
          { path: "contact-us-v2", loadChildren: () => import("./routes/contact-us-v2/contact-us-v2.module").then(mod => mod.ContactUsV2Module ) },
          { path: "faq", loadChildren: () => import("./routes/faq/faq.module").then(mod => mod.FAQModule ) },
          { path: "terms-and-conditions", loadChildren: () => import("./routes/terms-and-conditions/terms-and-conditions.module").then(mod => mod.TermsAndConditionsModule ) },
          { path: "page-not-found", loadChildren: () => import("./routes/not-found/not-found.module").then(mod => mod.NotFoundModule ) },

          { path: "products", loadChildren: () => import("./routes/product-listing/product-listing.module").then(mod => mod.ProductListingModule) },
          { path: "product/:productId", loadChildren: () => import("./routes/product-details/product-details.module").then(mod => mod.ProductDetailsModule) },
          
          { path: "**", redirectTo: '/page-not-found' },
          
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(webModuleRoutes),
        CommonModule,
        FormsModule,

        ComponentsModule,
        WebSharedModule,
    ],
    exports: [
        WebSharedModule,
    ],
    providers: [
        CanActivateWebMain
    ],
    declarations: [
        WebMainComponent,
    ]
})
export class WebMainModule {

}
