import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { catchError, concatMap, map, Observable, of, throwError } from "rxjs";
import { CurrentUserService } from "src/app/shared/services/current-user.service";
import { FingerprintService } from "src/app/shared/services/fingerprint.service";
import { SpinnerService } from "src/app/shared/services/spinner.service";

@Injectable()
export class CanActivateWebMain implements CanActivate {
  constructor(
    private fingerprintService: FingerprintService,
    private currentUserService: CurrentUserService,
    private router: Router,
    private spinnerService: SpinnerService,
  ) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    this.spinnerService.start();
    return new Promise((resolve: Function, reject: Function) => {
      setTimeout(() => {
        this.fingerprintService.getFingerPrint().then((clientFingerprint: string) => {
          this.currentUserService.loadUser().pipe(
            map((result: any) => {
              return true;
            }),
            catchError((error: any) => {
              return this.currentUserService.logInPreAuthUser(Object.assign({}, this.currentUserService.initialUser, {clientFingerprint}));
            }),
            concatMap((result: any) => {
              return this.currentUserService.loadCartData().pipe(map(() => result)).pipe(map(_r => true));
            }),
            catchError((error: any) => {
              this.currentUserService.logOutUser();
              return of(false);
            })
          ).subscribe(
            (result) => {
              if (!result) {
                this.router.navigate(['../error']);
              }
              this.spinnerService.stop();
              return resolve(result)
            }
          )
        })
      }, 0) // for test only, test late API response behaviour
    })
  }
}