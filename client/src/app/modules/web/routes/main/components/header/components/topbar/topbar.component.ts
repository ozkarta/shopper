import { Component } from "@angular/core";
import { CurrentUserService } from "src/app/shared/services/current-user.service";
import { AppLanguages, Currencies } from '../../../../../../../../shared/enums/index';

@Component({
  selector: 'app-web-topbar-component',
  templateUrl: 'topbar.component.html',
  styleUrls: ['topbar.component.css']
})

export class HeaderTopbarComponent {
  public readonly topBarDropdowns: any[] = [
    {
      title: 'My Account',
      titleWithValue: false,
      titleValue: () => undefined,
      withIcons: false,
      listItems: [
        {
          title: 'Login',
          route: '/login',
        },
        {
          title: 'Register',
          route: '/register',
        },
        {
          title: 'Orders',
          route: '/orders',
        },
        {
          title: 'Addresses',
          route: '/addresses',
        }
      ]
    },
    {
      title: 'Currency',
      titleWithValue: true,
      titleValue: () => this.currency,
      withIcons: false,
      listItems: [
        {
          title: 'ლარი',
          value: Currencies.GEL,
          callbackFunction: (val: Currencies) => {
            this.setCurrency(val);
          }
        },
        {
          title: 'US Dollar',
          value: Currencies.USD,
          callbackFunction: (val: Currencies) => {
            this.setCurrency(val);
          }
        },
      ]
    },
    {
      title: 'Language',
      titleWithValue: true,
      titleValue: () => this.language,
      withIcons: true,
      listItems: [
        {
          title: 'ქართული',
          value: AppLanguages.GEORGIAN,
          iconURL: 'images/others/language-2.png',
          callbackFunction: (val: AppLanguages) => {
            this.setLanguage(val);
          }
        },
        {
          title: 'English',
          value: AppLanguages.ENGLISH,
          iconURL: 'images/others/language-1.png',
          callbackFunction: (val: AppLanguages) => {
            this.setLanguage(val);
          }
        },
      ]
    }
  ]
  public get currency() {
    return this.currentUserService.user?.currency;
  }
  public get language() {
    return this.currentUserService.user?.language;
  }

  constructor(private currentUserService: CurrentUserService) {

  }
  // ============================================================

  setLanguage(lang: AppLanguages) {
    this.currentUserService.setLanguage(lang);
  }

  setCurrency(currency: Currencies) {
    this.currentUserService.setCurrency(currency);
  }
}



// <li><a href="account.html">Login</a></li>
//       <li><a href="account.html">Register</a></li>
//       <li><a href="#">Orders</a></li>
//       <li><a href="#">Addresses</a></li>
