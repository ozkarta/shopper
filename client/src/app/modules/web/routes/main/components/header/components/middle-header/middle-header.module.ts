import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// modules

// Components
import { MiddleHeaderComponent } from "./middle-header.component";

const declatations: any[] = [
  MiddleHeaderComponent
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

    ],
    exports: [
      ...declatations,
    ],
    declarations: [
      ...declatations
    ]
})
export class MiddleHeaderModule {}
