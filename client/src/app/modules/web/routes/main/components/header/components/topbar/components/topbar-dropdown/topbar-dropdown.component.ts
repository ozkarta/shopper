import { Component, ElementRef, HostListener, Input, ViewChild } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: 'app-web-header-topbar-dropdown-component',
  templateUrl: 'topbar-dropdown.component.html',
  styleUrls: ['topbar-dropdown.component.css']
})

export class TopbarDropdownComponent {
  @Input('title') title?: string;
  @Input('titleWithValue') titleWithValue?: boolean;
  @Input('titleValue') titleValue?: any;
  @Input('listItems') listItems: {route: string, title: string, value: any, iconURL?: string, callbackFunction?: Function}[] = [];
  @Input('withIcons') withIcons?: boolean;

  public isOpen: boolean = false;
  @ViewChild('elementRef') eRef?: ElementRef;

  @HostListener('document:click', ['$event'])
  clickout(event: any) {
    if(this.eRef?.nativeElement.contains(event.target)) {
      this.isOpen = !this.isOpen;
    } else {
      this.isOpen = false;
    }
  }

  constructor(
    private router: Router
  ) {}

  itemClickHandler(listItem: any) {
    // this.isOpen = false;
    if (listItem?.callbackFunction && typeof listItem.callbackFunction === 'function') {
      return listItem.callbackFunction(listItem.value);
    } else {
      this.router.navigate([listItem.route])
    }
  }
}
