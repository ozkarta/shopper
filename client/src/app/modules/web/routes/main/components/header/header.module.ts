import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { HeaderComponent } from './header.component';
import { ComponentsModule } from "./components/components.module";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        
        ComponentsModule
    ],
    providers: [
    ],
    declarations: [
        HeaderComponent,
    ],
    exports: [
        HeaderComponent,
        ComponentsModule
    ]
})
export class HeaderModule {

}
