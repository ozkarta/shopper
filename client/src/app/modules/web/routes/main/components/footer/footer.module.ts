import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from "./footer.component";

// ============================================

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        
    ],
    providers: [
    ],
    declarations: [
        FooterComponent,
    ],
    exports: [
        FooterComponent
    ]
})
export class FooterModule {

}
