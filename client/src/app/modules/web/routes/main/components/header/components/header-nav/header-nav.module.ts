import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderNavComponent } from "./header-nav.component";
import { ComponentsModule } from "./components/components.module";

// ============================================

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

        ComponentsModule
    ],
    providers: [
    ],
    declarations: [
        HeaderNavComponent,
    ],
    exports: [
        HeaderNavComponent
    ]
})
export class HeaderNavModule {

}
