import { Component, OnDestroy, OnInit } from "@angular/core";
import { map, catchError } from "rxjs/operators";
import { Observable } from "rxjs";
import { CategoryAPIService } from "src/app/shared/http/category.api.service";
import { ICategory } from "src/app/shared/interfaces/category";
import { SpinnerService } from "src/app/shared/services/spinner.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-web-header-component',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.css']
})

export class HeaderComponent implements OnInit, OnDestroy {
  public headerCategories: ICategory[] = [];
  constructor(
    private ruote: ActivatedRoute
  ) {}


  ngOnInit(): void {
    this.headerCategories = (<any>this.ruote.snapshot.data).headerShopCategories || [];
  }

  ngOnDestroy(): void {
    
  }

}
