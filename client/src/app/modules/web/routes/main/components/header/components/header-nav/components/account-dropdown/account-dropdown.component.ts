import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, Subscription } from 'rxjs';
import { IUser } from 'src/app/shared/interfaces/user';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';

@Component({
  selector: 'app-web-header-nav-account-dropdown-component',
  templateUrl: 'account-dropdown.component.html',
  styleUrls: ['account-dropdown.component.css']
})

export class AccountDropdownComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private _currentUser!: IUser | null;
  public get currentUser(): IUser | null {
    return this._currentUser;
  }
  public get isUserSignedIn(): boolean {
    return this.currentUserService.isSigned();
  }
  constructor(
    private currentUserService: CurrentUserService
  ) { }

  ngOnInit() { 
    this._currentUser = this.currentUserService.user;
    this.subscribeCurrentUser();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  subscribeCurrentUser() {
    this.subscription.add( this.currentUserService.currentUserObservable.pipe(
      map((currentUser: IUser) => {
        this._currentUser = currentUser;
        return currentUser;
      })
    ).subscribe());
  }
}