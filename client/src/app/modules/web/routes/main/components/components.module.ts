import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// modules
import { HeaderModule } from "./header/header.module";
import { FooterModule } from "./footer/footer.module";

// Components

const declatations: any[] = [
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

        HeaderModule,
        FooterModule,

    ],
    exports: [
      ...declatations,
      FooterModule,
      HeaderModule,
    ],
    declarations: [
      ...declatations
    ]
})
export class ComponentsModule {}
