import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// modules

import { MiddleHeaderModule } from "./middle-header/middle-header.module";
import { TopBarModule } from "./topbar/topbar.module";
import { HeaderNavModule } from "./header-nav/header-nav.module";

// Components


const declatations: any[] = [
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

        TopBarModule,
        HeaderNavModule,

        MiddleHeaderModule,

    ],
    exports: [
      ...declatations,
      MiddleHeaderModule,
      HeaderNavModule,
      TopBarModule
    ],
    declarations: [
      ...declatations
    ]
})
export class ComponentsModule {}
