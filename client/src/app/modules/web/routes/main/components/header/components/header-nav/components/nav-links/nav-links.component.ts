import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
declare var $: any;
@Component({
    selector: 'app-web-header-nav-nav-links-component',
    templateUrl: 'nav-links.component.html',
    styleUrls: ['nav-links.component.html']
})

export class NavLinksComponent  implements OnInit, OnDestroy, AfterViewInit{
    constructor() {}

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {
        
    }

    ngAfterViewInit(): void {
        $('.nav-links').each((ind: number, item: any) => {
            new CNavLinks(item);
        });


        $('.menu').each((ind: number, item: any) => {
            new CMenu($(item));
        });
    }
}

class CNavLinks {
    private element: any;
    private items: any;
    private currentItem: any;
    private DIRECTION: any = null;

    // public onMouseenter!: Function;
    // public onMouseleave!: Function;
    // public onGlobalTouchClick!: Function;
    // public onTouchClick!: Function;

    constructor(element: any) {
        this.element = $(element);
        this.items = this.element.find('.nav-links__item');
        this.currentItem = null;
    
        this.element.data('navLinksInstance', this);
    
        // this.onMouseenter = this.onMouseenter.bind(this);
        // this.onMouseleave = this.onMouseleave.bind(this);
        // this.onGlobalTouchClick = this.onGlobalTouchClick.bind(this);
        // this.onTouchClick = this.onTouchClick.bind(this);
    
        // add event listeners
        this.items.on('mouseenter', (event: any) => {
            this.onMouseenter(event);
        });
        this.items.on('mouseleave', (event: any) => {
            this.onMouseleave();
        });
        this.touchClick(document, this.onGlobalTouchClick);
        this.touchClick(this.items, this.onTouchClick);
    }

    onGlobalTouchClick(event: any) {
        // check that the click was outside the element
        if (this.element.not($(event.target).closest('.nav-links')).length) {
            this.unsetCurrentItem();
        }
    };

    onTouchClick(event: any) {
        if (event.cancelable) {
            const targetItem = $(event.currentTarget);
    
            if (this.currentItem && this.currentItem.is(targetItem)) {
                return;
            }
    
            if (this.hasSubmenu(targetItem)) {
                event.preventDefault();
    
                if (this.currentItem) {
                    this.currentItem.trigger('mouseleave');
                }
    
                targetItem.trigger('mouseenter');
            }
        }
    };

    onMouseenter(event: any) {
        this.setCurrentItem($(event.currentTarget));
    };

    onMouseleave() {
        this.unsetCurrentItem();
    };

    setCurrentItem(item: any) {
        this.currentItem = item;
        this.currentItem.addClass('nav-links__item--hover');
    
        this.openSubmenu(this.currentItem);
    };

    unsetCurrentItem() {
        if (this.currentItem) {
            this.closeSubmenu(this.currentItem);
    
            this.currentItem.removeClass('nav-links__item--hover');
            this.currentItem = null;
        }
    };

    hasSubmenu(item: any) {
        return !!item.children('.nav-links__submenu').length;
    };

    openSubmenu(item: any) {
        const submenu = item.children('.nav-links__submenu');
    
        if (!submenu.length) {
            return;
        }
    
        submenu.addClass('nav-links__submenu--display');
    
        // calculate max height
        const submenuTop = submenu.offset().top - $(window).scrollTop();
        const viewportHeight = window.innerHeight;
        const paddingBottom = 20;
    
        submenu.css('maxHeight', (viewportHeight - submenuTop - paddingBottom) + 'px');
        submenu.addClass('nav-links__submenu--open');
    
        // megamenu position
        if (submenu.hasClass('nav-links__submenu--type--megamenu')) {
            const container = submenu.offsetParent();
            const containerWidth = container.width();
            const megamenuWidth = submenu.width();
    
            if (this.isRTL()) {
                const itemPosition = containerWidth - (item.position().left + item.width());
                const megamenuPosition = Math.round(Math.min(itemPosition, containerWidth - megamenuWidth));
    
                submenu.css('right', megamenuPosition + 'px');
            } else {
                const itemPosition = item.position().left;
                const megamenuPosition = Math.round(Math.min(itemPosition, containerWidth - megamenuWidth));
    
                submenu.css('left', megamenuPosition + 'px');
            }
        }
    };

    closeSubmenu(item: any) {
        const submenu = item.children('.nav-links__submenu');
    
        if (!submenu.length) {
            return;
        }
    
        submenu.removeClass('nav-links__submenu--display');
        submenu.removeClass('nav-links__submenu--open');
        submenu.css('maxHeight', '');
    
        if (submenu && submenu.is('.nav-links__submenu--type--menu')) {
            const submenuInstance = submenu.find('> .menu').data('menuInstance');
    
            if (submenuInstance) {
                submenuInstance.unsetCurrentItem();
            }
        }
    };

    isRTL() {
        return this.direction() === 'rtl';
    }

    direction() {
        if (this.DIRECTION === null) {
            this.DIRECTION = getComputedStyle(document.body).direction;
        }

        return this.DIRECTION;
    }

    touchClick(elements: any, callback: Function) {
        elements = $(elements);

        let touchStartData: any = null;

        const onTouchstart = function(event: any){
            const originalEvent = event.originalEvent;

            if (originalEvent.touches.length !== 1) {
                touchStartData = null;
                return;
            }

            touchStartData = {
                target: originalEvent.currentTarget,
                touch: originalEvent.changedTouches[0],
                timestamp: (new Date).getTime(),
            };
        };
        const onTouchEnd = function(event: any){
            const originalEvent = event.originalEvent;

            if (
                !touchStartData ||
                originalEvent.changedTouches.length !== 1 ||
                originalEvent.changedTouches[0].identity !== touchStartData.touch.identity
            ) {
                return;
            }

            const timestamp = (new Date).getTime();
            const touch = originalEvent.changedTouches[0];
            const distance = Math.abs(
                Math.sqrt(
                    Math.pow(touchStartData.touch.screenX - touch.screenX, 2) +
                    Math.pow(touchStartData.touch.screenY - touch.screenY, 2)
                )
            );

            if (touchStartData.target === originalEvent.currentTarget && timestamp - touchStartData.timestamp < 500 && distance < 10) {
                callback(event);
            }
        };

        elements.on('touchstart', onTouchstart);
        elements.on('touchend', onTouchEnd);

        return function() {
            elements.off('touchstart', onTouchstart);
            elements.off('touchend', onTouchEnd);
        };
    }
}


class CMenu {
    private element: any;
    private container: any;
    private items: any;
    private currentItem: any;
    private DIRECTION: any = null;

    constructor(element: any) {
        this.element = $(element);
        this.container = this.element.find('> .menu__submenus-container');
        this.items = this.element.find('> .menu__list > .menu__item');
        this.currentItem = null;

        this.element.data('menuInstance', this);

        // add event listeners
        this.items.on('mouseenter', (event: any) => this.onMouseenter(event));
        this.element.on('mouseleave', (event: any) => this.onMouseleave());
        this.touchClick(this.items, this.onTouchClick);
    }

    onMouseenter(event: any) {
        const targetItem = $(event.currentTarget);
    
        if (this.currentItem && targetItem.is(this.currentItem)) {
            return;
        }
    
        this.unsetCurrentItem();
        this.setCurrentItem(targetItem);
    };

    onMouseleave () {
        this.unsetCurrentItem();
    };

    onTouchClick(event: any) {
        const targetItem = $(event.currentTarget);
    
        if (this.currentItem && this.currentItem.is(targetItem)) {
            return;
        }
    
        if (this.hasSubmenu(targetItem)) {
            this.preventTouchClick();
    
            this.unsetCurrentItem();
            this.setCurrentItem(targetItem);
        }
    };

    setCurrentItem (item: any) {
        this.currentItem = item;
        this.currentItem.addClass('menu__item--hover');
    
        this.openSubmenu(this.currentItem);
    };

    unsetCurrentItem() {
        if (this.currentItem) {
            this.closeSubmenu(this.currentItem);
    
            this.currentItem.removeClass('menu__item--hover');
            this.currentItem = null;
        }
    };

    getSubmenu(item: any) {
        let submenu = item.find('> .menu__submenu');
    
        if (submenu.length) {
            this.container.append(submenu);
            item.data('submenu', submenu);
        }
    
        return item.data('submenu');
    };

    hasSubmenu (item: any) {
        return !!this.getSubmenu(item);
    };

    openSubmenu(item: any) {
        const submenu = this.getSubmenu(item);
    
        if (!submenu) {
            return;
        }
    
        submenu.addClass('menu__submenu--display');
    
        // calc submenu position
        const menuTop = this.element.offset().top - $(window).scrollTop();
        const itemTop = item.find('> .menu__item-submenu-offset').offset().top - $(window).scrollTop();
        const viewportHeight = window.innerHeight;
        const paddingY = 20;
        const maxHeight = viewportHeight - paddingY * 2;
    
        submenu.css('maxHeight', maxHeight + 'px');
    
        const submenuHeight = submenu.height();
        const position = Math.min(
            Math.max(
                itemTop - menuTop,
                0
            ),
            (viewportHeight - paddingY - submenuHeight) - menuTop
        );
    
        submenu.css('top', position + 'px');
        submenu.addClass('menu__submenu--open');
    
        if (this.isRTL()) {
            const submenuLeft = this.element.offset().left - submenu.width();
    
            if (submenuLeft < 0) {
                submenu.addClass('menu__submenu--reverse');
            }
        } else {
            const submenuRight = this.element.offset().left + this.element.width() + submenu.width();
    
            if (submenuRight > $('body').innerWidth()) {
                submenu.addClass('menu__submenu--reverse');
            }
        }
    };

    closeSubmenu(item: any) {
        const submenu = this.getSubmenu(item);
    
        if (submenu) {
            submenu.removeClass('menu__submenu--display');
            submenu.removeClass('menu__submenu--open');
            submenu.removeClass('menu__submenu--reverse');
    
            const submenuInstance = submenu.find('> .menu').data('menuInstance');
    
            if (submenuInstance) {
                submenuInstance.unsetCurrentItem();
            }
        }
    };

    isRTL() {
        return this.direction() === 'rtl';
    }

    direction() {
        if (this.DIRECTION === null) {
            this.DIRECTION = getComputedStyle(document.body).direction;
        }

        return this.DIRECTION;
    }

    touchClick(elements: any, callback: Function) {
        elements = $(elements);

        let touchStartData: any = null;

        const onTouchstart = function(event: any){
            const originalEvent = event.originalEvent;

            if (originalEvent.touches.length !== 1) {
                touchStartData = null;
                return;
            }

            touchStartData = {
                target: originalEvent.currentTarget,
                touch: originalEvent.changedTouches[0],
                timestamp: (new Date).getTime(),
            };
        };
        const onTouchEnd = function(event: any){
            const originalEvent = event.originalEvent;

            if (
                !touchStartData ||
                originalEvent.changedTouches.length !== 1 ||
                originalEvent.changedTouches[0].identity !== touchStartData.touch.identity
            ) {
                return;
            }

            const timestamp = (new Date).getTime();
            const touch = originalEvent.changedTouches[0];
            const distance = Math.abs(
                Math.sqrt(
                    Math.pow(touchStartData.touch.screenX - touch.screenX, 2) +
                    Math.pow(touchStartData.touch.screenY - touch.screenY, 2)
                )
            );

            if (touchStartData.target === originalEvent.currentTarget && timestamp - touchStartData.timestamp < 500 && distance < 10) {
                callback(event);
            }
        };

        elements.on('touchstart', onTouchstart);
        elements.on('touchend', onTouchEnd);

        return function() {
            elements.off('touchstart', onTouchstart);
            elements.off('touchend', onTouchEnd);
        };
    }

    preventTouchClick() {
        const onClick = function(event: any){
            event.preventDefault();

            document.removeEventListener('click', onClick);
        };
        document.addEventListener('click', onClick);
        setTimeout(function() {
            document.removeEventListener('click', onClick);
        }, 100);
    }
}