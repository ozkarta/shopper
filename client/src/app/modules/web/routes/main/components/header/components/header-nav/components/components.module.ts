import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// modules

// Components
import { IndicatorsComponent } from "./indicators/indicators.component";
import { NavLinksComponent } from "./nav-links/nav-links.component";
import { DepartmentsTogglableComponent } from "./departments-togglable/departments-togglable.component";
import { DepartmentsFixedComponent } from "./departments-fixed/departments-fixed.component";
import { SharedModule } from "src/app/shared/shared.module";
import { CartDropdownComponent } from "./cart-dropdown/cart-dropdown.component";
import { AccountDropdownComponent } from "./account-dropdown/account-dropdown.component";

const declatations: any[] = [
    IndicatorsComponent,
    NavLinksComponent,
    DepartmentsTogglableComponent,
    DepartmentsFixedComponent,
    CartDropdownComponent,
    AccountDropdownComponent
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        SharedModule,
    ],
    exports: [
      ...declatations,
    ],
    declarations: [
      ...declatations
    ]
})
export class ComponentsModule {}
