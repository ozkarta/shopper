import { Component, Input } from "@angular/core";
import { ICategory } from "src/app/shared/interfaces/category";

@Component({
  selector: 'app-web-header-nav-component',
  templateUrl: 'header-nav.component.html',
  styleUrls: ['header-nav.component.css']
})

export class HeaderNavComponent {
  @Input() headerCategories!: ICategory[];
}
