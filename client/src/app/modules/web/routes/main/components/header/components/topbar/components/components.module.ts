import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TopbarDropdownComponent } from "./topbar-dropdown/topbar-dropdown.component";

// Components


const declatations: any[] = [
    TopbarDropdownComponent
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,

    ],
    exports: [
      ...declatations,
    ],
    declarations: [
      ...declatations
    ]
})
export class ComponentsModule {}
