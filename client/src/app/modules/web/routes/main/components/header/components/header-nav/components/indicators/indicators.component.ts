import {
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { catchError, map, of, Subscription } from 'rxjs';
import { ICart } from 'src/app/shared/interfaces/cart';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';

enum DropdownStates {
  OPEN = 'OPEN',
  CLOSED = 'CLOSED',
}

@Component({
  selector: 'app-web-header-nav-indicators',
  templateUrl: 'indicators.component.html',
  styleUrls: ['indicators.component.css'],
})
export class IndicatorsComponent implements OnInit, OnDestroy {
  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event: MouseEvent): void {
    // if (!this.cartLinkElement.nativeElement.contains(event.target)) {
    //   // clicked outside => close dropdown list
    //   if (this.cartElement.nativeElement.contains(event.target)) {
    //     return;
    //   }
    //   this._cartState = DropdownStates.CLOSED;
    //   return;
    // }

    // this.toggleCart();

    if (this.cartLinkElement.nativeElement.contains(event.target)) {
      this.toggleCart();
      if (this.isAccountOpen) {
        this.toggleAccount();
      }
      return;
    }

    if (this.accountLinkElement.nativeElement.contains(event.target)) {
      this.toggleAccount();
      if (this.isCartOpen) {
        this.toggleCart();
      }
      return;
    }

    if (!this.cartElement.nativeElement.contains(event.target) && this.isCartOpen) {
      this.toggleCart();
    }

    if (!this.accountElement.nativeElement.contains(event.target) && this.isAccountOpen) {
      this.toggleAccount();
    }

  }
  @ViewChild('cartElement') cartElement!: ElementRef;
  @ViewChild('cartLinkElement') cartLinkElement!: ElementRef;
  @ViewChild('accountElement') accountElement!: ElementRef;
  @ViewChild('accountLinkElement') accountLinkElement!: ElementRef;

  private subscription: Subscription = new Subscription();
  public cart!: ICart | null;

  private _cartState: DropdownStates = DropdownStates.CLOSED;
  public get isCartOpen(): boolean {
    return this._cartState === DropdownStates.OPEN;
  }

  private _accountState: DropdownStates = DropdownStates.CLOSED;
  public get isAccountOpen(): boolean {
    return this._accountState === DropdownStates.OPEN;
  }

  constructor(
    private currentUserService: CurrentUserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.cart = this.currentUserService.cart;
    this.subscription.add(this.subscribeCartData());
    this.subscription.add(this.subscribeNaviChange());
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  // ===========================================================
  subscribeCartData(): Subscription {
    return this.currentUserService.cartObservable
      .pipe(
        map((cartData: ICart | null) => {
          this.cart = cartData;
          return cartData;
        }),
        catchError((error: any) => {
          console.log(error);
          this.cart = null;
          return of(null);
        })
      )
      .subscribe();
  }

  subscribeNaviChange(): Subscription {
    return this.router.events
        .subscribe((event: any) => {
            if (event instanceof NavigationEnd) {
                this._cartState = DropdownStates.CLOSED;
            }
        });
  }
  // ===========================================================

  public toggleCart() {
    if (this.isCartOpen) {
      this._cartState = DropdownStates.CLOSED;
      return;
    }
    this._cartState = DropdownStates.OPEN;
  }

  public toggleAccount() {
    if (this.isAccountOpen) {
      this._accountState = DropdownStates.CLOSED;
      return;
    }
    this._accountState = DropdownStates.OPEN;
  }
}
