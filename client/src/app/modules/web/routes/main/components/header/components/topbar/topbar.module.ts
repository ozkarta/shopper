import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderTopbarComponent } from "./topbar.component";
import { ComponentsModule } from "./components/components.module";

// ============================================

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,

        ComponentsModule
    ],
    providers: [
    ],
    declarations: [
        HeaderTopbarComponent
    ],
    exports: [
        HeaderTopbarComponent,
        ComponentsModule
    ]
})
export class TopBarModule {

}
