import { Component, Input, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { catchError, concatMap, Subscription, throwError } from 'rxjs';
import { Currencies, MediaImageResolutions, Spinners } from 'src/app/shared/enums';
import { CartAPIService } from 'src/app/shared/http/cart-api.service';
import { ICart } from 'src/app/shared/interfaces/cart';
import { ICartItem } from 'src/app/shared/interfaces/cart-item';
import { IMedia } from 'src/app/shared/interfaces/media';
import { IProduct } from 'src/app/shared/interfaces/product';
import { IUser } from 'src/app/shared/interfaces/user';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';
import { MediaService } from 'src/app/shared/services/media.service';
import { SpinnerService } from 'src/app/shared/services/spinner.service';

@Component({
  selector: 'app-web-header-nav-cart-dropdown-component',
  templateUrl: 'cart-dropdown.component.html',
  styleUrls: ['cart-dropdown.component.css']
})

export class CartDropdownComponent implements OnInit {
  @Input('cart') cart!: ICart | null;
  private subscription: Subscription = new Subscription();
  private currentUser: IUser | null = this.currentUserService.user;

  get userCurrency(): Currencies {
    return this.currentUser?.currency || Currencies.GEL;
  }
  
  constructor(
    private currentUserService: CurrentUserService,
    private mediaService: MediaService,
    private spinnerService: SpinnerService,
    private cartAPIService: CartAPIService,
  ) { }

  ngOnInit() {
    this.subscription.add(this.subscribeCurrentUser());
  }

  subscribeCurrentUser(): Subscription {
    return this.currentUserService.currentUserObservable.subscribe((user: IUser) => this.currentUser = user);
  }

  getProductPrimaryImage(product: IProduct) {
    let primaryImage: IMedia = ((product?.media || []).filter((item: any) => item.isPrimary)[0])?.item || null;
    return this.mediaService.getProductMediaFileUrlByMediaImageResolutions(primaryImage, MediaImageResolutions.LOW_RESLUTION_IMAGE);
  }

  removeCartItem(cartItem: ICartItem) {
    this.spinnerService.start();
    this.cartAPIService.removeCartItem(cartItem._id).pipe(
      concatMap((result: any) => this.currentUserService.loadCartData()),
      map((result: any) => {
        this.spinnerService.stop();
        return result;
      }),
      catchError((error: any) => {
        console.log(error);
        this.spinnerService.stop();
        return throwError(() => error);
      })
    ).subscribe();
  }

  trackCartItemsByFN(index: number, cartItem: ICartItem) {
    return JSON.stringify(cartItem);
  }
}