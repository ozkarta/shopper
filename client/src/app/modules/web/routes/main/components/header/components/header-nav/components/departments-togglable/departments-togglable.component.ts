import { AfterViewInit, Component, ElementRef, HostListener, Input, OnChanges, QueryList, Renderer2, SimpleChanges, ViewChild, ViewChildren } from "@angular/core";
import { ICategory } from "src/app/shared/interfaces/category";
declare var $: any;

enum DepartmentStates {
    OPEN = 'OPEN',
    CLOSED = 'CLOSED'
}

@Component({
    selector: 'app-web-header-nav-departments-togglable',
    templateUrl: 'departments-togglable.component.html',
    styleUrls: ['departments-togglable.component.css']
})

export class DepartmentsTogglableComponent implements OnChanges, AfterViewInit {
    public columnConfig: any[] = [
        {
            numeration: 1,
            minCategoriesInColumn: 3,
            maxSubCategories: 5,
            maxTotalItemsInColumn: 21,
        },
        {
            numeration: 2,
            minCategoriesInColumn: 2,
            maxSubCategories: 5,
            maxTotalItemsInColumn: 14,
        },
        {
            numeration: 3,
            minCategoriesInColumn: 1,
            maxSubCategories: 5,
            maxTotalItemsInColumn: 7,
        },
        {
            numeration: 4,
            minCategoriesInColumn: 1,
            maxSubCategories: 4,
            maxTotalItemsInColumn: 6,
        }
    ]
    @Input() headerCategories!: ICategoryExtended[];

    constructor(
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes) {
            if (!!changes['headerCategories']) {
                this.initCategoriesByColumnConfig();
            }
        }
    }

    ngAfterViewInit(): void {
        const departmentsElement = $('.departments');
        const departments = departmentsElement.length ? new CDepartments(departmentsElement) : null;
        
    }

    initCategoriesByColumnConfig() {
        if (!this.headerCategories) return;
        const catReduceLimit: number = 2;
        const subCatReduceLimit: number = 1;
        for (let cat of this.headerCategories) {
            if (cat.hasChild) {
                cat._columnDrawable = [];
                let children = [...cat.childCategories];
                for (let conf of this.columnConfig) {
                    let columnRaws: ICategoryExtended[] = [];
                    let columnLimit = conf.maxTotalItemsInColumn;
                    while (columnLimit > 0) {
                        let cursor: ICategoryExtended = children[0];
                        if (!cursor) break;
                        if (conf.maxSubCategories > columnLimit - catReduceLimit && cursor.childCategories.length > columnLimit) break;
                        cursor = <any>children.shift();
                        cursor._childCategories = [];
                        columnLimit -= catReduceLimit;
                        let rawLimit = conf.maxSubCategories;
                        for (let subCat of cursor.childCategories) {
                            cursor._childCategories.push(subCat);
                            columnLimit -= 1;
                            rawLimit -= subCatReduceLimit;
                            if (columnLimit <= 0) break;
                            if (rawLimit <= 0) break;
                        }
                        columnRaws.push(cursor);
                    }
                    cat._columnDrawable.push(columnRaws);
                }
                cat._columnDrawable = cat._columnDrawable.filter((itemArr: ICategoryExtended[]) => itemArr && itemArr.length > 0);
            }
        }
    }

}

interface ICategoryExtended extends ICategory {
    _childCategories?: ICategoryExtended[];
    _columnDrawable?: ICategoryExtended[][];
}

class CDepartments {
    private element: any;
    private container: any;
    private linksWrapper: any;
    private body: any;
    private button: any;
    private items: any;
    private mode: any;
    private fixedBy: any;
    private fixedHeight: any;
    private currentItem: any;
    constructor(element: any) {
        const self = this;

        element.data('departmentsInstance', self);

        this.element = element;
        this.container = this.element.find('.departments__submenus-container');
        this.linksWrapper = this.element.find('.departments__links-wrapper');
        this.body = this.element.find('.departments__body');
        this.button = this.element.find('.departments__button');
        this.items = this.element.find('.departments__item');
        this.mode = this.element.is('.departments--fixed') ? 'fixed' : 'normal';
        this.fixedBy = $(this.element.data('departments-fixed-by'));
        this.fixedHeight = 0;
        this.currentItem = null;

        if (this.mode === 'fixed' && this.fixedBy.length) {
            this.fixedHeight = this.fixedBy.offset().top - this.body.offset().top + this.fixedBy.outerHeight();
            this.body.css('height', this.fixedHeight + 'px');
        }

        this.linksWrapper.on('transitionend', (event: any) => {
            if (event.originalEvent.propertyName === 'height') {
                $(this.linksWrapper).css('height', '');
                $(this.linksWrapper).closest('.departments').removeClass('departments--transition');
            }
        });

        this.onButtonClick = this.onButtonClick.bind(this);
        this.onGlobalClick = this.onGlobalClick.bind(this);
        this.onMouseenter = this.onMouseenter.bind(this);
        this.onMouseleave = this.onMouseleave.bind(this);
        this.onTouchClick = this.onTouchClick.bind(this);

        // add event listeners
        this.button.on('click', this.onButtonClick);
        document.addEventListener('click', this.onGlobalClick, true);
        this.touchClick(document, this.onGlobalClick);
        this.items.on('mouseenter', this.onMouseenter);
        this.linksWrapper.on('mouseleave', this.onMouseleave);
        this.touchClick(this.items, this.onTouchClick);
    } 


    onButtonClick(event: any) {
        event.preventDefault();

        if (this.element.is('.departments--open')) {
            this.close();
        } else {
            this.open();
        }
    };

    closeSubmenu(item: any) {
        const submenu = item.data('submenu');

        if (submenu) {
            submenu.removeClass('departments__submenu--open');

            if (submenu.is('.departments__submenu--type--menu')) {
                submenu.find('> .menu').data('menuInstance').unsetCurrentItem();
            }
        }
    };

    onGlobalClick(event: any) {
        if (this.element.not($(event.target).closest('.departments')).length) {
            if (this.element.is('.departments--open')) {
                this.close();
            }
        }
    };

    setMode(mode: any) {
        this.mode = mode;

        if (this.mode === 'normal') {
            this.element.removeClass('departments--fixed');
            this.element.removeClass('departments--open');
            this.body.css('height', 'auto');
        }
        if (this.mode === 'fixed') {
            this.element.addClass('departments--fixed');
            this.element.addClass('departments--open');
            this.body.css('height', this.fixedHeight + 'px');
            $('.departments__links-wrapper', this.element).css('maxHeight', '');
        }
    };

    close() {
        if (this.element.is('.departments--fixed')) {
            return;
        }

        const content = this.element.find('.departments__links-wrapper');
        const startHeight = content.height();

        content.css('height', startHeight + 'px');
        this.element
            .addClass('departments--transition')
            .removeClass('departments--open');

        content.height(); // force reflow
        content.css('height', '');
        content.css('maxHeight', '');

        this.unsetCurrentItem();
    };

    closeImmediately() {
        if (this.element.is('.departments--fixed')) {
            return;
        }

        const content = this.element.find('.departments__links-wrapper');

        this.element.removeClass('departments--open');

        content.css('height', '');
        content.css('maxHeight', '');

        this.unsetCurrentItem();
    };

    open() {
        const content = this.element.find('.departments__links-wrapper');
        const startHeight = content.height();

        this.element
            .addClass('departments--transition')
            .addClass('departments--open');

        const documentHeight = document.documentElement.clientHeight;
        const paddingBottom = 20;
        const contentRect = content[0].getBoundingClientRect();
        const endHeight = Math.min(content.height(), documentHeight - paddingBottom - contentRect.top);

        content.css('height', startHeight + 'px');
        content.height(); // force reflow
        content.css('maxHeight', endHeight + 'px');
        content.css('height', endHeight + 'px');
    };

    onMouseenter(event: any) {
        const targetItem = $(event.currentTarget);

        if (this.currentItem && targetItem.is(this.currentItem)) {
            return;
        }

        this.unsetCurrentItem();
        this.setCurrentItem(targetItem);
    };

    onMouseleave() {
        this.unsetCurrentItem();
    };

    onTouchClick(event: any) {
        const targetItem = $(event.currentTarget);

        if (this.currentItem && this.currentItem.is(targetItem)) {
            return;
        }

        if (this.hasSubmenu(targetItem)) {
            this.preventTouchClick();

            this.unsetCurrentItem();
            this.setCurrentItem(targetItem);
        }
    };

    setCurrentItem(item: any) {
        this.unsetCurrentItem();

        this.currentItem = item;
        this.currentItem.addClass('departments__item--hover');

        this.openSubmenu(this.currentItem);
    };

    unsetCurrentItem() {
        if (this.currentItem) {
            this.closeSubmenu(this.currentItem);

            this.currentItem.removeClass('departments__item--hover');
            this.currentItem = null;
        }
    };

    getSubmenu(item: any) {
        let submenu = item.find('> .departments__submenu');

        if (submenu.length) {
            this.container.append(submenu);

            item.data('submenu', submenu);
        }

        return item.data('submenu');
    };

    hasSubmenu(item: any) {
        return !!this.getSubmenu(item);
    };

    openSubmenu(item: any) {
        const submenu = this.getSubmenu(item);

        if (submenu) {
            submenu.addClass('departments__submenu--open');

            const documentHeight = document.documentElement.clientHeight;
            const paddingBottom = 20;

            if (submenu.hasClass('departments__submenu--type--megamenu')) {
                const submenuTop = submenu.offset().top - $(window).scrollTop();
                submenu.css('maxHeight', (documentHeight - submenuTop - paddingBottom) + 'px');
            }

            if (submenu.hasClass('departments__submenu--type--menu')) {
                submenu.css('maxHeight', (documentHeight - paddingBottom - Math.min(
                    paddingBottom,
                    this.body.offset().top - $(window).scrollTop()
                )) + 'px');

                const submenuHeight = submenu.height();
                const itemTop = this.currentItem.offset().top - $(window).scrollTop();
                const containerTop = this.container.offset().top - $(window).scrollTop();

                submenu.css('top', (Math.min(itemTop, documentHeight - paddingBottom - submenuHeight) - containerTop) + 'px');
            }
        }
    };

    preventTouchClick() {
        const onClick = function(event: any){
            event.preventDefault();

            document.removeEventListener('click', onClick);
        };
        document.addEventListener('click', onClick);
        setTimeout(function() {
            document.removeEventListener('click', onClick);
        }, 100);
    }

    touchClick(elements: any, callback: Function) {
        elements = $(elements);

        let touchStartData: any = null;

        const onTouchstart = function(event: any){
            const originalEvent = event.originalEvent;

            if (originalEvent.touches.length !== 1) {
                touchStartData = null;
                return;
            }

            touchStartData = {
                target: originalEvent.currentTarget,
                touch: originalEvent.changedTouches[0],
                timestamp: (new Date).getTime(),
            };
        };
        const onTouchEnd = function(event: any){
            const originalEvent = event.originalEvent;

            if (
                !touchStartData ||
                originalEvent.changedTouches.length !== 1 ||
                originalEvent.changedTouches[0].identity !== touchStartData.touch.identity
            ) {
                return;
            }

            const timestamp = (new Date).getTime();
            const touch = originalEvent.changedTouches[0];
            const distance = Math.abs(
                Math.sqrt(
                    Math.pow(touchStartData.touch.screenX - touch.screenX, 2) +
                    Math.pow(touchStartData.touch.screenY - touch.screenY, 2)
                )
            );

            if (touchStartData.target === originalEvent.currentTarget && timestamp - touchStartData.timestamp < 500 && distance < 10) {
                callback(event);
            }
        };

        elements.on('touchstart', onTouchstart);
        elements.on('touchend', onTouchEnd);

        return function() {
            elements.off('touchstart', onTouchstart);
            elements.off('touchend', onTouchEnd);
        };
    }
}

    // const departmentsElement = $('.departments');
    // const departments = departmentsElement.length ? new CDepartments(departmentsElement) : null;
