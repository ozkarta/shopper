import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { catchError, mapTo, Observable, map } from "rxjs";
import { CategoryAPIService } from "src/app/shared/http/category.api.service";
import { ICategory } from "src/app/shared/interfaces/category";
import { SpinnerService } from "src/app/shared/services/spinner.service";

@Injectable({providedIn: 'root'})
export class HeaderShopCategoriesResolver implements Resolve<any>{
  constructor(
    private categoryAPIService: CategoryAPIService,
    private spinnerService: SpinnerService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    this.spinnerService.start();
    return loadCategoryData(this.categoryAPIService, 2).pipe(
      map((categories: ICategory[]) => {
        this.spinnerService.stop();
        return categories;
      })
    )
  }
}

// ========================================================================================================
function loadCategoryData(categoryAPIService: CategoryAPIService, depth: number): Observable<ICategory[]> {
  return categoryAPIService.getCategories(undefined, depth).pipe(
    map((categories: ICategory[]) => {
      return categories;
    }),
    catchError((error: any) => {
      console.log(error);
      return [];
    })
  )
}