import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { WebSharedModule } from "../../shared/shared.module";
import { WebErrorComponent } from "./error.component";

const webModuleRoutes: Routes = [
    {
        path: "",
        component: WebErrorComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(webModuleRoutes),
        CommonModule,
        FormsModule,

        WebSharedModule,
    ],
    exports: [
        WebSharedModule,
    ],
    providers: [
    ],
    declarations: [
        WebErrorComponent,
    ]
})
export class WebErrorModule {

}
