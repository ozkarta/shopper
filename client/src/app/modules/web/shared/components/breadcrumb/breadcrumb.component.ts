import { Component, Input } from "@angular/core";
import { IBreadCrumbDataItem } from "src/app/shared/interfaces/breadcrumb";

@Component({
    selector: 'app-web-breadcrumb-component',
    templateUrl: 'breadcrumb.component.html',
    styleUrls: ['breadcrumb.component.css']
})
export class BreadCrumbComponent {
    @Input('breadCrumbData') breadCrumbData!: IBreadCrumbDataItem[];
    private defaultItem: IBreadCrumbDataItem = {
        title: {
            en: 'Home',
            ge: 'სახლი',
        },
        path: '/'
    };

    public get list(): IBreadCrumbDataItem[] {
        return [this.defaultItem, ...(this.breadCrumbData || [])]
    }

    public get lastItem(): IBreadCrumbDataItem | undefined {
        return [...(this.breadCrumbData || [])].pop()
    }
}