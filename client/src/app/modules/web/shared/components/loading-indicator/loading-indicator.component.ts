import { Component, OnDestroy, OnInit } from "@angular/core";
import { LoadingIndicatorBaseComponent } from "src/app/shared/classes/components/loading-indicator.base-component";
import { SpinnerService } from "src/app/shared/services/spinner.service";

@Component({
    selector: 'app-web-loading-indicator',
    templateUrl: 'loading-indicator.component.html',
    styleUrls: ['loading-indicator.component.css']
})

export class WebLoadingIndicator extends LoadingIndicatorBaseComponent implements OnInit, OnDestroy {
    
    constructor(protected override spinnerService: SpinnerService) {
        super(spinnerService);
    }

    override ngOnInit(): void {
        super.ngOnInit();
    }

    override ngOnDestroy(): void {
        super.ngOnDestroy();
    }
}