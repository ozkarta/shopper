import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ComponentsModule } from "./components/components.module";

import { SharedModule as GlobalSharedModule } from "src/app/shared/shared.module";



const declarationComponents: any[] = [
  // pipes
]

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        ComponentsModule,        
        GlobalSharedModule,
    ],
    exports: [
      ...declarationComponents,
      ComponentsModule,
      GlobalSharedModule,
    ],
    declarations: declarationComponents,
    providers: [
    ],
  entryComponents: [
  ]
})
export class WebSharedModule {
  constructor() {}
}
