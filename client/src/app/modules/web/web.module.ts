import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// ============================================
import { WebComponent } from './web.component';
import { WebSharedModule } from "./shared/shared.module";

const webModuleRoutes: Routes = [
    {
        path: "",
        // component: WebComponent,
        children: [
          { path: "error", loadChildren: () => import("./routes/error/error.module").then(mod => mod.WebErrorModule ) }, 
          { path: "", loadChildren: () => import("./routes/main/main.module").then(mod => mod.WebMainModule ) },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(webModuleRoutes),
        CommonModule,
        FormsModule,

        WebSharedModule,
    ],
    exports: [
        WebComponent,
        WebSharedModule,
    ],
    providers: [
    ],
    declarations: [
        WebComponent,
    ]
})
export class WebModule {

}
