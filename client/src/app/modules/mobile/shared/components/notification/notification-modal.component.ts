import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { BaseNotificationModalComponent } from 'src/app/shared/classes/components/base-notification-modal.component';
import { INotificationServiceOptions } from 'src/app/shared/interfaces/notifications';
import { ModalService } from 'src/app/shared/services/modals.service';
@Component({
  selector: 'app-notification-web-component',
  templateUrl: './notification-modal.component.html',
  styleUrls: ['./notification-modal.component.css']
})

export class NotificationModalComponent extends BaseNotificationModalComponent {
  constructor(
    @Inject('data')data: {notificationStream: Observable<INotificationServiceOptions>},
    protected override modalService: ModalService,
    protected override router: Router,
    protected override route: ActivatedRoute) {
      super(data, modalService, router, route);
  }

  override ngOnInit() {
    super.ngOnInit();
  }

  override ngOnDestroy() {
    super.ngOnDestroy()
  }

}

