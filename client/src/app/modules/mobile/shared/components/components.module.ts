import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PaginationComponent } from "./pagination/pagination.component";
import { SharedModule } from "src/app/shared/shared.module";
import { NotificationModalComponent } from "./notification/notification-modal.component";
// modules

// Components

const declatations: any[] = [
  PaginationComponent,
  NotificationModalComponent,
]

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        SharedModule
    ],
    exports: [
      ...declatations,
    ],
    declarations: [
      ...declatations
    ]
})
export class ComponentsModule {}
