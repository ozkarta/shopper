import { Component, OnDestroy, OnInit } from "@angular/core";
import { PaginationBaseComponent } from "src/app/shared/classes/components/pagination.base-component";

@Component({
  selector: 'app-web-pagination',
  templateUrl: 'pagination.component.html',
  styleUrls: ['pagination.component.css']
})
export class PaginationComponent extends PaginationBaseComponent implements OnInit, OnDestroy {
  constructor() {
    super();
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}