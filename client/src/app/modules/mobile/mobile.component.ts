import { Component } from "@angular/core";

@Component({
  selector: 'app-mobile-component',
  templateUrl: 'mobile.component.html',
  styleUrls: ['mobile.component.css']
})
export class MobileComponent {}