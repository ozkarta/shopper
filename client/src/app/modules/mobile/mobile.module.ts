import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MobileSharedModule } from "./shared/shared.module";
import { MobileComponent } from "./mobile.component";

// ============================================

const webModuleRoutes: Routes = [
    {
        path: "",
        // component: MobileComponent,
        children: [
            
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(webModuleRoutes),
        CommonModule,
        FormsModule,

        MobileSharedModule,
    ],
    exports: [
        MobileComponent,
        MobileSharedModule,
    ],
    providers: [
    ],
    declarations: [
        MobileComponent,
    ]
})
export class MobileModule {

}
