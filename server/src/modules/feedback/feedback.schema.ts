import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type FeedbackDocument = Feedback & Document;

@Schema({timestamps: true})
export class Feedback {

}

export const FeedbackSchema = SchemaFactory.createForClass(Feedback);