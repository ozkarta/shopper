import { ConsoleLogger, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { BaseService } from "src/shared/classes/base-service";
import { ProductService } from "../product/product.service";
import { Cart, CartDocument } from "./cart.schema";

@Injectable()
export class CartService extends BaseService {
  constructor(
    @InjectModel(Cart.name) private readonly userModel: Model<CartDocument>,
    private productService: ProductService,
  ) {
    super(userModel);
  }

  async createOrUpdate(payload: {userId: string, productId: string, qty: number, metadata: unknown}) {
    let {userId, productId} = payload;
    let cartItem: any = await this.getOneByQuery({
      userId, 
      productId,
      $or: [
        {deleted: {$exists: false}},
        {deleted: {$eq: false}}
      ]
    });
    if (!!cartItem) {
      return await this.updateById(cartItem._id, {
        $inc: {
          qty: payload.qty
        }
      })
    } else {
      return await this.createOne(payload);
    }
  }

  async getUsersCartData(userId: string) {
    let findStage = {
      '$match': {
        'userId': userId,
        '$or': [
          {
            'deleted': {
              '$exists': false
            }
          }, {
            'deleted': {
              '$eq': false
            }
          }
        ]
      }
    }
    let lookupProductsStage = {
      '$lookup': {
        'from': 'products', 
        'localField': 'productId', 
        'foreignField': '_id', 
        'as': 'product'
      }
    }
    let unwindProductStage = {
      '$unwind': {
        'path': '$product'
      }
    }
    let projectFieldsStage = {
      '$project': {
        '_id': 1, 
        'userId': 1, 
        'product': 1,
        'productId': 1,
        'qty': 1, 
        'createdAt': 1, 
        'updatedAt': 1
      }
    }
    let groupStage = {
      '$group': {
        '_id': '$userId', 
        'cartItems': {
          '$push': '$$ROOT'
        }
      }
    };
    let replaceIdWithUserIdStage = {
      '$project': {
        '_id': 0, 
        'userId': '$_id', 
        'cartItems': 1,
        'numTotal': {
          $size: '$cartItems'
        }
      }
    };

    let findPipeline = [
      findStage, 
      // lookupProductsStage, 
      // unwindProductStage, 
      projectFieldsStage, 
      groupStage, 
      replaceIdWithUserIdStage
    ];
    let countPipeline = [
      findStage, 
      // lookupProductsStage, 
      // unwindProductStage, 
      projectFieldsStage, 
      groupStage
    ];
    // ========================================================================================================================
    let cart = (await this.getByAggregationQuery(findPipeline, countPipeline)).items[0] || {userId, cartItems: [], numTotal: 0};
    let productIdArray: string[] = cart.cartItems.map((item: any) => item.productId);
    let productPopulation = [
      {
        path: 'shop',
      },
      {
        path: 'categories',
      },
      {
        path: 'media.item',
        populate: [
          {
            path: 'originalImage',
            select: 'size path mimetype filename',
          },
          {
            path: 'highResolutionImage',
            select: 'size path mimetype filename',
          },
          {
            path: 'mediumResolutionImage',
            select: 'size path mimetype filename',
          },
          {
            path: 'lowResolutionImage',
            select: 'size path mimetype filename',
          },
        ],
      },
    ]
    let cartProducts: any[] = (await this.productService.getByQuery({
      find: {
        _id: {$in: productIdArray}
      },
      populate: <any>productPopulation
    })).items;
    
    cart.cartItems.map((item: any) => {
      item.product = cartProducts.find((p) => p._id.toString() === item.productId.toString());
      return item;
    });

    cart.subTotal = cart.cartItems.reduce((accumulator: number, cartItem: any) => {
      return accumulator + (cartItem.qty || 0) * (cartItem.product.price || 0);
    }, 0)
    cart.shipping = await calculateShippingSomehow(cart);
    cart.tax = getTaxValue(cart);
    cart.total = cart.subTotal + cart.shipping + cart.tax;
    return cart;
  }

  async removeProductFromCart(userId: string, productId: string): Promise<any> {
    return this.setDeletedFlagByQuery({
      userId, 
      productId,
      $or: [
        {deleted: {$exists: false}},
        {deleted: {$eq: false}}
      ]
    }, true);
  }

  async removeItemFromCart(userId: string, cartItemId: string) {
    return this.setDeletedFlagByQuery({
      userId,
      _id: cartItemId
    }, true);
  }
}

async function calculateShippingSomehow(cart): Promise<number> {
  if (cart.subTotal === 0) return 0;
  return 45;
}

function getTaxValue(cart): number {
  return cart.subTotal * 20/100;
}