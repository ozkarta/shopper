import { createParamDecorator, ExecutionContext } from "@nestjs/common";

export const parseAddProduToCartBody = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { productId, qty } = request.body;
    let parsed: any = {
      productId, qty
    }
    return parsed;
  },
);