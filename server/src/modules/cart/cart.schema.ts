import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
// ====================================================
import { User } from 'src/modules/user/user.schema';
import { Product } from 'src/modules/product/product.schema';

export type CartDocument = Cart & Document;

@Schema({timestamps: true})
export class Cart {
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true})
    userId: User
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Product', required: true})
    productId: Product
    @Prop({type: Number, default: 1})
    qty: number;
    @Prop()
    metadata: mongoose.Schema.Types.Mixed;
    @Prop({type: Boolean, default: false})
    deleted: boolean;
}

export const CartSchema = SchemaFactory.createForClass(Cart);