import * as Joi from "joi";
import { JoiObjectId } from "src/shared/_helpers/joi-extensions";

export const addProductToCartValidationSchema: Joi.ObjectSchema<any> = Joi.object({
  productId: JoiObjectId().required(),
  qty: Joi.number().required().min(-500).max(500),
})

export const removeProductFromCartJoiValidationSchema: Joi.ObjectSchema<any> = Joi.object({
  cartItemId: JoiObjectId().required(),
})