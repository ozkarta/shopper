import { Body, Controller, Delete, Get, Next, Param, Post, Req, Res } from "@nestjs/common";
import { NextFunction, Request, Response } from "express";
import { UserRoles } from "src/shared/constants";
import { Authenticated } from "src/shared/decorators/authenticated";
import { User } from "src/shared/decorators/get-user";
import { Roles } from "src/shared/decorators/roles";
import { JoiValidationPipe } from "src/shared/pipes/joi-validate";
import { ProductService } from "../product/product.service";
import { parseAddProduToCartBody } from "./cart.parser";
import { CartService } from "./cart.service";
import { addProductToCartValidationSchema, removeProductFromCartJoiValidationSchema } from "./cart.validator";

@Controller('cart')
export class CartController {
  constructor(
    private cartService: CartService,
    private productService: ProductService
  ) {}

  @Get('')
  @Authenticated(true)
  @Roles([UserRoles.CLIENT, UserRoles.VISITOR])
  async getUserCart(
    @User() user: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    let result = await this.cartService.getUsersCartData(user._id);
    return res.status(200).json(result);
  }

  @Post('')
  @Authenticated(true)
  @Roles([UserRoles.CLIENT, UserRoles.VISITOR])
  async upsertProductToCart(
    @Body(new JoiValidationPipe(addProductToCartValidationSchema)) body: any,
    @User() user: any,
    @parseAddProduToCartBody() payload: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    let product: any = await this.productService.getById(payload.productId);
    payload.userId = user._id;
    payload.metadata = {
      user,
      product,
    }
    await this.cartService.createOrUpdate(payload);
    // let result = await this.cartService.getUsersCartData(user._id);
    return res.status(200).json({});
  }

  @Delete(':cartItemId')
  @Authenticated(true)
  @Roles([UserRoles.CLIENT, UserRoles.VISITOR])
  async removeProductFromCart(
    @User() user: any,
    @Param(new JoiValidationPipe(removeProductFromCartJoiValidationSchema)) params: any,
    @Param('cartItemId') cartItemId: string,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    await this.cartService.removeItemFromCart(user._id, cartItemId);
    return res.status(200).json({});
  }
  
}