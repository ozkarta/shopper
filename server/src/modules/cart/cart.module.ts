import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ProductModule } from "../product/product.module";
import { CartController } from "./cart.controller";
import { Cart, CartSchema } from "./cart.schema";
import { CartService } from "./cart.service";
@Module({
  imports:[
    MongooseModule.forFeature([{name: Cart.name, schema: CartSchema}]),
    ProductModule,
  ],
  controllers: [
    CartController
  ],
  providers: [
    CartService,
  ],
  exports: [

  ]
})
export class CartModule {}