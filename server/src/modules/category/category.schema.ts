import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { LocalizationString } from 'src/modules/localization-string/localization-string.schema';

export type CategoryDocument = Category & Document;

@Schema({timestamps: true})
export class Category {

    @Prop({type: LocalizationString})
    categoryName: LocalizationString;
    @Prop({type: LocalizationString})
    friendlyId:   LocalizationString;
    @Prop({type: Boolean, default: false, es_indexed: true})
    includeInSearch: boolean;
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Category', es_indexed: true})
    parentCategory: Category;
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Category', es_indexed: true}]})
    childCategories: Category[]
    @Prop({type: Number, default: 0})
    sortWeight: number;
}

export const CategorySchema = SchemaFactory.createForClass(Category);
CategorySchema.virtual('hasChild')
    .get(function(this: any) {
        return this.childCategories && Array.isArray(this.childCategories) &&  this.childCategories.length > 0;
    });

CategorySchema.virtual('hasPopulatedChild')
    .get(function(this: any) {
        return this.childCategories && Array.isArray(this.childCategories) && this.childCategories.filter((item: any) => {
            return item && typeof item === 'object' && Object.keys(item).length > 0;
        }).length > 0;
    });