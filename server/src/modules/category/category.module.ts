import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose';
import { categorySortHeaderAssignPlugin } from 'src/shared/mongoose-plugins/category-sort-weight-assign.plugin';
import { CategoryController } from './category.controller';

import { Category, CategorySchema } from './category.schema';
import { CategoryService } from './category.service';

@Module({
    imports: [
        MongooseModule.forFeatureAsync([
            {
                name: Category.name,
                useFactory: () => {
                    const schema = CategorySchema;
                    schema.plugin(categorySortHeaderAssignPlugin);
                    return schema;
                }
            }
        ])
    ],
    providers: [
        CategoryService,
    ],
    exports: [
        CategoryService,
    ],
    controllers: [
        CategoryController
    ]
})

export class CategoryModule {}