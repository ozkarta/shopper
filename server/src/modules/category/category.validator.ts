import * as Joi from 'joi';
import { JoiObjectId } from 'src/shared/_helpers/joi-extensions';

export const getByQueryJoiValidationSchema: Joi.ObjectSchema<any> = Joi.object({
  categoryId: JoiObjectId().optional().allow(null, ''),
  depth: Joi.number().optional().allow(null, ''),
})

export const categryIdParamValidationSchema: Joi.ObjectSchema<any> = Joi.object({
  categoryId: JoiObjectId().required(),
})