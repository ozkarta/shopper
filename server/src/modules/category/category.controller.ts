import {
  Controller,
  Get,
  HttpStatus,
  Next,
  Param,
  Put,
  Query,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { UserRoles } from 'src/shared/constants';
import { Authenticated } from 'src/shared/decorators/authenticated';
import { Roles } from 'src/shared/decorators/roles';
import { JoiValidationPipe } from 'src/shared/pipes/joi-validate';
import { parseGetCategoriesByQuery } from './category.parser';
import { CategoryService } from './category.service';
import {
  categryIdParamValidationSchema,
  getByQueryJoiValidationSchema,
} from './category.validator';

@Controller('category')
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Get()
  async getCategoriesByQuery(
    @Query(new JoiValidationPipe(getByQueryJoiValidationSchema)) query: any,
    @parseGetCategoriesByQuery() parsed: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    let result = (await this.categoryService.getByQuery(parsed)).items;
    return res.status(200).json(result);
  }

  // we need this for breeadcrumb
  @Get('ascendants/:categoryId')
  async getAscendantCategories(
    @Param(new JoiValidationPipe(categryIdParamValidationSchema)) params: any,
    @Param('categoryId') categoryId: string,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    let result: any = await this.categoryService.getAllAscendantCategories(categoryId);
    return res.status(200).json(result);
  }

  //TODO parse data eree
  @Put('category-location/:categoryId')
  @Authenticated(true)
  @Roles([UserRoles.ADMIN])
  async updateCategoryLocation(
    @Param('categoryId') categoryId: string,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    let { sortWeight, parentCategory } = req.body;
    let result = await this.categoryService.updateById(categoryId, {
      $set: {
        sortWeight,
        parentCategory,
      }
    });
    return res.status(HttpStatus.ACCEPTED).json(result);
  }
}
