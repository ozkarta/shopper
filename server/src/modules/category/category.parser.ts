import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { CategoryService } from "./category.service";

export const parseGetCategoriesByQuery = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { query } = request;
    const { categoryId, depth } = query;
    let parsed: any = {
      find: {
      },
      populate: CategoryService.generateNthLevelPopulation(+depth || 0, 'childCategories'),
      sort: {
        sortWeight: 1
      }
    };
    if (categoryId) {
      parsed.find = Object.assign({}, parsed.find || {}, {parentCategory: categoryId});
    } else {
      parsed.find = Object.assign({}, parsed.find || {}, { parentCategory: null });
    }
    return parsed;
  },
);