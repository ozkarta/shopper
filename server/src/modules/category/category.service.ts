import { Injectable } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';
import { Category, CategoryDocument } from 'src/modules/category/category.schema';
import { BaseService } from "src/shared/classes/base-service";

@Injectable()
export class CategoryService extends BaseService {
    constructor(
        @InjectModel(Category.name) private readonly categoryModel: Model<CategoryDocument>
    ) {
        super(categoryModel);
    }

    async getAllAscendantCategories(categoryId: string): Promise<any[]> {
        let category = await this.getById(categoryId, true, '_id parentCategory categoryName', CategoryService.generateNthLevelPopulation(100, 'parentCategory'));
        let categories: any[] = [];
        while (!!category) {
            categories.unshift(category);
            category = category.parentCategory;
        }
        return categories;
    }

    async getAllAscendantCategoryIDs(categoryId: string): Promise<(string | mongoose.Types.ObjectId)[]> {
        let category = await this.getById(categoryId, true, '_id parentCategory', CategoryService.generateNthLevelPopulation(100, 'parentCategory'));
        let categoryIDs = [category._id];
        while (!!category.parentCategory) {
            categoryIDs.push(category.parentCategory._id);
            category = category.parentCategory;
        }
        return categoryIDs;
    }

    async getAllDescendantCategoryIDs(categoryId: string): Promise<(string | mongoose.Types.ObjectId)[]> {
        let category = await this.getById(categoryId, true, '_id childCategories', CategoryService.generateNthLevelPopulation(100, 'childCategories'));
        let categoryIDs = this.getAllChildCategoryIDs_recursive(category);        
        return categoryIDs;
    }
    // =====================================================
    public static generateNthLevelPopulation(level: number = 0, path: string) {
        if (level === 0) return [];
        let result;
        let ref: any = {
          path
        };
        result = ref;
        for (let i = 0; i < level-1; i++) {
          ref.populate = { path: 'childCategories' };
          ref.options = { sort: { 'sortWeight': 1 } };
          ref = ref.populate;
        }
        return [result];
    }

    private getAllChildCategoryIDs_recursive(category: any): (string | mongoose.Types.ObjectId)[] {
        let categoryIDs: (string | mongoose.Types.ObjectId)[] = [category._id];

        if (!!category?.childCategories && category.childCategories.length > 0) {
            for (let childCategory of category.childCategories) {
                categoryIDs.push(...this.getAllChildCategoryIDs_recursive(childCategory));
            }
        }
        return categoryIDs;
    }
}