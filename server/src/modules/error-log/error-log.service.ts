import { Injectable } from "@nestjs/common";
import { BadRequestError } from "src/shared/classes/errors";
import * as fs from 'fs';
import * as path from 'path';
import * as util from 'util'


@Injectable()
export class ErrrLogService {
  private readonly homePath = process.env.HOME || process.env.HOMEPATH;
  private readonly appName = process.env.appName || 'shopper-APP';
  private readonly serverDataDirName = process.env.serverDataDirName || `.${this.appName}-seerver-data`;
  private readonly dataRoot = path.join(this.homePath, this.serverDataDirName);
  private readonly logsDirPath = path.join(this.dataRoot, process.env.logsDirName || 'logs');
  constructor() {}

  async getLogDirectories(parsed: any): Promise<any> {
    const {offset, limit, keyword} = parsed;
    const readDir = util.promisify(fs.readdir);
    let directories: any[] = (await readDir(this.logsDirPath)).sort();
    if (keyword) {
      directories = directories.filter(item => item.match(new RegExp(`^${keyword}.*`, 'g')));
    }
    
    directories = (await this.filterAsync(directories, async (item ) => this.isDirectory(path.join(this.logsDirPath, item))))
      .map(item => {return {title: item}});
    return {items: directories.slice(offset, offset + limit), numTotal: directories.length};    
  }
    
  async getFilesByDirectory(parsed: any) {
    const {directoryName} = parsed;
    const readDir = util.promisify(fs.readdir)
    let directoryPath = path.join(this.logsDirPath, directoryName);
    
    if (!directoryName) throw new BadRequestError(`directoryName is required!`);
    if (!fs.existsSync(directoryPath)) {
      throw new BadRequestError(`directoryName is not correct!`);
    }


    let files: any[] = await readDir(directoryPath);
    
    files = (await this.filterAsync(files, async (item) => this.isFile(path.join(directoryPath, item))))
      .map(item => {return {title: item}})
    return {items: files, numTotal: files.length};
    
  }
    
  async getFile(parsed: any) {
    const {fileName, directoryName} = parsed;
  
      if (!fileName) throw new BadRequestError(`fileName is required!`);
      if (!directoryName) throw new BadRequestError(`directoryName is required!`);
  
      let directoryPath = path.join(this.logsDirPath, directoryName);
      let filePath = path.join(directoryPath, fileName);
      
      if (!fs.existsSync(filePath)) {
        throw new BadRequestError(`File does not exist!`);
      }
  
      const fileContent = fs.readFileSync(filePath).toString();
      let fileContentArray = fileContent.split('\n').filter(item => !!item).map(item => {
        try {
          return JSON.parse(item);
        } catch (error) {
          return null;
        }
      }).filter(item => !!item);
  
      return {title: fileName, content: fileContentArray}
  }
  // =================================================================================================
  private async isDirectory(source) {
    const lstat = util.promisify(fs.lstat)
    return (await lstat(source)).isDirectory()
  }
  private async isFile(source) {
    const lstat = util.promisify(fs.lstat)
    return (await lstat(source)).isFile();
  } 

  private async filterAsync(array: any[], predicate: any): Promise<any[]> {
    const results = await Promise.all(array.map(predicate));

	  return array.filter((_v, index) => results[index]);
  }
}