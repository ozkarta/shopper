import { Controller, Get, Next, Query, Req, Res } from "@nestjs/common";
import { NextFunction, Request, Response } from "express";
import { UserRoles } from "src/shared/constants";
import { Authenticated } from "src/shared/decorators/authenticated";
import { Roles } from "src/shared/decorators/roles";
import { JoiValidationPipe } from "src/shared/pipes/joi-validate";
import { ErrrLogService } from "./error-log.service";
import { parseGetDirectories, parseGetFile, parseGetFilesByDirectory } from "./error.log.parser";
import { getFilesByDirectoryValidationSchema, getLogFileValidationScema, logDirectoriesValidationSchema } from "./errr-log.validators";

@Controller('error-log')
export class ErrorLogController {
    constructor(private service: ErrrLogService) {}

    @Get('log-directories')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async getLogDirectories(@Query(new JoiValidationPipe(logDirectoriesValidationSchema)) query: any, @parseGetDirectories() parsed: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        return res.status(200).json(await this.service.getLogDirectories(parsed));
    }

    @Get('files-by-directory')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async getFilesByDirectory(@Query(new JoiValidationPipe(getFilesByDirectoryValidationSchema)) query: any, @parseGetFilesByDirectory() parsed: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        return res.status(200).json(await this.service.getFilesByDirectory(parsed))
    }

    @Get('file')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async getFile(@Query(new JoiValidationPipe(getLogFileValidationScema)) query: any, @parseGetFile() parsed: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        return res.status(200).json(await this.service.getFile(parsed));
    }
}