import * as Joi from 'joi';

export const logDirectoriesValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    keyword: Joi.string().optional().allow(null, ''),
    page: Joi.number().min(1).optional(),
    limit: Joi.number().max(200).optional()
})

export const getFilesByDirectoryValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    directoryName: Joi.string().required(),
    page: Joi.number().min(1).optional(),
    limit: Joi.number().max(200).optional(),
    keyword: Joi.string().optional().allow(null, ''),
})

export const getLogFileValidationScema: Joi.ObjectSchema<any> = Joi.object({
    fileName: Joi.string().required(),
    directoryName: Joi.string().required(),
})