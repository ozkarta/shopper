import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { Request } from "express";
import { parseOffsetAndLimit } from "src/shared/_helpers/parser";

export const parseGetDirectories = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
      const req: Request = ctx.switchToHttp().getRequest();
      const { query } = req;
      let parsed: any = {
        ...parseOffsetAndLimit(<any>(req.query)),
        keyword: req.query.keyword,
      }
      return parsed;
    },
);
export const parseGetFilesByDirectory = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
      const req: Request = ctx.switchToHttp().getRequest();
      const { query } = req;
      let parsed: any = {
        ...parseOffsetAndLimit(<any>(req.query)),
        directoryName: req.query.directoryName
      }
      return parsed;
    },
);

export const parseGetFile = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
      const req: Request = ctx.switchToHttp().getRequest();
      const { query } = req;
      let parsed: any = {
        fileName: req.query.fileName,
        directoryName: req.query.directoryName
      }
      return parsed;
    },
);
