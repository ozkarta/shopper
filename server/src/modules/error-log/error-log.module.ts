import { Module } from "@nestjs/common";
import { ErrorLogController } from "./error-log.controller";
import { ErrrLogService } from "./error-log.service";

@Module({
    imports: [
    ],
    exports: [

    ],
    providers: [
        ErrrLogService
    ],
    controllers: [
        ErrorLogController
    ]
})
export class ErrorLogModule {}