import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Document, Model } from "mongoose";
import { BaseService } from "src/shared/classes/base-service";
import { Product, ProductDocument } from "./product.schema";

@Injectable()
export class ProductService extends BaseService {
    constructor(@InjectModel(Product.name) private readonly productModel: Model<ProductDocument>) {
        super(productModel)
    }
}