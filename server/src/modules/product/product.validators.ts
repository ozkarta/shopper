import * as Joi from "joi";
import { JoiLocalizationString, JoiObjectId } from "src/shared/_helpers/joi-extensions";

export const getByQueryJoiValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    categoryId: JoiObjectId().optional().allow(null, ''),
    keyword: Joi.string().optional().allow(null, ''),
    page: Joi.number().min(1).optional(),
    limit: Joi.number().max(200).optional(),
    sortBy: Joi.string().max(100).optional(),
})

export const productIdParamVaildationSchema: Joi.ObjectSchema<any> = Joi.object({
    productId: JoiObjectId().required()
})

export const createProductWithShopValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    shop: JoiObjectId().required(),
    title: JoiLocalizationString().required(),
    friendlyId: JoiLocalizationString().required(),
    shortDescription: JoiLocalizationString().required(),
    longDescription: JoiLocalizationString().required()
})

export const updateProductMediaByIdBodyValidationSchema: Joi.ArraySchema = Joi.array().items(
    Joi.object({
        isPrimary: Joi.boolean().required(),
        item: JoiObjectId().required()
    })
)

export const updateProductCategoriesAndKeywordsBodyValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    categories: Joi.array().min(1).max(10).items(JoiObjectId()).required(),
    keywords: Joi.array().min(1).max(10).items(Joi.string()).required()
})

export const updateProductEnabledStatusBodyValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    enabled: Joi.boolean().required()
})

export const updateProducPriceAndDiscountInfoBodyValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    price: Joi.number().required().min(1).max(100000),
    quantity: Joi.number().required().min(1).max(1000)
})