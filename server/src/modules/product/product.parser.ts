import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { parseOffsetAndLimit, parseSortBy } from 'src/shared/_helpers/parser';

export const parseGetByQuery = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { query } = request;
    let parsed: any = {
      ...parseOffsetAndLimit(query),
      ...parseSortBy(query),
      find: {},
      or: undefined,
      populate: [
        {
          path: 'shop',
        },
        {
          path: 'categories',
        },
        {
          path: 'media.item',
          populate: [
            {
              path: 'originalImage',
              select: 'size path mimetype filename',
            },
            {
              path: 'highResolutionImage',
              select: 'size path mimetype filename',
            },
            {
              path: 'mediumResolutionImage',
              select: 'size path mimetype filename',
            },
            {
              path: 'lowResolutionImage',
              select: 'size path mimetype filename',
            },
          ],
        },
      ],
    };
    return parsed;
  },
);

export const parseCreateProductwithShop = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { body } = request;
    const { shop, title, friendlyId, shortDescription, longDescription } = body;
    let parsed: any = {
      shop,
      title,
      friendlyId,
      shortDescription,
      longDescription,
    };
    return parsed;
  },
);

export const parseUpdateProductMedia = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { body } = request;
    let parsed: any = {
      media: body,
    };
    return parsed;
  },
);

export const parseUpdateProductCategoriesAndKeywords = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { categories, keywords } = request.body || {};
    let parsed: any = {
      categories,
      keywords,
    };
    return parsed;
  },
);

export const parseUpdateProductEnabledStatus = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { enabled } = request.body || {};
    let parsed: any = {
      enabled,
    };
    return parsed;
  },
);

export const parseUpdateProductPriceAndDiscounts = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { quantity, price } = request.body || {};
    let parsed: any = {
      quantity, price
    };
    return parsed;
  },
);