import {
  Controller,
  Get,
  Next,
  Req,
  Res,
  Query,
  Param,
  Post,
  Body,
  Put,
  Delete,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { ProductService } from './product.service';
import {
  parseGetByQuery,
  parseCreateProductwithShop,
  parseUpdateProductMedia,
  parseUpdateProductCategoriesAndKeywords,
  parseUpdateProductEnabledStatus,
  parseUpdateProductPriceAndDiscounts,
} from './product.parser';
import {
  getByQueryJoiValidationSchema,
  createProductWithShopValidationSchema,
  updateProductMediaByIdBodyValidationSchema,
  updateProductCategoriesAndKeywordsBodyValidationSchema,
  productIdParamVaildationSchema,
  updateProductEnabledStatusBodyValidationSchema,
  updateProducPriceAndDiscountInfoBodyValidationSchema,
} from './product.validators';
import { JoiValidationPipe } from 'src/shared/pipes/joi-validate';
import { Authenticated } from 'src/shared/decorators/authenticated';
import { Roles } from 'src/shared/decorators/roles';
import { UserRoles } from 'src/shared/constants';
import { User } from 'src/shared/decorators/get-user';
import { CategoryService } from '../category/category.service';

@Controller('product')
export class ProductController {
  private readonly defaultProductPopulation = [
    {
      path: 'shop',
    },
    {
      path: 'categories',
    },
    {
      path: 'media.item',
      populate: [
        {
          path: 'originalImage',
          select: 'size path mimetype filename',
        },
        {
          path: 'highResolutionImage',
          select: 'size path mimetype filename',
        },
        {
          path: 'mediumResolutionImage',
          select: 'size path mimetype filename',
        },
        {
          path: 'lowResolutionImage',
          select: 'size path mimetype filename',
        },
      ],
    },
  ];
  constructor(private productService: ProductService, private categoryService: CategoryService) {}

  // ==========================================================GET===============================================================================
  @Get('query')
  async queryProducts(
    @Query(new JoiValidationPipe(getByQueryJoiValidationSchema)) query: any,
    @parseGetByQuery() parsed: any,
    @Query('categoryId') categoryId: string,
    @User() user: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    // for client show only
    if (!user || user.role === UserRoles.CLIENT) {
      parsed.find = Object.assign({}, parsed.find || {}, {
        $and: [
          {deleted: {$in: [null, false]}},
          {enabled: {$eq: true}},
        ]
      });
    }
    let categories: any[];
    // generate all child categories for categoryId
    if (categoryId) {
      categories = await this.categoryService.getAllDescendantCategoryIDs(categoryId);
      if (categories.length > 0) {
        parsed.find = Object.assign({}, parsed.find || {}, {
          categories: {$in: categories},
        });
      }
    }
    // ========================================================
    let result = await this.productService.getByQuery(parsed);
    return res.status(200).json(result);
  }

  @Get(':productId')
  async getProductById(
    @Param(new JoiValidationPipe(productIdParamVaildationSchema)) params: any,
    @Param('productId') productId: string,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    let product = await this.productService.getById(
      productId,
      true,
      undefined,
      this.defaultProductPopulation,
    );
    return res.status(200).json(product);
  }

  // ============================================================POST=============================================================================
  // TODO parse body
  @Post('product-with-shop')
  @Authenticated(true)
  @Roles([UserRoles.ADMIN])
  async createProductWithShop(
    @Body(new JoiValidationPipe(createProductWithShopValidationSchema))
    body: any,
    @parseCreateProductwithShop() parsed: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    let createdProduct = await this.productService.createOne(parsed);
    let product = await this.productService.getById(createdProduct._id);
    return res.status(200).json(product);
  }

  // ==============================================================PUT===============================================================================
  @Put('product-media/:productId')
  @Authenticated(true)
  @Roles([UserRoles.ADMIN])
  async updateProductMedia(
    @Param(new JoiValidationPipe(productIdParamVaildationSchema)) params: any,
    @Param('productId') productId: string,
    @Body(new JoiValidationPipe(updateProductMediaByIdBodyValidationSchema))
    body: any,
    @parseUpdateProductMedia() parsed: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    await this.productService.updateById(productId, parsed);
    let updatedProduct = await this.productService.getById(
      productId,
      true,
      undefined,
      this.defaultProductPopulation,
    );
    return res.status(200).json(updatedProduct);
  }

  @Put('product-categories-and-keywords/:productId')
  @Authenticated(true)
  @Roles([UserRoles.ADMIN])
  async updateProductCategoriesAndKeywords(
    @Param(new JoiValidationPipe(productIdParamVaildationSchema)) params: any,
    @Param('productId') productId,
    @Body(
      new JoiValidationPipe(
        updateProductCategoriesAndKeywordsBodyValidationSchema,
      ),
    )
    body: any,
    @parseUpdateProductCategoriesAndKeywords() parsed: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    await this.productService.updateById(productId, parsed);
    let updatedProduct = await this.productService.getById(
      productId,
      true,
      undefined,
      this.defaultProductPopulation,
    );
    return res.status(200).json(updatedProduct);
  }

  @Put('product-enabled-status/:productId')
  @Authenticated(true)
  @Roles([UserRoles.ADMIN])
  async updateProductEnabledStatus(
    @Param(new JoiValidationPipe(productIdParamVaildationSchema)) params: any,
    @Param('productId') productId: string,
    @Body(new JoiValidationPipe(updateProductEnabledStatusBodyValidationSchema)) body: any,
    @parseUpdateProductEnabledStatus() parsed: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    await this.productService.updateById(productId, parsed);
    let updatedProduct = await this.productService.getById(
      productId,
      true,
      undefined,
      this.defaultProductPopulation,
    );
    return res.status(200).json(updatedProduct);
  }

  @Put('/price-and-discuoont-data/:productId')
  @Authenticated(true)
  @Roles([UserRoles.ADMIN])
  async updateProductPriceAndDiscountData(
    @Param(new JoiValidationPipe(productIdParamVaildationSchema)) params: any,
    @Param('productId') productId: string,
    @Body(new JoiValidationPipe(updateProducPriceAndDiscountInfoBodyValidationSchema)) body: any,
    @parseUpdateProductPriceAndDiscounts() parsed: any,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    await this.productService.updateById(productId, parsed);
    let updatedProduct = await this.productService.getById(
      productId,
      true,
      undefined,
      this.defaultProductPopulation,
    );
    return res.status(200).json(updatedProduct);
  }

  // ==================================================================DELETE===========================================================================
  @Delete(':productId')
  @Authenticated(true)
  @Roles([UserRoles.ADMIN])
  async deleteProduct(
    @Param(new JoiValidationPipe(productIdParamVaildationSchema)) params: any,
    @Param('productId') productId: string,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
    ) {
      await this.productService.setDeletedFlag(productId, true)
      return res.status(200).json();
    }

  
  @Delete('imediate/:productId')
  @Authenticated(true)
  @Roles([UserRoles.ADMIN])
  async deleteProductImediate(
    @Param(new JoiValidationPipe(productIdParamVaildationSchema)) params: any,
    @Param('productId') productId: string,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
    ) {
      await this.productService.findOneAndDelete(productId)
      return res.status(200).json();
  }

    
}
