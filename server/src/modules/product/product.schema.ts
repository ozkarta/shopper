import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import Record, { Document } from 'mongoose';
import * as mongoose from 'mongoose';
// ===============================================
import { LocalizationString } from 'src/modules/localization-string/localization-string.schema';
import { Shop } from 'src/modules/shop/shop.schema';
import { Media, MediaSchema } from 'src/modules/media/media.schema';
import { Category } from 'src/modules/category/category.schema';
import { Feedback } from 'src/modules/feedback/feedback.schema';
import { Sales } from 'src/modules/sales/sales.schema';
import { Rating } from 'src/modules/rating/rating.schema';
import { Currencies } from 'src/shared/constants';
import { currencies } from 'src/shared/enums';

export type ProductDocument = Product & Document;

@Schema({timestamps: true})
export class Product {
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Shop'})
    shop: Shop;
    @Prop()
    friendlyId: LocalizationString;
    @Prop()
    title: LocalizationString;
    @Prop({ type: LocalizationString, required: false })
    shortDescription: LocalizationString;
    @Prop({ type: LocalizationString, required: false })
    longDescription: LocalizationString;
    @Prop({type: [
      {
        item: {type: mongoose.Schema.Types.ObjectId, ref: 'Media'},
        isPrimary: {type: Boolean, default: false},
      }
    ]})
    media: Record<string, any>[];
  
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Category'}]})
    categories: Category[];
    @Prop({type: [{type: String, trim: true}]})
    keywords: string[];
  
    @Prop({default: 0})
    quantity: number;
    @Prop({default: 0})
    price: number;
    @Prop({enum: currencies, default: Currencies.GEL})
    currency: string;
    // status: {type: String, enum: ['active', 'disabled', 'out_of_stock']},
  
    // ==============================================
    // discounts: [{
    //   currency: {
    //     type: String,
    //     enum: CountryEnum,
    //   },
    //   price: {
    //     type: Number,
    //     default: 0
    //   }
    // }],
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, Ref: 'Rating'}]})
    rating: Rating[]; // 
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Sales'}]})
    sales: Sales[];
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Feedback'}]})
    feedback: Feedback[];
  
    // SYSTEM
    @Prop({ type: Boolean, default: false })
    enabled: boolean;
    @Prop({ type: Boolean, default: false })
    deleted: boolean;
}

export const ProductSchema = SchemaFactory.createForClass(Product);