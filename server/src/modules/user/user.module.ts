import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RefreshToken, RefreshTokenSchema } from './refresh-token.schema';
import { UserController } from './user.controller';

import { User, UserSchema } from './user.schema';
import { UserService } from './user.service';
@Module({
    imports: [
        // MongooseModule.forFeatureAsync([
        //     {
        //         name: User.name,
        //         useFactory: () => {
        //             const schema = UserSchema;
        //             schema.plugin(require('mongoose-lean-virtuals'));
        //             return schema;
        //           },
        //     }
        // ])
        MongooseModule.forFeature([{name: User.name, schema: UserSchema}]),
        MongooseModule.forFeature([{name: RefreshToken.name, schema: RefreshTokenSchema}]),
    ],
    controllers: [
        UserController
    ],
    providers: [
        UserService,
    ],
    exports: [
        UserService
    ]
})
export class UserModule {} 