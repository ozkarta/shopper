import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as Mongoose from 'mongoose';

export type RefreshTokenDocument = RefreshToken & Document;

@Schema({timestamps: true, autoIndex: true, toJSON: {virtuals: true}})
export class RefreshToken {
    @Prop({ref: 'User', required: true})
    userId: Mongoose.Schema.Types.ObjectId;

    @Prop({required: true})
    accessToken: true;
    @Prop({required: true})
    refreshToken: string;
    @Prop()
    usedAt: Date;
}

export const RefreshTokenSchema = SchemaFactory.createForClass(RefreshToken);