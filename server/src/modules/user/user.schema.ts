import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { string } from 'joi';
import { Document } from 'mongoose';
// ===================================
import { currencies, languages, userRoles } from 'src/shared/enums';

export type UserDocument = User & Document;

@Schema({timestamps: true, autoIndex: true, toJSON: {virtuals: true}})
export class User {

    @Prop({type: String, trim: true, enum: currencies})
    currency: String;
    @Prop({type: String, trim: true, enum: languages})
    language: String;
    @Prop({type: String, trim: true})
    clientFingerprint: String;


    @Prop({type: String, trim: true})
    firstName: string;
    @Prop({type: String, trim: true})
    lastName: string;
    @Prop({type: String, trim: true})
    email: string;
    @Prop({type: String})
    passwordHash: string;
    @Prop({type: String, enum: userRoles, required: true})
    role: string;
    @Prop({type: String})
    hashedRT: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.virtual('isRegisteredUser')
    .get(function(this: any) {
        return !!this.email;
    });