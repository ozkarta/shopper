import * as Joi from 'joi';
import { currencies, languages } from 'src/shared/enums';

export const registerClientValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    firstName: Joi.string().trim().required().min(2).max(50),
    lastName: Joi.string().trim().required().min(2).max(50),
    email: Joi.string().trim().lowercase().required().min(2).max(50).email(),
    password: Joi.string().required().min(8).max(50),
    confirmPassword: Joi.string().required().valid(Joi.ref('password')),
})

export const registerMarketManagerValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    firstName: Joi.string().trim().required().min(2).max(50),
    lastName: Joi.string().trim().required().min(2).max(50),
    email: Joi.string().trim().lowercase().required().min(2).max(50).email(),
    password: Joi.string().required().min(8).max(50),
    confirmPassword: Joi.string().required().valid(Joi.ref('password')),
})

export const adminCreateMarketManagerValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    firstName: Joi.string().trim().required().min(2).max(50),
    lastName: Joi.string().trim().required().min(2).max(50),
    email: Joi.string().trim().lowercase().required().min(2).max(50).email(),
    password: Joi.string().required().min(8).max(50),
})

export const loginValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    email: Joi.string().trim().lowercase().required().min(2).max(50).email(),
    password: Joi.string().required().min(8).max(50),
})

export const queryUsersValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    keyword: Joi.string().optional().allow(null, ''),
    page: Joi.number().min(1).optional(),
    limit: Joi.number().max(200).optional()
})

export const registerPreAuthClientUserValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    clientFingerprint: Joi.string().required(),
    language: Joi.string().required().valid(...languages),
    currency: Joi.string().required().valid(...currencies),
})