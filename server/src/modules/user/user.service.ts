import { Injectable } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/modules/user/user.schema';

import { BadRequestError, InternalServerError, InvalidOrExpireedRefreshTokenError, ResourceNotFoundError } from "src/shared/classes/errors";

import * as Promise from 'bluebird';
import * as JWT from 'jsonwebtoken';
import * as Argon from 'argon2';
import { RefreshToken, RefreshTokenDocument } from "./refresh-token.schema";
import { BaseService } from "src/shared/classes/base-service";

@Injectable()
export class UserService extends BaseService {
    constructor(
        @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
        @InjectModel(RefreshToken.name) private readonly refreshTokenModel: Model<RefreshTokenDocument>
    ) {
      super(userModel);
    }

    public async createUser(userPayload): Promise<any> {
      let userExists = !!(await this.userModel.findOne({email: userPayload.email}).exec());
      if (userExists) {
        throw new BadRequestError(`email [${userPayload.email}] already exists`)
      };
      userPayload.passwordHash = await Argon.hash(userPayload.password);
      let createdUser: any = await this.userModel.create(userPayload);
      return createdUser;
    }

    public async registerPreAuthUser(userPayload: any): Promise<{access_token: string, refresh_token: string, user: any}> {     
      let createdUser: any = await this.createOne(userPayload);
      try {
        let {access_token, refresh_token} = this.createTokens(createdUser);
        let hashedRT = await Argon.hash(refresh_token);
        await this.userModel.updateOne({_id: createdUser._id}, {$set: {hashedRT}});
        let user = await this.getById(createdUser._id, true, '_id language currency role');
        return { access_token, refresh_token, user }
      } catch (error) {
        await this.userModel.deleteOne({_id: createdUser._id});
        throw error;
      }
    }


    public async registerUserWithCredentials(userPayload: any): Promise<{access_token: string, refresh_token: string, user: any}> {     
      let createdUser: any = await this.createUser(userPayload); 
      try {
        return await this.logIn(userPayload);
      } catch (error) {
        await this.userModel.deleteOne({_id: createdUser._id});
        throw error;
      }
    }

    public async logIn(userPayload: {email: string, password: string}): Promise<{access_token: string, refresh_token: string, user: any}> {
      let {email, password} = userPayload;
      let user = await this.userModel.findOne({email}).lean().exec();
      if (!user) throw new ResourceNotFoundError(`user [${email}] was not found`);
      let isPasswordValid = await Argon.verify(user.passwordHash, password);
      if (!isPasswordValid) throw new BadRequestError('password does not match');
      try {
        let {access_token, refresh_token} = this.createTokens(user);
        // save refresh token...
        let hashedRT = await Argon.hash(refresh_token);
        await this.userModel.updateOne({_id: user._id}, {$set: {hashedRT}});
        
        delete user.passwordHash;
        // transfer initial user cart to registered user
        return { access_token, refresh_token, user }
      } catch (error) {
        console.log(error);
        throw new InternalServerError('could not sign user');
      }
    }

    async logOut(accessToken: string): Promise<boolean> {
      try {
        let {userId} = JWT.decode(accessToken);
        await this.userModel.updateOne({_id: userId, $ne: {hashedRT: null}}, {$set: {hashedRT: null}}).exec();
        return true;
      } catch (error) {
        console.log(error);
      }
    }

    public async refreshUserTokens(accessToken: string, refreshToken: string): Promise<{access_token, refresh_token}> {
      try {
        JWT.verify(refreshToken, process.env.appSecret);
      } catch (error) {
        console.log(error);
        throw new InvalidOrExpireedRefreshTokenError(error.message);
      }
      let {userId} = JWT.decode(accessToken);
      let user = await this.userModel.findById(userId);
      let match = await Argon.verify(user.hashedRT, refreshToken);
      if (!match) throw new InvalidOrExpireedRefreshTokenError('Tokens does not match');
      let {access_token, refresh_token} = this.createTokens(user);
      let hashedRT = await Argon.hash(refresh_token);
      await this.userModel.updateOne({_id: user._id}, {$set: {hashedRT}});
      return {access_token, refresh_token};
    }

    // ===========================================================================================
    private createTokens(user: any) {
      let access_token: string = JWT.sign({ userId: user._id }, process.env.appSecret, {
        expiresIn: parseInt(process.env.JWT_ACCESS_TOKEN_EXPIRATION_SECONDS) || 15 * 60 * 60
      });
      let refresh_token: string = JWT.sign({ userId: user._id }, process.env.appSecret, {
          expiresIn: parseInt(process.env.JWT_REFRESH_TOKEN_EXPIRATION_SECONDS) || 15 * 60 * 60
      });
      return {access_token, refresh_token}
    }

}