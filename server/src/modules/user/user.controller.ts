import { Body, Controller, Get, HttpStatus, Next, Post, Req, Res, UsePipes, Headers, Query } from "@nestjs/common";
import { Request, Response, NextFunction } from 'express';
import { any } from "joi";
import { UserRoles } from "src/shared/constants";
import { Authenticated } from "src/shared/decorators/authenticated";
import { Roles } from "src/shared/decorators/roles";
import { JoiValidationPipe } from "src/shared/pipes/joi-validate";
import { parseGetByQuery, parseRegisterPreAuthClientUser } from "./user.parser";
import { UserService } from './user.service';
import { registerClientValidationSchema, loginValidationSchema, registerMarketManagerValidationSchema, adminCreateMarketManagerValidationSchema, queryUsersValidationSchema, registerPreAuthClientUserValidationSchema } from "./user.validators";

@Controller('user')
export class UserController {
    constructor(
        private userService: UserService,
    ) {}
    
    // =============================REG===============================================================================
    @Post('preauth-client-user')
    async registerPreAuthUser(
        @Body(new JoiValidationPipe(registerPreAuthClientUserValidationSchema)) body: any,
        @parseRegisterPreAuthClientUser() payload: any,
        @Req() req: Request,
        @Res() res: Response, 
        @Next() next: NextFunction
    ) {
        let {access_token, refresh_token, user} = await this.userService.registerPreAuthUser(payload);
        return res.status(HttpStatus.OK).json({'accessToken': access_token, 'refreshToken': refresh_token, user});
    }

    @Post('client')
    async registerClient(@Body(new JoiValidationPipe(registerClientValidationSchema)) body: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let userPayload: any = body;
        userPayload.role = UserRoles.CLIENT;
        let {access_token, refresh_token, user} = await this.userService.registerUserWithCredentials(userPayload);
        return res.status(HttpStatus.OK).json({'accessToken': access_token, 'refreshToken': refresh_token, user});
    }

    @Post('market-manager')
    async registerMarketManager(@Body(new JoiValidationPipe(registerMarketManagerValidationSchema)) body: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let userPayload: any = body;
        userPayload.role = UserRoles.MARKET_MANAGER;
        let {access_token, refresh_token, user} = await this.userService.registerUserWithCredentials(userPayload);
        return res.status(HttpStatus.OK).json({'accessToken': access_token, 'refreshToken': refresh_token, user});
    }

    @Post('admin-create-market-manager')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN, UserRoles.OWNER])
    async adminCreateMarketManager(@Body(new JoiValidationPipe(adminCreateMarketManagerValidationSchema)) body: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let userPayload: any = body;
        userPayload.role = UserRoles.MARKET_MANAGER;
        let createdUser: any = await this.userService.createUser(userPayload);
        return res.status(HttpStatus.OK).json(createdUser);
    }
    // ================================================================================================================
    @Get('me')
    @Authenticated(true)
    async getMe(@Body() body: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let user = (<any>req).user;
        delete user.passwordHash;
        delete user.hashedRT;
        return res.status(HttpStatus.OK).json(user);
    }

    @Get('query')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async getByQuery(@Query(new JoiValidationPipe(queryUsersValidationSchema)) query, @parseGetByQuery() parsed, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let result = await this.userService.getByQuery(parsed);
        return res.status(200).json(result);
    }

    @Post('login')
    async logIn(@Body(new JoiValidationPipe(loginValidationSchema)) body: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let logInPayload = body;
        let {access_token, refresh_token, user} = await this.userService.logIn(logInPayload);
        return res.status(HttpStatus.OK).json({'accessToken': access_token, 'refreshToken': refresh_token, user});
    }

    // TODO validate access and refresh tokens
    @Post('refresh-tokens')
    async refreshTokens(@Headers() headers: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let {access_token, refresh_token} = await this.userService.refreshUserTokens(headers['access-token'], headers['refresh-token']);
        return res.status(HttpStatus.OK).json({'accessToken': access_token, 'refreshToken': refresh_token});
    }
    // TODO validate access token
    @Post('logout')
    @Authenticated(true)
    async logOut(@Headers() headers: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        this.userService.logOut(headers['access-token']);
        return res.status(HttpStatus.OK).json({});
    }

}