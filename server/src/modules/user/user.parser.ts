import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserRoles } from 'src/shared/constants';
import { parseOffsetAndLimit, parseSortBy } from 'src/shared/_helpers/parser';
import * as mongoose from 'mongoose';


export const parseGetByQuery = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      let parsed: any = {
        ...parseOffsetAndLimit(query),
        ...parseSortBy(query),
        find: {
          role: UserRoles.MARKET_MANAGER
        },
        or: [
          parseEmailSearch(query),
          ...parseNameSearch(query),
          parseUserIdSearch(query),
        ]
      }
      return parsed;
    },
);

export const parseRegisterPreAuthClientUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { language, currency, clientFingerprint } = request.body;
    let parsed: any = {
      language, currency, clientFingerprint, role: UserRoles.VISITOR,
    }
    return parsed;
  },
);


function parseEmailSearch({keyword}) {
    return keyword ? { email: { $regex: keyword, $options: 'i' } } : {};
  }
  
  function parseNameSearch({keyword}) {
    return keyword ? [{ firstName: { $regex: keyword, $options: 'i' } }, { lastName: { $regex: keyword, $options: 'i' } }] : [{}];
  }
  
  function parseUserIdSearch({keyword}) {
    try {
      if (keyword && new mongoose.Types.ObjectId(keyword)) {
        return {_id: new mongoose.Types.ObjectId(keyword)};
      } else {
        return {};
      }
    } catch (e) {
      return {_id: new mongoose.Types.ObjectId()};
    }
  }