import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import Record from 'mongoose';
import { LocalizationString } from 'src/modules/localization-string/localization-string.schema'
import { Address } from 'src/modules/address/address.schema';
import { User } from 'src/modules/user/user.schema';
import { Business } from 'src/modules/business/business.schema';
import { Category } from 'src/modules/category/category.schema';
import { Product } from 'src/modules/product/product.schema';
import { Media } from 'src/modules/media/media.schema';
import { Feedback } from 'src/modules/feedback/feedback.schema';
import { Sales } from 'src/modules/sales/sales.schema';
import { Rating } from 'src/modules/rating/rating.schema';

export type ShopDocument = Shop & Document;

@Schema({timestamps: true})
export class Shop {

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true})
    owner: User;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Business'})
    business: Business;
    
    @Prop({ type: LocalizationString, required: false })
    title: LocalizationString;
    @Prop({ type: LocalizationString, required: false })
    friendlyId: LocalizationString;
    @Prop({ type: LocalizationString, required: false })
    shortDescription: LocalizationString;
    @Prop({ type: LocalizationString, required: false })
    longDescription: LocalizationString;
    @Prop({ type: LocalizationString, required: false })
    termsAndCondition: LocalizationString;
    

    @Prop()
    addresses: Address[];
    @Prop({type: [{type: String, trim: true}]})
    phones: string[]
    @Prop({type: [{type: String, trim: true}]})
    emails: string[];
    @Prop({type: String, trim: true})
    facebook: string;

    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Category'}]})
    categories: Category[];
    @Prop({type: [{type: String, trim: true}]})
    keywords: string[];
    // products may not be referenced here
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Product'}]})
    products: Product[];

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Media'})
    media: Media;
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, Ref: 'Rating'}]})
    rating: Rating[]; // 
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Sales'}]})
    sales: Sales[];
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Feedback'}]})
    feedback: Feedback[];

    // SYSTEM
    @Prop({ type: Boolean, default: false })
    enabled: boolean;
    @Prop({ type: Boolean, default: false })
    deleted: boolean;
}

export const ShopSchema = SchemaFactory.createForClass(Shop);