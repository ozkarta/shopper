import { Body, Controller, Delete, Get, Next, Param, Post, Put, Query, Req, Res, UsePipes, ValidationPipe } from "@nestjs/common";
import { NextFunction, Request, Response } from "express";
import { UserRoles } from "src/shared/constants";
import { Authenticated } from "src/shared/decorators/authenticated";
import { Roles } from "src/shared/decorators/roles";
import { JoiValidationPipe } from "src/shared/pipes/joi-validate";
import { parseCreateShopWithOwner, parseGetByQuery, parseUpdateShopCategoriesAndKeywords, parseUpdateShopContactInfo, parseUpdateShopEnabledStatus, parseUpdateShopInfo, parseUpdateShopMedia } from "./shop.parser";
import { ShopService } from "./shop.service";
import { 
    createShopWiithOwnerValidationSchema, 
    queryShopsValidationSchema, 
    updateShopEnabledStatusBodyValidationSchema, 
    updateShopMediaBodyValidationSchema, 
    getShopIdFromParamsValidationSchema, 
    updateShhopInfoBodyValidationSchema,
    updateShhopContactInfoBodyValidationSchema,
    updateShhopCategoriesAndKeywordsBodyValidationSchema} from "./shop.validators";

@Controller('shop')
export class ShopController {
    private readonly defaultPopulation: any[] = [
        {path: 'owner'},
        {path: 'categories'},
        {
            path: 'media',
            populate: [
              {
                path: 'originalImage',
                select: 'size path mimetype'
              },
              {
                path: 'highResolutionImage',
                select: 'size path mimetype'
              },
              {
                path: 'mediumResolutionImage',
                select: 'size path mimetype'
              },
              {
                path: 'lowResolutionImage',
                select: 'size path mimetype'
              }
            ]
          }
    ];

    constructor(
        private shopService: ShopService
    ) {}

    // ==================================================GET==================================================================================
    @Get('query')
    async queryShops(@Query(new JoiValidationPipe(queryShopsValidationSchema)) query: any, @parseGetByQuery() parsed, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let result = await this.shopService.getByQuery(parsed);
        return res.status(200).json(result);
    }

    @Get(':shopId')
    async getShopById(@Param(new JoiValidationPipe(getShopIdFromParamsValidationSchema)) params: any, @Param('shopId') shopId: string, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let result = await this.shopService.getById(shopId, true, undefined, this.defaultPopulation);
        return res.status(200).json(result);
    }

    // =====================================================POST============================================================================
    @Post('shop-with-owner')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async createShopWithOwner(@Body(new JoiValidationPipe(createShopWiithOwnerValidationSchema)) body: any, @parseCreateShopWithOwner() parsed: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let result = await this.shopService.createOne(parsed);
        return res.status(200).json(result);
    }

    // =====================================================PUT=============================================================================
    @Put('shop-media/:shopId')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async updateShopMedia(@Param(new JoiValidationPipe(getShopIdFromParamsValidationSchema)) params: any, @Body(new JoiValidationPipe(updateShopMediaBodyValidationSchema)) body: any, @Param('shopId') shopId: string, @parseUpdateShopMedia() parsed: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        await this.shopService.updateById(shopId, parsed);
        let result = await this.shopService.getById(shopId, true, undefined, this.defaultPopulation);
        return res.status(200).json(result);
    }

    @Put('shop-enabled-status/:shopId')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async updateShopEnabledStatus(@Param(new JoiValidationPipe(getShopIdFromParamsValidationSchema)) params: any, @Body(new JoiValidationPipe(updateShopEnabledStatusBodyValidationSchema)) body: any, @Param('shopId') shopId: string, @parseUpdateShopEnabledStatus() parsed: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        await this.shopService.updateById(shopId, parsed);
        let result = await this.shopService.getById(shopId, true, undefined, this.defaultPopulation);
        return res.status(200).json(result);
    }

    @Put('shop-info/:shopId')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async updatShopInfo(
        @Param(new JoiValidationPipe(getShopIdFromParamsValidationSchema)) params: any,
        @Param('shopId') shopId: string,
        @Body(new JoiValidationPipe(updateShhopInfoBodyValidationSchema))
        @parseUpdateShopInfo() parsed: any,
        @Req() req: Request, 
        @Res() res: Response, 
        @Next() next: NextFunction
    ) {
        await this.shopService.updateById(shopId, parsed);
        let result = await this.shopService.getById(shopId, true, undefined, this.defaultPopulation);
        return res.status(200).json(result);
    }

    @Put('shop-contact-info/:shopId')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async updatShopContactInfo(
        @Param(new JoiValidationPipe(getShopIdFromParamsValidationSchema)) params: any,
        @Param('shopId') shopId: string,
        @Body(new JoiValidationPipe(updateShhopContactInfoBodyValidationSchema))
        @parseUpdateShopContactInfo() parsed: any,
        @Req() req: Request, 
        @Res() res: Response, 
        @Next() next: NextFunction
    ) {
        await this.shopService.updateById(shopId, parsed);
        let result = await this.shopService.getById(shopId, true, undefined, this.defaultPopulation);
        return res.status(200).json(result);
    }

    @Put('categories-and-keywords/:shopId')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async updatShopCategoriesAndKeywords(
        @Param(new JoiValidationPipe(getShopIdFromParamsValidationSchema)) params: any,
        @Param('shopId') shopId: string,
        @Body(new JoiValidationPipe(updateShhopCategoriesAndKeywordsBodyValidationSchema))
        @parseUpdateShopCategoriesAndKeywords() parsed: any,
        @Req() req: Request, 
        @Res() res: Response, 
        @Next() next: NextFunction
    ) {
        await this.shopService.updateById(shopId, parsed);
        let result = await this.shopService.getById(shopId, true, undefined, this.defaultPopulation);
        return res.status(200).json(result);
    }

    // =========================================================DELETE===========================================================================

    @Delete(':shopId')
    @Authenticated(true)
    @Roles([UserRoles.ADMIN])
    async deleteShopById(@Param(new JoiValidationPipe(getShopIdFromParamsValidationSchema)) params: any, @Param('shopId') shopId: string, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let result = await this.shopService.findOneAndDelete(shopId);
        return res.status(200).json(result);
    }
}