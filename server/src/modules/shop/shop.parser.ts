import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserRoles } from 'src/shared/constants';
import { parseOffsetAndLimit, parseSortBy } from 'src/shared/_helpers/parser';
import * as mongoose from 'mongoose';


export const parseGetByQuery = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      let parsed: any = {
        ...parseOffsetAndLimit(query),
        ...parseSortBy(query),
        find: {
          role: UserRoles.MARKET_MANAGER
        },
        or: [
          parseEmailSearch(query),
          ...parseNameSearch(query),
          parseUserIdSearch(query),
        ],
        populate: [
          {
            path: 'owner'
          },
          {
            path: 'media',
            populate: [
              {
                path: 'originalImage',
                select: 'size path mimetype'
              },
              {
                path: 'highResolutionImage',
                select: 'size path mimetype'
              },
              {
                path: 'mediumResolutionImage',
                select: 'size path mimetype'
              },
              {
                path: 'lowResolutionImage',
                select: 'size path mimetype'
              }
            ]
          }
        ]
      }
      return parsed;
    },
);

export const parseUpdateShopMedia = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const {body} = request;
  let parsed: any = {
    media: body.mediaId,
  }
  return parsed;
});

export const parseUpdateShopEnabledStatus = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const {body} = request;
  let parsed: any = {
    enabled: body.enabled,
  }
  return parsed;
});

export const parseCreateShopWithOwner = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const {body} = request;
  let parsed: any = {
    owner: body.owner
  }
  return parsed;
});

export const parseUpdateShopInfo = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const {body} = request;
  let {title, friendlyId, shortDescription, longDescription, termsAndCondition} = body || {};
  let parsed: any = {
    title, friendlyId, shortDescription, longDescription, termsAndCondition
  }
  return parsed;
});

export const parseUpdateShopContactInfo = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const {body} = request;
  let {addresses, emails, facebook, phones} = body || {};
  let parsed: any = {
    addresses, emails, facebook, phones
  }
  return parsed;
});

export const parseUpdateShopCategoriesAndKeywords = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { categories, keywords } = request.body || {};
    let parsed: any = {
      categories,
      keywords,
    };
    return parsed;
  },
);


// ==================================================================================================================================
function parseEmailSearch({keyword}) {
    return keyword ? { email: { $regex: keyword, $options: 'i' } } : {};
}

function parseNameSearch({keyword}) {
  return keyword ? [{ firstName: { $regex: keyword, $options: 'i' } }, { lastName: { $regex: keyword, $options: 'i' } }] : [{}];
}

function parseUserIdSearch({keyword}) {
  try {
    if (keyword && new mongoose.Types.ObjectId(keyword)) {
      return {_id: new mongoose.Types.ObjectId(keyword)};
    } else {
      return {};
    }
  } catch (e) {
    return {_id: new mongoose.Types.ObjectId()};
  }
}