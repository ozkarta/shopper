import { Injectable } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Shop, ShopDocument } from 'src/modules/shop/shop.schema';
import { BaseService } from "src/shared/classes/base-service";

@Injectable()
export class ShopService extends BaseService {
    constructor(
        @InjectModel(Shop.name) private readonly shopModel: Model<ShopDocument>
    ) {
        super(shopModel)
    }
}