import * as Joi from 'joi';
import { JoiObjectId, JoiLocalizationString } from 'src/shared/_helpers/joi-extensions';
import { addAbortSignal } from 'stream';

export const queryShopsValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    keyword: Joi.string().optional().allow(null, ''),
    page: Joi.number().min(1).optional(),
    limit: Joi.number().max(200).optional()
})

export const getShopIdFromParamsValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    shopId: JoiObjectId().required()
})

export const updateShopMediaBodyValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    mediaId: JoiObjectId().required()
})

export const updateShopEnabledStatusBodyValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    enabled: Joi.boolean().optional().allow(null)
})

export const createShopWiithOwnerValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    owner: JoiObjectId().required()
})

export const updateShhopInfoBodyValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    title: JoiLocalizationString(3, 30).required(),
    friendlyId: JoiLocalizationString(3, 100).required(),
    shortDescription: JoiLocalizationString(10, 100).required(),
    longDescription: JoiLocalizationString(10, 500).required(),
    termsAndCondition: JoiLocalizationString(10, 500).required()
})

export const updateShhopContactInfoBodyValidationSchema: Joi.ObjectSchema<any> = Joi.object({
    addresses: Joi.array().items(
        Joi.object({
            street: Joi.string().required(),
            city: Joi.string().required(),
            province: Joi.string().optional().allow(...[null, '']),
            country: Joi.object({
                name: Joi.string().required(), 
                alpha_2: Joi.string().optional().allow(...[null, '']),
                alpha_3: Joi.string().optional().allow(...[null, '']),
                countryCode: Joi.string().required(), 
                iso_3166_2: Joi.string().required(), 
                region: Joi.string().optional().allow(...[null, '']),
                subRegion: Joi.string().optional().allow(...[null, '']),
                intermediateRegion: Joi.string().optional().allow(...[null, '']),
                regionCode: Joi.string().optional().allow(...[null, '']),
                subRegionCode: Joi.string().optional().allow(...[null, '']),
                intermediateRegionCode: Joi.string().optional().allow(...[null, '']),
            }),
            zip: Joi.string().required()
        })
    ),
    emails: Joi.array().items(
        Joi.string().email()
    ).allow(...[null, '']),
    facebook: Joi.string().optional().allow(...[null, '']),
    phones: Joi.array().items(
        Joi.string().required()
    )
})

export const updateShhopCategoriesAndKeywordsBodyValidationSchema:  Joi.ObjectSchema<any> = Joi.object({
    categories: Joi.array().min(1).max(10).items(JoiObjectId()).required(),
    keywords: Joi.array().min(1).max(10).items(Joi.string()).required()
})