import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
// ====================================================

export type StoreDocument = Store & Document;

@Schema({timestamps: true})
export class Store {
    
}

export const StoreSchema = SchemaFactory.createForClass(Store);