import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
// ====================================================
import { User } from 'src/modules/user/user.schema';
import { Category } from 'src/modules/category/category.schema';
import { Address } from 'src/modules/address/address.schema';
import { Store } from 'src/modules/store/store.schema';

export type BusinessDocument = Business & Document;

@Schema({timestamps: true})
export class Business {
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true})
    owner: User;
    @Prop({type: String, trim: true, required: true, default: ''})
    businessDisplayName: string;
    @Prop()
    businessType: any[];
    @Prop({type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Category'}]})
    businessCategories: Category[];
    @Prop()
    identificationCode: string;
    @Prop()
    registrationCode: string;
    @Prop()
    registrationDate: Date;
    @Prop()
    address: Address;
    @Prop({typee: [{type: mongoose.Schema.Types.ObjectId, ref: 'Store'}]})
    stores: Store[];
}

export const BusinessSchema = SchemaFactory.createForClass(Business);