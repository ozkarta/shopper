import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SalesDocument = Sales & Document;

@Schema({timestamps: true})
export class Sales {

}

export const SalesSchema = SchemaFactory.createForClass(Sales);