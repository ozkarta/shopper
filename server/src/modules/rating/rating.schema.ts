import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type RatingDocument = Rating & Document;

@Schema({timestamps: true})
export class Rating {

}

export const RatingSchema = SchemaFactory.createForClass(Rating);