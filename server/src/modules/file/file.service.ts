import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseService } from 'src/shared/classes/base-service';
import { File, FileDocument } from './file-schema';
import { Model } from 'mongoose';
import { S3 } from 'aws-sdk';
import { Express } from 'express';
import { BadRequestError } from 'src/shared/classes/errors';
import { v4 as uuidv4 } from 'uuid';
import * as Jimp from 'jimp'
import { MediaService } from '../media/media.service';

@Injectable()
export class FileService extends BaseService {
  private readonly imageTypes: RegExp = /jpeg|jpg|png|svg|gif/;
  private readonly videoTypes: RegExp = /webm|mp4/;
  private readonly fileConfig: any =  {
    image: {
      resolutions: {
        lowResolutionImage: {
          filePrefix: 'low',
          width: 120,
          height: 160,
        },
        mediumResolutionImage: {
          filePrefix: 'medium',
          width: 480,
          height: 640,
        },
        highResolutionImage: {
          filePrefix: 'high',
          width: 960,
          height: 1280,
        },
        // extraLarge: {
        // filePrefix: 'extra-high',
        //   width: 960,
        //   height: 1128,
        // },
      }
    }
  }
  private bucketS3 = process.env.S3_BUCKET_NAME + '/public/images';

  constructor(@InjectModel(File.name) private readonly shopModel: Model<FileDocument>, private mediaService: MediaService) {
    super(shopModel);
  }

  async uploadImage(mediaId: string, original: Express.Multer.File, cropped: Express.Multer.File, owner: any): Promise<any> {
    // console.log(file)
    const fileIdentifier: string = uuidv4();
    const fileUploadLimitInBytes = parseInt(process.env.FILE_UPLOAD_MAX_LIMIT_IN_BYTES) || 5 * 1000 * 1000;
    if (!!original) {
      if (original.size > fileUploadLimitInBytes) {
        throw new BadRequestError(`Upload file size must not be greater than ${fileUploadLimitInBytes} bytes`);
      }
      if (!this.isImage(original)) {
          throw new BadRequestError('Uploaded file must be Image');
      }
      let fileType = original.originalname.split('.').pop();
      let fileName = `${fileIdentifier}.${fileType}`;
      original.filename = fileName;
    }

    let resizedImages: {[key: string]: Express.Multer.File} = Object.assign(
      {},
      original? {originalImage: (() => {
        let file_cp = Object.assign({}, original);
        file_cp.filename = `original-${original.filename}`;
        return file_cp
      })()}: {}, 
      await this.createImageResolutions(cropped || original, fileIdentifier)
    );
    
    let media: any =  {};
    let result: {[key: string]: any} = {};
    for (let imageType of Object.keys(resizedImages)) {
      let resizedFile = resizedImages[imageType];
      let uploadedFile = await this.uploadFile(resizedFile);
      result[imageType] = {
        originalname: resizedFile.originalname,
        encoding: resizedFile.encoding,
        mimetype: resizedFile.mimetype,
        destination: `S3-(${uploadedFile.Bucket})`,
        filename: resizedFile.filename,
        path: uploadedFile.Location,
        size: resizedFile.size,
        owner,
        eTag: uploadedFile.ETag,
        key: uploadedFile.key,
        fileId: fileIdentifier,
      }
      let createdFile = await this.createOne(result[imageType]);
      media[imageType] = createdFile;
    }
    if (!!mediaId) {
      console.log('update');
      return await this.mediaService.updateOneByQuery({_id: mediaId}, media, undefined, true);
    }
    console.log('create')
    return await this.mediaService.createOne(media);
  }

  async getImage(fileId: string): Promise<{mimetype, body}> {
    let file = await this.getById(fileId);
    let  options = {
      Bucket: this.bucketS3,
      Key: file.filename,
    }
    const s3 = this.getS3();
    return new Promise((resolve, reject) => {
      s3.getObject(options, (err, data) => {
        if (err) return reject(err);
        return resolve({
          mimetype: file.mimetype,
          body: data.Body
        });
      })
    })
  }
  // ==================================================================================================================================================
  private uploadFile(file: Express.Multer.File): Promise<any> {
    const { filename } = file;
    return this.uploadS3(file.buffer, this.bucketS3, filename, file.mimetype);
  }

  private async uploadS3(file, bucket, name, contentType): Promise<any> {
    const s3 = this.getS3();
    const params = {
      Bucket: bucket,
      Key: String(name),
      Body: file,
      ACL: 'public-read',
      ContentType: contentType
    };
    return new Promise((resolve, reject) => {
      s3.upload(params, (err, data) => {
        if (err) {
          reject(err.message);
        }
        resolve(data);
      });
    });
  }

  private getS3() {
    return new S3({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    });
  }

  private isImage(file: Express.Multer.File) {
    return this.imageTypes.test(file.mimetype);
  }

  private isVideo(file: Express.Multer.File) {
    return this.videoTypes.test(file.mimetype);
  }

  private async createImageResolutions(_file: Express.Multer.File, fileIdentifier: string): Promise<{[key: string]: Express.Multer.File}> {
    
    let result: {[key: string]: Express.Multer.File} = {};
    try {
      const image = await Jimp.read(_file.buffer);
      const imageQuality = 100;
      let targetConfig = this.fileConfig.image.resolutions;
      let resolutions = Object.keys(targetConfig).map(key => Object.assign(targetConfig[key], {name: key}));
  
      for (let resolution of resolutions) {
        try {
          let file = Object.assign({}, _file);
          let imageClone = image.clone();
          await imageClone.resize(resolution.width || Jimp.AUTO, resolution.height || Jimp.AUTO);
          await imageClone.quality(imageQuality);
          file.filename = `${resolution.filePrefix}-${fileIdentifier}.${_file.originalname.split('.').pop()}`;
          file.buffer = await imageClone.getBufferAsync(file.mimetype);
          file.size = file.buffer.length;
          result[resolution.name] = file;
        } catch (error) {
          console.log(error);
        }
      }
    } catch (error) {
      throw error;
    }

    return result;
  }
}
