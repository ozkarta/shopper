import { Body, Controller, Get, Next, Param, Post, Req, Res, UploadedFile, UploadedFiles, UseInterceptors } from "@nestjs/common";
import { FileInterceptor, FileFieldsInterceptor } from "@nestjs/platform-express";
import { Express } from 'express';
import { FileService } from './file.service';
import { NextFunction, Request, Response } from "express";
import { Authenticated } from "src/shared/decorators/authenticated";
import { User } from 'src/shared/decorators/get-user';

@Controller('file')
export class FileContrller {

    constructor(
        private fileService: FileService
    ) {}

    @Post('single-image')
    @Authenticated(true)
    @UseInterceptors(FileInterceptor('image-file'))
    async upload(@UploadedFile() file: Express.Multer.File, @User() user: any, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        const response = {
            originalname: file.originalname,
            filename: file.filename,
        };
        let result = await this.fileService.uploadImage(null, file, null, user)
        return res.status(200).json(result);
    }

    @Post('cropped-image')
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'original-image-file', maxCount: 1 },
        { name: 'cropped-image-file', maxCount: 1 },
      ]))
    async uploadFile(@UploadedFiles() files: { 'original-image-file'?: Express.Multer.File[], 'cropped-image-file'?: Express.Multer.File[] },  @User() user: any, @Body('mediaId') mediaId: string, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let result = await this.fileService.uploadImage(mediaId, (files['original-image-file'] || [])[0], (files['cropped-image-file'] || [])[0], user)
        return res.status(200).json(result);
    }


    @Get('image/:fileId')
    async getImageFromS3(@Param('fileId') fileId: string, @Req() req: Request, @Res() res: Response, @Next() next: NextFunction) {
        let result: {mimetype, body} = await this.fileService.getImage(fileId);
        res.attachment(fileId)
        res.contentType(result.mimetype)
        res.send(result.body);
    }
}