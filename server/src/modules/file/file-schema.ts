import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
// ====================================================
import { User } from 'src/modules/user/user.schema';

export type FileDocument = File & Document;

@Schema({timestamps: true})
export class File {
    @Prop({type: String, required: true})
    fileId: string;
    @Prop({type: String, required: true})
    originalname: string;
    @Prop({type: String, required: true})
    key: string;
    @Prop({type: String, required: true})
    encoding: string;
    @Prop({type: String, required: true})
    mimetype: string;
    @Prop({type: String, required: true})
    destination: string;
    @Prop({type: String, required: true})
    filename: string;
    @Prop({type: String, required: true})
    path: string;
    @Prop({type: Number, required: true})
    size: number;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'User'})
    owner: User;
}

export const FileSchema = SchemaFactory.createForClass(File);