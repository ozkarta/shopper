import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MediaModule } from '../media/media.module';

import { File, FileSchema } from './file-schema';
import { FileContrller } from './file.controller';
import { FileService } from './file.service';

@Module({
    imports: [
        MongooseModule.forFeature([{name: File.name, schema: FileSchema}]),
        MediaModule,
    ],
    controllers: [
        FileContrller
    ],
    providers: [
        FileService,
    ],
    exports: [
    ]
})

export class FileModule {}