import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
// ====================================================


export type LocationDocument = Location & Document;

@Schema({timestamps: true})
export class Location {
    
}

export const LocationSchema = SchemaFactory.createForClass(Location);