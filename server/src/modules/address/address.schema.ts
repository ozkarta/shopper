import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import Record from 'mongoose';

export type AddressDocument =  Address & Document;

@Schema({timestamps: true})
export class Country {
    @Prop({ type: String, trim: true })
    name: string; 

    @Prop({ type: String, trim: true })
    alpha_2: string; 

    @Prop({ type: String, trim: true })
    alpha_3: string; 

    @Prop({ type: String, trim: true })
    countryCode: string; 

    @Prop({ type: String, trim: true })
    iso_3166_2: string; 

    @Prop({ type: String, trim: true })
    region: string; 

    @Prop({ type: String, trim: true })
    subRegion: string; 

    @Prop({ type: String, trim: true })
    intermediateRegion: string; 

    @Prop({ type: String, trim: true })
    regionCode: string; 

    @Prop({ type: String, trim: true })
    subRegionCode: string; 

    @Prop({ type: String, trim: true })
    intermediateRegionCode: string;
}

@Schema({ timestamps: true })
export class Address {
    @Prop({ type: String, trim: true })
    street: string;

    @Prop({ type: String, trim: true })
    city: string;

    @Prop({ type: String, trim: true })
    province: string;

    @Prop({type: Country})
    country: Country;
    
    @Prop({ type: String, trim: true })
    zip: string;
}

export const AddressSchema = SchemaFactory.createForClass(Address);