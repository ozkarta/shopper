import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { Media, MediaSchema } from './media.schema';
// import { MediaContrller } from './media.controller';
import { MediaService } from './media.service';

@Module({
    imports: [
        MongooseModule.forFeature([{name: Media.name, schema: MediaSchema}])
    ],
    controllers: [
        // MediaContrller
    ],
    providers: [
        MediaService,
    ],
    exports: [
        MediaService
    ]
})

export class MediaModule {}