import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { File } from '../file/file-schema';

export type MediaDocument = Media & Document;

@Schema({timestamps: true})
export class Media {
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'File' })
    originalImage: File;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'File' })
    highResolutionImage: File;
    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'File'})
    mediumResolutionImage: File;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'File' })
    lowResolutionImage: File;
}

export const MediaSchema = SchemaFactory.createForClass(Media);