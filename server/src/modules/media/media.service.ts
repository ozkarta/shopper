import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { BaseService } from "src/shared/classes/base-service";
import { Media, MediaDocument } from "./media.schema";

@Injectable()
export class MediaService extends BaseService {
    constructor(@InjectModel(Media.name) private readonly shopModel: Model<MediaDocument>) {
        super(shopModel);
    }
}