import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type LocalizationStringDocument = LocalizationString & Document;

@Schema({ timestamps: true })
export class LocalizationString {
    @Prop({type: String, trim: true, required: true, default: ''})
    en: string;

    @Prop({type: String, trim: true, required: true, default: ''})
    ge: string;
}

export const LocalizationStringSchema  = SchemaFactory.createForClass(LocalizationString);