import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
// ========================================================
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
// ========================================================
import { ShopModule } from './modules/shop/shop.module';
import { UserModule } from './modules/user/user.module';
import { CategoryModule } from './modules/category/category.module';
import { SeederModule } from './shared/modules/seeder/seeder.module';
import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { ExpressErrorHandler } from './shared/filters/express-error-handler.filter';
import { Logger } from './shared/services/logger.service';
import { ThrottlerModule } from '@nestjs/throttler';
import { SetUserMiddleware } from './shared/middlewares/set-user-middleware';
import { VerifyTokenMiddleware } from './shared/middlewares/verify-token-middleware';
import { AuthorizedGuard } from './shared/guards/authorized-guard';
import { AuthenticatedGuard } from './shared/guards/authenticated-guard';
import { FileModule } from './modules/file/file.module';
import { MediaModule } from './modules/media/media.module';
import { ErrorLogModule } from './modules/error-log/error-log.module';
import { ProductModule } from './modules/product/product.module';
import { CartModule } from './modules/cart/cart.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.mongoURL, {
      connectionFactory: (connection) => {
        connection.plugin(require('mongoose-lean-virtuals'));
        return connection;
      }
    }),
    ThrottlerModule.forRoot({
      ttl: parseInt(process.env.ttl) || 60,
      limit: parseInt(process.env.limitPerTTL) || 20,
    }),
    SeederModule,
    // ==========================================
    CategoryModule,
    CartModule,
    FileModule,
    ShopModule,
    UserModule,
    MediaModule,
    ProductModule,
    ErrorLogModule,
    
  ],
  controllers: [],
  providers: [
    Logger,
    {
      provide: APP_FILTER,
      useClass: ExpressErrorHandler,
    },
    {
      provide: APP_GUARD,
      useClass: AuthorizedGuard,
    },
    {
      provide: APP_GUARD,
      useClass: AuthenticatedGuard
    }
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(VerifyTokenMiddleware)
      .exclude(
        {path: 'api/v1/user/refresh-tokens', method: RequestMethod.POST},
        {path: 'api/v1/user/login', method: RequestMethod.POST}
      )
      .forRoutes({path: '*', method: RequestMethod.ALL});
    consumer
      .apply(SetUserMiddleware)
      .forRoutes({path: '*', method: RequestMethod.ALL});
  }
}
