import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import helmet from 'helmet';
import * as compression from 'compression';


async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  if (process.env.enableCORS === 'true') {
    app.enableCors({
      origin: process.env.allowedOrigins.split(',')
    })
  } else {
    app.enableCors({
      origin: '*'
    })
  }
  app.use(helmet());
  app.use(compression());
  app.setGlobalPrefix('api/v1');
  let port = parseInt(process.env.PORT) || 3000;
  await app.listen(port);
  console.log('App is running on PORT', port)
}
bootstrap();
