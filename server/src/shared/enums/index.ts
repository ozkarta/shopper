import { generateEnumFromConstant } from "../_helpers/util";

// Named Enums
import { Currencies, UserRoles, AddressTypes } from '../constants';
import { Languages } from "../constants/languages";


const userRoles: string[] = generateEnumFromConstant(UserRoles);
const addressTypes: string[] = generateEnumFromConstant(AddressTypes);
const currencies: string[] = generateEnumFromConstant(Currencies);
const languages: string[] = generateEnumFromConstant(Languages);

export { 
  userRoles,
  addressTypes, 
  currencies,
  languages,
}

export * from './countries';