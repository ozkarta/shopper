// import {logger} from './logger';

function handleError(err: any) {
  const content = 'Uncaught Error! - ' + JSON.stringify(err.stack || err);

  // logger.error(content);
  console.log(err);
  process.exit(1);
}


export function handleUncaughtError() {
  process.on('uncaughtException', handleError);
  process.on('unhandledRejection', handleError);
}