import * as Joi from 'joi';

export function JoiObjectId() {
  return Joi.alternatives().try(
      Joi.string().regex(/^[0-9a-fA-F]{24}$/),
      Joi.object().keys({
        id: Joi.any(),
        _bsontype: Joi.allow('ObjectId')
      })
  );
};

export function JoiLocalizationString(_min?: number, _max?: number) {
  let rule = Joi.string().required();
  if (_min) rule = rule.min(_min);
  if (_max) rule = rule.max(_max);
  let joiObjectSchema: Joi.ObjectSchema<any> = Joi.object({
    en: rule,
    ge: rule,
  });
  return joiObjectSchema;
}