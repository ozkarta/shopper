const MAX_LIMIT = 1e3;

export function parseOffsetAndLimit(data: {page: number | string, limit: number | string, all: boolean | string}) {
  let {page, limit, all} = data;
  if (typeof page === 'string') {
    page = parseInt(page);
  }
  if (typeof limit === 'string') {
    limit = parseInt(limit);
  }
  
  all = all === 'true';
  return {
    offset: all ? 0 : computeOffset(parsePage(page), parseLimit(limit)),
    limit: all ? MAX_LIMIT : parseLimit(limit)
  };
}

function parsePage(page = 1) {
  try {
    page = Number(page);
  } catch {
    page = 1;
  }
  return (isFinite(page) && page > 0) ? page : 1;
}

function parseLimit(limit = 10) {
  try {
    limit = Number(limit);
  } catch {
    limit = 10;
  }
  return (isFinite(limit) && limit > 0 && limit <= MAX_LIMIT) ? limit : 1;
}

function computeOffset(page: number, limit: number) {
  return (page - 1) * limit;
}

export function parseSortBy(data: {sortBy: string}) {
  let {sortBy} = data;
  if (!sortBy) return {};

  sortBy = String(sortBy);
  const key = sortBy.substr(0, sortBy.length - 1);
  const sign = sortBy[sortBy.length - 1];
  let value;

  if (sign === '-') {
    value = -1;
  } else {
    value = 1;
  }

  return { sort: { [key]: value } };
}