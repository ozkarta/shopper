import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { catchError, Observable, throwError, map} from "rxjs";
import { AppError } from "../classes/errors";
import { Logger } from "../services/logger.service";

@Injectable()
export class ErrorHandlerInterceptor implements NestInterceptor {
    constructor(
        private logger: Logger
    ) {}

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        console.log('we are in interceptor');
        // next.handle() is an Observable of the controller's result value
        return next.handle()
            .pipe(
                map((result: any) => {
                    console.log('reesult', result);
                    return result;
                }),
                catchError((error: Error | string) => {
                    console.log('we have error...')
                    // this.logger.error(error);
                    return throwError(() => {
                        return error;
                    });
                })
            );
      }
}