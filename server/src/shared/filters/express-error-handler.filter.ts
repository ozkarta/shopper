import { ArgumentsHost, Catch, ExceptionFilter, Inject } from '@nestjs/common';
import { AppError } from 'src/shared/classes/errors';
import { Logger } from '../services/logger.service';

@Catch()
export class ExpressErrorHandler<T extends AppError> implements ExceptionFilter {
  constructor(private logger: Logger) {}
  catch(exception: T, host: ArgumentsHost) {
    this.logger.error(exception);
    const res = host.switchToHttp().getResponse();
    return res.status(exception.statusCode || 500).send({ message: exception.message });
  }
}