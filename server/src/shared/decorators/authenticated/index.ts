import { SetMetadata } from '@nestjs/common';

export const AUTHENTICATED_KEY = 'authenticated';
export const Authenticated = (authenticated: boolean) => SetMetadata(AUTHENTICATED_KEY, authenticated);