import { Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction } from "express";
import * as JWT from 'jsonwebtoken';
import { UserService } from "src/modules/user/user.service";

@Injectable()
export class SetUserMiddleware implements NestMiddleware {
    constructor(
        private userService: UserService
    ) {}
    
    async use(req: Request, res: Response, next: NextFunction) {
        try {
            // let {userId} = JWT.verify(accessToken, process.env.appSecret);
            let {userId} = <any>req;
            if (!userId) return next();
            let user = await this.userService.getById(userId);
            if (user) (<any>req).user = user;
            return next();
        } catch (error) {
            return next(error);
        }
      }
}