import { Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction } from "express";
import * as JWT from 'jsonwebtoken';
import { BadRequestError, JWTSessionExpiredError } from "src/shared/classes/errors";

@Injectable()
export class VerifyTokenMiddleware implements NestMiddleware {
    constructor(
    ) {}
    
    use(req: Request, res: Response, next: NextFunction) {
        let accessToken = req.headers['access-token'];
        let userId: string;
        if (!accessToken) return next();
        try {
            userId = JWT.verify(accessToken, process.env.appSecret).userId;
        } catch (error) {
            return next(new JWTSessionExpiredError(error.message));
        }
        if (!userId) return next(new BadRequestError('Provided token is not valid'));
        (<any>req).userId = userId;
        return next();
      }
}