import { HttpStatus } from "@nestjs/common";

export class AppError extends Error {
    public statusCode: number;
    constructor(message: string) {
        super(message);
        this.name = this.constructor.name;
        this.message = message;
    }
}

export class ResourceNotFoundError extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = HttpStatus.NOT_FOUND;
    }
}

export class UnauthorizedError extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = HttpStatus.UNAUTHORIZED;
    }
}

export class MethodNotAllowedError extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = HttpStatus.METHOD_NOT_ALLOWED;
    }
}

export class JWTSessionExpiredError extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = HttpStatus.GONE;
    }
}

export class UnsupportedMediaType extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
    }
}

export class InternalServerError extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    }
}

export class BadRequestError extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = 400;
    }
}

export class InvalidOrExpireedRefreshTokenError extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = 487;
    }
}

export class ForbiddenError extends AppError {
    constructor(message: string) {
        super(message);
        this.statusCode = HttpStatus.FORBIDDEN;
    }
}