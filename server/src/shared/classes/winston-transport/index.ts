import * as Transport from 'winston-transport';
import * as fs from 'fs';
import * as path from 'path';

export class WinstonTransport extends Transport {
  private readonly homePath = process.env.HOME || process.env.HOMEPATH;
  private readonly appName = process.env.appName || 'shopper-APP';
  private readonly serverDataDirName = process.env.serverDataDirName || `.${this.appName}-seerver-data`;
  private readonly dataRoot = path.join(this.homePath, this.serverDataDirName);
  private readonly logsDirPath = path.join(this.dataRoot, process.env.logsDirName || 'logs');
  private readonly environment: string = process.env.development === 'true'? 'development': 'production'

  private logFileParentDirName: string;
  private logFileParentDirPath: string;

  private logFileName: string;
  private logFilePath: string;

  private activeInterval: NodeJS.Timeout;
  private endOfTheDay: Date;
  private lastRotateDate: Date;

  constructor(opts: any) {
    super(opts);

    this.initFilesAndFolders();
    this.startRotationInterval();
  }

  initFilesAndFolders() {
    this.initRootDirectory();
    this.initLogFolderByDate();
    this.initLogFile();
  }

  startRotationInterval() {
    if (this.activeInterval) {
      clearInterval(this.activeInterval);
    }
    let msDiff = this.endOfTheDay.getTime() - this.lastRotateDate.getTime();
    this.activeInterval = setInterval(() => {
      this.initFilesAndFolders();
      this.startRotationInterval();
    }, msDiff);
  }

  initRootDirectory() {
    if (!fs.existsSync(this.dataRoot)) {
      fs.mkdirSync(this.dataRoot);
    }

    if (!fs.existsSync(this.logsDirPath)) {
      fs.mkdirSync(this.logsDirPath);
    }
  }

  initLogFolderByDate() {
    this.lastRotateDate = new Date();
    this.endOfTheDay = new Date();
    this.endOfTheDay.setHours(24, 0, 0, 0);

    this.logFileParentDirName = this.yyyymm(this.lastRotateDate);
    this.logFileParentDirPath = path.join(this.logsDirPath, this.logFileParentDirName);
    if (!fs.existsSync(this.logFileParentDirPath)) {
      fs.mkdirSync(this.logFileParentDirPath);
    }
  }

  initLogFile() {
    this.appendFileSync('', false);
  }

  appendFileSync(content: any, newLine: boolean = true) {
    if (newLine) {
      content = `${content}\n`;
    }
    this.logFileName = `${this.appName}-${this.environment}-${this.yyyymmdd()}.log`;
    this.logFilePath = path.join(this.logFileParentDirPath, this.logFileName);
    fs.appendFileSync(this.logFilePath, content, 'utf8');
  }

  log(info: any, callback: Function) {
    setImmediate(() => {
      this.emit('logged', info);
    });

    if (!this.lastRotateDate || this.lastRotateDate.getDate() !== (new Date()).getDate()) {
      this.initRootDirectory();
      this.initLogFolderByDate();
      this.startRotationInterval();
    }
    if (!info) info = {};
    info.createDate = new Date();
    this.appendFileSync(JSON.stringify(info));
    callback();
  }

  yyyymm(date = new Date()) {
    let mm = date.getMonth() + 1; // getMonth() is zero-based
    // var dd = date.getDate();

    return [
      date.getFullYear(),
      (mm > 9 ? '' : '0') + mm
    ].join('-');
  };

  yyyymmdd(date = new Date()) {
    let mm = date.getMonth() + 1; // getMonth() is zero-based
    var dd = date.getDate();

    return [
      date.getFullYear(),
      (mm > 9 ? '' : '0') + mm,
      (dd > 9 ? '' : '0') + dd
    ].join('-');
  };
}