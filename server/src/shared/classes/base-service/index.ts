import { Model } from 'mongoose';
import * as Promise from 'bluebird';

export class BaseService {
    constructor(
        private model: Model<any>
    ) {}

    //=====================================GET========================================================
    getAll(virtuals: boolean = true, selectFields: string = ''): Promise<any[]> {
        return this.model.find({}, selectFields).lean({virtuals}).exec();
    }

    getByQuery({ find = {}, or = [{}], sort = {}, offset = 0, limit = 10, populate = '' }, virtuals: boolean = true, selectFields: string = "" ): Promise<{items: any[], page: number, limit: number, numTotal: number}> {
        if (!sort || Object.keys(sort).length === 0) {
          sort = { _id: -1 };
        }
        return Promise.all([
          this.model
            .find(find, selectFields)
            .lean({ virtuals })
            .or(or)
            .sort(sort)
            .skip(offset)
            .limit(limit)
            .populate(populate)
            .exec(),
          this.model.find(find).lean().or(or).countDocuments().exec(),
        ]).spread((items: any[], numTotal: number) => ({ items, page: Math.floor(offset / limit) + 1, limit, numTotal }));
    }

    getByAggregationQuery(findPipeline: any[] = [], countPipeline: any[] = [], offset: number = 0, limit?: number ): Promise<any> {
        if (!findPipeline.length) {
            findPipeline = [...findPipeline, {'$match': {}}];
        }
        if (countPipeline.length === 0) countPipeline = [...findPipeline];
        findPipeline.push(
            {$skip: offset},
        );

        if (limit) {
            findPipeline.push(
                {$limit: limit}
            );
        }

        countPipeline.push({
            $group: { _id: 1, numTotal: { $sum: 1 } }
        });

        return Promise.all([
            this.model.aggregate(findPipeline),
            this.model.aggregate(countPipeline),
        ]).spread((items: any[], [countResult, ...[]]: any[]) => ({ items, numTotal: countResult && countResult.numTotal || 0, page: Math.floor(offset / limit) + 1, limit }));
    }

    getById(id: string, virtuals: boolean = true, selectFields: string = '', populationQuery?: any): Promise<any> {
        let queue = this.model.findOne({ _id: id }, selectFields);
        if (populationQuery) queue = queue.populate(populationQuery);
        queue = queue.lean({virtuals});
        return queue.exec();
    }

    getOneByQuery(find: any = {}, virtuals: boolean = true, selectFields: string = '', populationQuery?: any): Promise<any> {
        let queue = this.model.findOne(find, selectFields);
        if (populationQuery) queue = queue.populate(populationQuery);
        queue = queue.lean({virtuals});
        return queue.exec();
    }

    // =======================================CREATE=======================================================
    createOne(data: any): Promise<any> {
        return this.model.create(data);
    }

    createMany(shops: any[]): Promise<any> {
        return this.model.insertMany(shops);
    }

    upsert(find: any, data: any): Promise<any> {
        return this.model.updateMany(find, data, {upsert: true}).exec();
    }
    // ===========================================DELETE===========================================================
    clearCollection(): Promise<any> {
        return this.model.deleteMany({}).exec();
    }

    findOneAndDelete(_id: string): Promise<any> {
        return this.model.findOneAndRemove({_id}).exec();
    }

    findOneByQueryAndDelete(query: any): Promise<any> {
        return this.model.findOneAndRemove(query).exec();
    }

    findManyByQueryAndDelete(query: any): Promise<any> {
        return this.model.deleteMany(query).exec();
    }

    setDeletedFlag(_id, value) {
        return this.updateById(_id, {$set: {deleted: value}})
    }

    setDeletedFlagByQuery(query, value) {
        return this.updateOneByQuery(query, {deleted: value});
    }

    // ============================================UPDATE===============================================================
    updateById(_id: string, payload: {$set?: unknown, $inc?: unknown}): Promise<any> {
        return this.model.updateOne({_id}, payload).exec();
    }

    updateOneByQuery(query: any, $set: any = {}, $inc: any = {}, $new: boolean = true, $push: any = {}): Promise<any> {
        return this.model.findOneAndUpdate(query, { $set, $inc, $push }, {new: $new}).exec();
    }

    updateManyByQuery(query: any, $set: any = null, $inc: any = null, $new: boolean = true, $push: any = null): Promise<any> {
        let updateQuery: any = {};
        if ($set) updateQuery['$set'] = $set;
        if ($inc) updateQuery['$inc'] = $inc;
        if ($push) updateQuery['$push'] = $push;
        return this.model.updateMany(query, updateQuery , {new: $new}).exec();
    }
}