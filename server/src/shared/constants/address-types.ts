export enum AddressTypes {
  SHIPPING_ADDRESS = 'shipping-address',
  PRODUCT_LOCATION = 'product-location',
  GENERIC = 'generic'
}