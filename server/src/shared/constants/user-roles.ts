export enum UserRoles {
  OWNER = 'owner',
  ADMIN = 'admin',
  MARKET_MANAGER = 'market-manager',
  MARKET_SELLER = 'market-seller',
  CLIENT = 'client',
  VISITOR = 'visitor'
}