import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import * as Joi from 'joi';
import { BadRequestError } from 'src/shared/classes/errors';

@Injectable()
export class JoiValidationPipe implements PipeTransform {
  constructor(private schema: Joi.ObjectSchema | Joi.ArraySchema) {
  }

  transform(value: any, metadata: ArgumentMetadata) {
    const { error } = this.schema.validate(value);
    if (error) {
        throw new BadRequestError(error.message);
    }
    return value;
  }
}
