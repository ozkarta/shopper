import { model, Schema, Types } from 'mongoose';
import { ResourceNotFoundError } from '../classes/errors';

export function categorySortHeaderAssignPlugin(schema: Schema) {

  schema.pre('save', async function () {
    const CategoryModel = this.constructor;
    try {
      let count = await CategoryModel.countDocuments({parentCategory: this.parentCategory});
      this.sortWeight = count + 1;
    } catch (error) {
      console.log(error);
      throw error;
    }
  });

  schema.pre('updateOne', async function() {
    const CategoryModel = this.model;
    try {
      let category = await CategoryModel.findOne(this);
      let updateData = this._update['$set'] || this._update;
      if (!category) throw new ResourceNotFoundError(`Could not find category by _id=${updateData._id}`);
      if (!updateData.sortWeight) {
        throw new ResourceNotFoundError(`${JSON.stringify(updateData)} => could not find sortWeight`);
      };

      if (category.sortWeight === updateData.sortWeight && category.parentCategory === new Types.ObjectId(updateData.parentCategory)) {
        return
      };
      
      let prevParentCategoriesToUpdate: any[] = await CategoryModel.find({parentCategory: category.parentCategory, sortWeight: {$gt: category.sortWeight}});
      
      await CategoryModel.updateMany({_id: {$in: prevParentCategoriesToUpdate.map((item: any) => item._id)}}, {$inc: {sortWeight: -1}});
      
      let newParentCategoriesToUpdate = await CategoryModel.find({parentCategory: updateData.parentCategory, sortWeight: {$gte: updateData.sortWeight}});
      await CategoryModel.updateMany({_id: {$in: newParentCategoriesToUpdate.map((item: any) => item._id)}}, {$inc: {sortWeight: 1}});

      if (category.parentCategory) {
        if (category.parentCategory.toString()!==updateData.parentCategory) {
          await CategoryModel.updateOne({_id: category.parentCategory}, {$pull: {childCategories: category._id}});
        }
      }
      if (updateData.parentCategory) {
        if (category.parentCategory!==updateData.parentCategory || (category.parentCategory && category.parentCategory.toString()!==updateData.parentCategory)) {
          await CategoryModel.updateMany({_id: updateData.parentCategory}, {$push: {childCategories: category._id}});
        }
      }
    } catch (error) {
      console.log(error);
    }
  });

  schema.pre('deleteOne', async function() {
    const CategoryModel = this.constructor;
    try {
      let category = await CategoryModel.findOne(this);
      let categoriesToUpdate: any[] = await CategoryModel.find({parentCategory: category.parentCategory, sortWeight: {$gt: category.sortWeight}});
      await CategoryModel.updateMany({_id: {$in: categoriesToUpdate.map(item => item._id)}}, {$inc: {sortWeight: -1}});
    } catch (error) {
      console.log(error)
      throw error;
    }
  });

}