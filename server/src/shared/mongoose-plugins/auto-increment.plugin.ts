import { model, Schema } from 'mongoose';

let autoIncrementSchema = new Schema({
  schemaName: {type: String},
  index: {type: Number},
}, {
  timestamps: true
});

let AutoIncrementModel = model('auto-increment', autoIncrementSchema);

const defaultSchemaName = 'default';
const defaultFieldName = '_index';
const defaultStep = 1;

export function autoIncrementPlugin (schema: Schema, {schemaName, fieldName, step} = {schemaName: defaultSchemaName, fieldName: defaultFieldName, step: defaultStep}) {
  let obj: any = {};
  obj[fieldName] = 0;
  schema.add(obj);

  schema.pre('save', async (doc: any) => {
    try {
      let autoIncrement: any = await AutoIncrementModel.findOne({schemaName: schemaName}).exec();
      if (!autoIncrement) {
        autoIncrement = new AutoIncrementModel({schemaName: schemaName, index: 1});
        await autoIncrement.save();
      } else {
        autoIncrement.index = autoIncrement.index + step;
        await AutoIncrementModel.findByIdAndUpdate(autoIncrement._id, autoIncrement).exec();
      }

      doc[fieldName] = autoIncrement.index;
    } catch (error) {
      throw(error);
    }    
  });

  schema.pre('update', function (next) {
    next();
  });

}