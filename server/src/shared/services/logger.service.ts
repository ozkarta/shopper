import { Injectable } from "@nestjs/common";
import * as winston from 'winston';
import { WinstonTransport } from '../classes/winston-transport';

@Injectable()
export class Logger {
    private _transport = new WinstonTransport({});

    private _logger = winston.createLogger({
      transports: [
        this._transport
      ]
    });

    public error(error: Error | string) {
      let logData: string = error instanceof Error? error?.stack : error;
      if (process.env.development === 'true') {
        console.log(error);
      }
      this._logger.error(logData);
    }
}