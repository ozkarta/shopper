import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserRoles } from 'src/shared/constants';
import { AUTHENTICATED_KEY } from 'src/shared/decorators/authenticated';

@Injectable()
export class AuthenticatedGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const needsAuthentication = this.reflector.getAllAndOverride<UserRoles[]>(AUTHENTICATED_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!needsAuthentication) {
      return true;
    }
    const { user } = context.switchToHttp().getRequest();
    if (!user) return false;
    return !!(user && user._id);
  }
}