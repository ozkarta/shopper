import { Module } from '@nestjs/common';
import { CategoryModule } from 'src/modules/category/category.module';
import { ShopModule } from 'src/modules/shop/shop.module';
import { UserModule } from 'src/modules/user/user.module';
import { SeederService } from './seeder.service';

@Module({
    imports: [
        UserModule,
        ShopModule,
        CategoryModule,
    ],
    providers: [
        SeederService,
        {
            provide: 'SEED',
            useFactory: async (seederService: SeederService) => {
                if (process.env.seedDB === 'true') {
                    console.log('starting DB seeding...');
                    if (process.env.development === 'true') {
                        await seederService.seedDBDevelopment();
                    } else {
                        await seederService.seedDBProduction();
                    }
                    console.log('DB seeding is complete');
                }
            },
            inject: [SeederService]
        }
    ]
})

export class SeederModule {
    constructor() {}
}