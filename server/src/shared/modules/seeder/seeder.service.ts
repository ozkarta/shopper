import { Injectable } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserService } from 'src/modules/user/user.service';
import { UserRoles } from "src/shared/constants";
import * as Argon from 'argon2';
import { ShopService } from "src/modules/shop/shop.service";
import { CategoryService } from "src/modules/category/category.service";
import * as mongoose from 'mongoose';

@Injectable()
export class SeederService {
    constructor(
        private userService: UserService,
        private shopService: ShopService,
        private categoryService: CategoryService,
    ) {}

    public async seedDBDevelopment() {
        console.log('ENV=DEVELOPMENT')
        console.log('clearing USERS');
        await this.clearDBUsers();
        console.log('Seeding USERS');
        await this.seedDBUsers();

        console.log('clearing SHOPS...');
        await this.clearDBShops();
        console.log('Seeding SHOPS');
        await this.seedDBShops();

        console.log('clearing CATEGORIES...')
        await this.clearDBCategories();
        console.log('Seeding CATEGORIES');
        await this.seedDBCategories();
    }

    public async seedDBProduction() {
        console.log('ENV=DEVELOPMENT')
        console.log('clearing USERS');
        await this.clearDBUsers();
        console.log('Seeding USERS');
        await this.seedDBUsers();

        console.log('clearing SHOPS...');
        await this.clearDBShops();
        console.log('Seeding SHOPS');
        await this.seedDBShops();

        console.log('clearing CATEGORIES...')
        await this.clearDBCategories();
        console.log('Seeding CATEGORIES');
        await this.seedDBCategories();
    }

    // ==============================USERS=======================
    private async seedDBUsers() {
        let admins = [
          { 
            email: 'admin@shopper.ge', 
            password: 'adminadmin', 
            firstName: 'admin', 
            lastName: 'admin',
            role: UserRoles.ADMIN
          },
          { 
            email: 'admin1@shopper.ge', 
            password: 'adminadmin', 
            firstName: 'admin', 
            lastName: 'admin',
            role: UserRoles.ADMIN
          },
          { 
            email: 'admin2@shopper.ge', 
            password: 'adminadmin', 
            firstName: 'admin', 
            lastName: 'admin',
            role: UserRoles.ADMIN
          }
        ];
      
        let clients = Array.from(Array(20), (k, i) => {
          return { 
            email: `client${i + 1}@shopper.ge`, 
            password: 'clientclient', 
            firstName: `name${i + 1}`, 
            lastName: 'last-name',
            role: UserRoles.CLIENT
          }
        });  
      
        let marketManagers = Array.from(Array(20), (k, i) => {
          return { 
            email: `market-manager${i + 1}@shopper.ge`, 
            password: 'market-managermarket-manager', 
            firstName: `name${i + 1}`, 
            lastName: 'last-name',
            role: UserRoles.MARKET_MANAGER
          }
        });
      
        let users: any[] = [...admins, ...clients, ...marketManagers];
        for (let user of users) {
            user.passwordHash = await Argon.hash(user.password);
        }
        await this.userService.createMany(users);
    }

    private async clearDBUsers() {
        return this.userService.clearCollection();
    }
    // ===============================SHOPS=======================
    private async seedDBShops() {
        let {items: users} = await this.userService.getByQuery({
        find: {
            role: UserRoles.MARKET_MANAGER
        }
        });
        let shops = this.generateShops(20, users);
        await this.shopService.createMany(shops);
    }

    private generateShops(shopQuantity = 10, owners: any[] = []) {
        if (!owners || !owners.length) return;
        let shops: any[] = []
        for (let i=0; i<shopQuantity; i++) {
          let owner = owners[Math.floor(Math.random() * owners.length)];
          shops.push({
            "phones": ["568208075"],
            "emails": [
              `shop${i}ify1@gmail.com`,
              `shop${i}ify2@gmail.com`,
              `shop${i}ify3@gmail.com`,
            ],
            "categories": [], 
            "keywords": [],
            "products": [],
            "sales": [],
            "feedback": [],
            "enabled": true,
            "deleted": false,
            owner,
            "addresses": [],
            "friendlyId": { 
              "en": `Shop-${i}-Ify`, 
              "ge": `მაღაზია-${i}` 
            }, 
            "longDescription": { 
              "en": "<p><span style=\"font-size:20px\"><em><strong>Miusov, as a man man of breeding and deilcacy</strong></em></span></p>\n\n<p>could not but feel some inwrd qualms, when he reached the Father Superior&#39;s with Ivan: he felt ashamed of havin lost his temper. He felt that he ought to have disdaimed that despicable wretch, Fyodor Pavlovitch, too much to have been upset by him in Father Zossima&#39;s cell, and so to have forgotten himself. &quot;Teh monks were not to blame, in any case,&quot; he reflceted, on the steps.</p>\n\n<p><u><em><strong><span style=\"font-size:18px\">Another Test Bold</span></strong></em></u></p>\n\n<p>&quot;And if they&#39;re decent people here (and the Father Superior, I understand, is a nobleman) why not be friendly and courteous withthem? I won&#39;t argue, I&#39;ll fall in with everything, I&#39;ll win them by politness, and show them that I&#39;ve nothing to do with that Aesop, thta buffoon, that Pierrot, and have merely been takken in over this affair, just as they have.&quot; <u><em>He determined to drop his litigation with the monastry</em></u>, and relinguish his claims to the wood-cuting and fishery rihgts at once. He was the more ready to do this becuase the rights had becom much less valuable, and he had indeed the vaguest idea where the wood and river in quedtion were.</p>\n", 
              "ge": "18 მაისს, 18:30 საათზე, ილიას სახელმწიფო უნივერსიტეტის წიგნის სახლში „ლიგამუსი“  გაიმართება პროფესორ ბელა წიფურიას წიგნის პრეზენტაცია. წიგნი - სახელწოდებით, „ქართული ტექსტი საბჭოთა/პოსტსაბჭოთა/პოსტმოდერნულ კონტექსტში“ - ილიაუნის გამომცემლობამ გამოსცა.\n\nწიგნი ეხება თანამედროვე ქართული ლიტერატურის საკითხებს მეოცე საუკუნის პირველი ნახევრიდან, საბჭოური კულტურული პოლიტიკის დამკვიდრებიდან დღემდე. შესაბამისად, განხილულია ის კულტურულ-ესთეტიკური პრობლემები, რასაც ქართული ლიტერატურა აწყდება ცვალებადი პოლიტიკური, იდეოლოგიური გარემოს პირობებში. ასევე, ნაჩვენებია, თუ როგორ ახდენს მწერლობა ამ პრობლემებზე რეაგირებას. წიგნის პირველ თავში საუბარია ქართული ლიტერატურული ტექსტის კონტექსტუალიზაციის ზოგად პრობლემებზე; მეორე თავში ცალკეული ლიტერატურული ტექსტები განხილულია შემოთავაზებული პრინციპებით; მესამე ნაწილში განხილულია პოსტმოდერნი, როგორც კონტექსტი, და პოსტმოდერნიზმის ზოგადი კულტურულ-მსოფლმხედველობრივი პრინციპები." 
            }, 
            "shortDescription": { "en": "<p>This is Market Hub, We are tying to make your life easier test test</p>\n\n<p>&nbsp;</p>\n", "ge": "ეს არის მარკეტ ჰაბი, ჩვენ ვაქცევთ შენს ცხოვრებას უფრო მარტივს" }, "termsAndCondition": { "en": "\nThese excellant intentions were strengthed when he enterd the Father Superior's diniing-room, though, stricttly speakin, it was not a dining-room, for the Father Superior had only two rooms alltogether; they were, however, much larger and more comfortable than Father Zossima's. But tehre was was no great luxury about the furnishng of these rooms eithar. The furniture was of mohogany, covered with leather, in the old-fashionned style ", 
              "ge": "18 მაისს, 18:30 საათზე, ილიას სახელმწიფო უნივერსიტეტის წიგნის სახლში „ლიგამუსი“  გაიმართება პროფესორ ბელა." 
            }, 
            "title": { 
              "en": `Shop ${i} Ify`, 
              "ge": `მაღაზია ${i}` 
            }, 
            "facebook": null, 
            "media": null
          })
        }
        return shops;
    }

    private async clearDBShops() {
        return this.shopService.clearCollection();
    }

    // ===============================CATEEGORIES==================
    private generateFriendlyId(text: string) {
        let result = text.split('@').join('');
        result = result.split('&').join('_and_');
        return result;
      }
      
    private generateCategoryName(text: string) {
        let result = text.split('@').join('');
        result = result.split('_').join(' ');
        result = result.split('&').join(' and ');
        return result;
    }

    private async createCategoryObject(category: any, parent: any) {
        let categoryName = this.generateCategoryName(category.category_name);
        let categoryFriendlyId = this.generateFriendlyId(category.category_name);
      
        if (!categoryName) {
          return;
        }
        let categoryObject: any = {_id: new mongoose.Types.ObjectId()};
        categoryObject.categoryName = {
          en: categoryName,
          ge: categoryName
        };
        categoryObject.friendlyId = {
          en: categoryFriendlyId,
          ge: categoryFriendlyId
        };
        if (category.show_in_search) {
          categoryObject.includeInSearch = category.show_in_search;
        }
        categoryObject.parentCategory = parent;
      
        try {
          if (category.child_category && category.child_category.length) {
            let children = [];
            for (let i = 0; i < category.child_category.length; i++) {
              if (category.child_category[i]) {
                let child = await this.createCategoryObject(category.child_category[i], categoryObject);
                if (child) {
                  children.push(child);
                }
              }
            }
            categoryObject.childCategories = children || [];
          }
        } catch (error) {
          console.log(error);
          return null
        }
      
        try {
          return this.categoryService.createOne(categoryObject);
        } catch (error) {
          console.log(error);
        }
        return null;
    }

    private async clearDBCategories() {
        return await this.categoryService.clearCollection();
    }

    private async seedDBCategories() {
        const categories = require('./category');
        for (let i = 0; i < categories.length; i++) {
          try {
            await this.createCategoryObject(categories[i], null);
          } catch (error) {
            console.dir(error);
          }    
        }  
      }
}